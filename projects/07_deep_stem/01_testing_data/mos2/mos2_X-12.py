#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#abtem commit 1219c12af835d88fff70a4788cc6e8fe06e4caae Wed Mar 22 11:21:23 2023 +0100 branch WIP_invMS


# In[ ]:


import mat73
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from abtem.reconstruct import invms
from abtem.measure import Measurement, Calibration
from abtem.utils import energy2wavelength
from abtem.waves import Probe
from abtem.utils import energy2wavelength
import scipy.io as io


matplotlib.rcParams["figure.figsize"] = (20, 10)
matplotlib.rcParams['lines.linewidth'] = 2
font = {'weight' : 'bold',
                'size'   : 22}
matplotlib.rc('font', **font)


# In[ ]:


from abtem.utils import fft_shift
def pad(arr,targetted_px):
    arr_px = np.shape(arr)[0]
    if arr_px>targetted_px:
        raise ValueError("array is larger then targetted pixel size...")
    
    diff_px=targetted_px-arr_px
    pad_px=diff_px//2
    odd=0
    if diff_px%2 != 0:
        odd=1
    padded = np.pad(arr,((pad_px,pad_px+odd),(pad_px,pad_px+odd)))
    return(padded)

def crop_to_given_px(arr,targetted_px): # arr should have same number of pixels in x and y
    arr_px = np.shape(arr)[0]
    if arr_px<targetted_px:
        raise ValueError("array is smaller then targetted pixel size...")
    
    diff_px=arr_px-targetted_px
    crop_px=diff_px//2
    odd=0
    if diff_px%2 != 0:
        odd=1
    cropped=arr[crop_px:-(crop_px+odd),crop_px:-(crop_px+odd)]
    return(cropped)

def idx_mask_crop(mask):
    nonzero_idxs = np.array(np.nonzero(mask)).reshape(-1) # relying on the fact that the number of pixels is the same in x and y 
    tmp=nonzero_idxs-(mask.shape[0]-1)/2
    max_idx = np.argmax(tmp) # finding which index is the furthest from center of the array (in taxicab metric)
    min_idx = np.argmin(tmp) 
    idx_range=slice(nonzero_idxs[min_idx],nonzero_idxs[max_idx]+1) # idx_range of mask
    return(idx_range)

def get_mask(probe_intensity):
    mask = np.zeros_like(probe_intensity)
    #mask[probe_intensity_norm>1/0.1e4] = 1
    mask[probe_intensity_norm>probe_intensity_norm.max()*0.16] = 1
    return(mask)


# In[ ]:


data=io.loadmat("/home/jilek/Downloads/MoS2_4D_data_step2x_crop_3x3uc.mat") #data from https://drive.google.com/drive/folders/1QF9ntp8tRhJetT-5_fkcsxml7X--9PFw 
energy=data['voltage'][0,0]*1e3 # voltage in kV
wavelength=energy2wavelength(energy)
dp_=data['dp'].reshape((*data['dp'].shape[:2],-1)) # from shape 124,124,31,26 to shape 124,124,806 
N_dp_x=26
N_dp_y=31 
dp_=dp_.transpose((2,0,1)) # from shape 124,124,806 to shape 806,124,124
dp_[dp_<20]=0 #ADU_background_cutoff = 20 #Threshold that below which would be removed

dp=dp_/np.mean(np.sum(np.abs(dp_),(1,2)))
x = np.linspace(0,(N_dp_x-1)*data['scanStepSize_x'],N_dp_x) 
y = np.linspace(0,(N_dp_y-1)*data['scanStepSize_y'],N_dp_y) 
grid = np.meshgrid(x,y)
positions_x = grid[0].reshape(-1)
positions_y = grid[1].reshape(-1)

rot_angle = 30/180*np.pi
positions_x_rot = positions_x*np.cos(rot_angle) + positions_y*np.sin(rot_angle)
positions_y_rot = positions_x*-np.sin(rot_angle) + positions_y*np.cos(rot_angle)
positions_x_rot_centered = positions_x_rot#+positions_x.mean()
positions_y_rot_centered = positions_y_rot#+positions_y.mean()
positions = np.array([positions_x_rot_centered,positions_y_rot_centered]).T


# In[ ]:


plt.figure()
np.sum(dp_[0]/150) #ADU_electronCount_ratio = 151.0 # factor needed to convert between detector output and actual electron counts 


# In[ ]:


plt.figure(figsize=(10,10))
plt.plot(positions_x_rot,positions_y_rot,"-x")
plt.plot(positions_x_rot[0],positions_y_rot[0],"o")
plt.xlim(-6,16)
plt.ylim(-6,16)
plt.show()


# In[ ]:


units = 'mrad'
names = ['alpha_x', 'alpha_y']

dk = data['dk'][0][0] 
dalpha = dk/(1/wavelength)*1e3

dp_px=128
dp_padded=np.zeros((len(dp),dp_px,dp_px))
for i,_ in enumerate(dp):
    dp_padded[i]=pad(dp[i],dp_px)
offsets=-dalpha*np.array(dp_padded.shape[1:])/2

cals=(None,)
for i, name in enumerate(names):
    cal=Calibration(offset=offsets[i],sampling=dalpha, units = units, name=name)
    #cal=calibrations_from_grid(gpts=np.shape(dp)[1:], sampling = (data['dk'],)*2 , fourier_space=True, names=name, units=units, scale_factor=scale_factor)
    cals+=(cal,)
measurement=Measurement(dp_padded,calibrations=cals)


# In[ ]:


measurement.shape


# In[ ]:


measurement[9].show(power=0.2)


# In[ ]:


semiangle_cutoff = data['alpha_max'][0,0]
defocus = data['df'][0,0]
probe_guess = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=defocus,extent=1/dk,gpts=dp_padded.shape[1])
reconstructions = invms(measurement, probe_guess, positions = positions, modes=1,k_modes=4, alpha=0.4, fac=1, beta=0.4, slices=1, slice_thickness=21,  maxiter=16,max_angle=None, return_iterations=True, fix_com=True,device='gpu')


# In[ ]:


np.shape(reconstructions[0][0][0])


# In[ ]:


plot_every = 4

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][-1].array).T, origin='lower', cmap='gray')
    #axes[0,i].imshow(np.fft.fftshift(np.angle(reconstructions[0][j][-1].array).T))
    axes[1,i].imshow(np.abs(reconstructions[1][j][0].array).T ** 2, origin='lower', cmap='gray')
#    for ax in (axes[0,i], axes[1,i]):
#        ax.axis('off')
    
plt.tight_layout()


# In[ ]:


get_ipython().run_line_magic('matplotlib', 'widget')
plt.imshow(np.angle(reconstructions[0][-1][-1].array).T, origin='lower', cmap='gray')


# In[ ]:


probe_guess.show()


# In[ ]:


plt.figure()
res=[]
for i in range(16):
        res.append(np.sum(np.abs(reconstructions[1][i][0].array)**2))
        plt.plot(res)


# In[ ]:





# In[ ]:


#Downsample diffraction patter by factor of `p_num`
p_num=4 # downsampling is turned off
shape_orig=np.shape(measurement)
array=np.zeros([shape_orig[0],shape_orig[1]//p_num,shape_orig[2]//p_num])

for i in range(np.shape(measurement)[0]):
        m = measurement.array[i]
        if p_num==1:
            array[i]=m
        else:
            #array[i,j]=m[0::p_num,0::p_num]+m[0::p_num,1::p_num]+m[1::p_num,0::p_num]+m[1::p_num,1::p_num]            
            for k in range(p_num):
                for l in range(p_num):
                    array[i]+=m[k::p_num,l::p_num]

#setting calibrations and creating measurement
cals=(None,)+tuple(i.copy() for i in measurement.calibrations[1:]) # cannot use equals because it wouldnt copy but it would instead pass refference
print("sampling in reciprocal space before:")
print(cals[1].sampling,cals[2].sampling)
cals[1].sampling*=p_num
cals[2].sampling*=p_num
print("sampling in reciprocal space after:")
print(cals[1].sampling,cals[2].sampling)
measurement_resampled= Measurement(array, calibrations=cals, name=measurement.name, units=measurement.units) 


# In[ ]:


np.sum(measurement[4].array)


# In[ ]:


print(probe_guess.gpts)


# In[ ]:


semiangle_cutoff = data['alpha_max'][0,0]
defocus = data['df'][0,0]
probe_guess = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=defocus,extent=1/dk,gpts=dp_padded.shape[1])
reconstructions_resampled = invms(measurement_resampled, probe_guess, positions = positions, modes=1,k_modes=4, alpha=0.4, fac=1, beta=0.4, slices=1, slice_thickness=21,  maxiter=16,max_angle=None, return_iterations=True, fix_com=True,device='gpu')


# In[ ]:


plot_every = 4

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions_resampled[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions_resampled[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions_resampled[0][j][-1].array).T, origin='lower', cmap='gray')
    #axes[0,i].imshow(np.fft.fftshift(np.angle(reconstructions[0][j][-1].array).T))
    axes[1,i].imshow(np.abs(reconstructions_resampled[1][j][0].array).T ** 2, origin='lower', cmap='gray')
#    for ax in (axes[0,i], axes[1,i]):
#        ax.axis('off')
    
plt.tight_layout()


# In[ ]:


measurement_resampled[0].show(power=0.2)


# In[ ]:


measurement[0].show(power=0.2)


# In[ ]:


# Exporting phase patches, probe and mask for deep learning 


# In[ ]:


probe_guess.gpts


# In[ ]:


if 0:
    probe = reconstructions[1][-1][0].array; hdf5_output_filename = "reconstructed_probe.hdf5"
else:
    probe_guess = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=defocus,extent=1/dk,gpts=dp_padded.shape[1],device="gpu")
    probe = probe_guess.build().array.get(); hdf5_output_filename = "guessed_probe.hdf5"
probe_intensity = (np.abs(probe) ** 2)
probe_intensity_norm = probe_intensity/np.sum(probe_intensity)

mask=get_mask(probe_intensity)
idx_range=idx_mask_crop(mask)

px=32
padded_phase_patches=np.zeros((len(positions),px,px))
for i,position in enumerate(positions):
    shift=np.array([position[0]/reconstructions[0][-1][0].calibrations[0].sampling,position[1]/reconstructions[0][-1][0].calibrations[1].sampling]) # shift in pixels
    recon=reconstructions[0][-1][-1].array
    phase_patch = np.real(np.fft.fftshift(np.angle(fft_shift(recon,-shift))) - np.min(np.angle(recon))) * mask #offseting value so that it starts from zero and masking
    padded_phase_patches[i] = pad(phase_patch[idx_range,idx_range],px) # padding to 64x64 size
padded_mask=pad(mask[idx_range,idx_range],px) # padding to 64x64 size


#padded_probe=pad(probe[idx_range,idx_range],px) # padding to 64x64 size
cropped_probe=crop_to_given_px(probe,px)

print(np.shape(cropped_probe))
print(np.shape(padded_mask))
print(np.shape(padded_phase_patches[0]))


plt.figure()
plt.imshow(np.fft.fftshift(np.angle(reconstructions[0][-1][-1].array)).T, origin='lower', cmap='gray')
plt.figure()
plt.imshow(phase_patch.T, origin='lower', cmap='gray')
plt.figure()
plt.imshow(padded_phase_patches[0].T, origin='lower', cmap='gray')
plt.figure()
plt.imshow(padded_mask.T,origin="lower")
plt.figure()
plt.imshow(np.abs(cropped_probe)**2,origin="lower")

for i in [measurement_resampled.array,positions,energy,cropped_probe,padded_mask,padded_phase_patches,probe_guess.sampling[0]]:
    print(np.shape(i))

# save to .hdf5
import h5py
with h5py.File("./output/"+hdf5_output_filename, "w") as f:
    f.create_dataset('diffpats', data=measurement_resampled.array,compression="gzip")
    f.create_dataset('positions', data=positions,compression="gzip")
    f.create_dataset('energy', data=energy)
    normalised_probe=cropped_probe/np.sqrt(np.sum(np.abs(cropped_probe)**2))*1/np.sqrt(np.prod(cropped_probe.shape))
    f.create_dataset('probe', data=normalised_probe,compression="gzip")
    f.create_dataset('mask', data=padded_mask,compression="gzip")
    f.create_dataset('phase_patches', data=padded_phase_patches,compression="gzip")
    probe_guess = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=defocus,extent=1/dk,gpts=measurement_resampled[0].array.shape[1])
    f.create_dataset('recon_sampling', data=probe_guess.sampling[0])
    



# In[ ]:


f = h5py.File("./output/"+hdf5_output_filename, "r")
diffpats = f["diffpats"][()] # 806 difrakčních obrazců - je to intenzita - |Psi|^2 
positions = f["positions"][()] #806 pozic (x,y) v angströmech
energy = f["energy"][()] #energie elektronů v elektronvoltech
probe = f["probe"][()] #komplexní vlnová funkce
mask = f["mask"][()] #maska odpovídající 0.16*max(probe)
phase_patches = f["phase_patches"][()] #jak by měla dopadnout výsledná rekonstrukce (v radianech) pro danou pozici (použil jsem výsledek rekonstrukce z ePIE a z toho jsem vyřízl vždy oblast, která odpovídá masce, ještě před vyříznutím jsem rekonstruovaný obrázek natočil, aby výřez byl vycentrovaný)
recon_sampling = f["recon_sampling"][()] #vzdálenost mezi pixely polí: `mask`, `probe`, `phase_patches` v angströmech
f.close()


# In[ ]:


measurement_resampled.calibrations[1].sampling


# In[ ]:


#Marny pokus o seskladani obrazku zpet
from time import sleep
huh=np.zeros((px*4,px*4))
def fleamch(positions):
    for i,position in enumerate(positions):
        shift=np.array([position[0]/reconstructions[0][-1][0].calibrations[0].sampling,position[1]/reconstructions[0][-1][0].calibrations[1].sampling]) # shift in pixels
        patch_fitted=pad(padded_phase_patches[i],np.shape(huh)[0])
        patch_shifted=np.real(fft_shift(patch_fitted,shift)) # tu je mozna problem s presnosti.. 
        threshold_bool=patch_shifted>=0.01
        threshold_indexes = np.where(threshold_bool) #from bool -> indexes 
        common_bool=np.isin(threshold_indexes,np.nonzero(huh==0)) #np.nonzero returns indexes
        common_idx=np.array(threshold_indexes)[common_bool] # tuple -> array is necessary for slicing
        huh[common_idx]+=patch_shifted[common_idx] # hmm cheat
        #plt.imshow(huh.T,origin="lower")
        #yield i


# In[ ]:


plt.figure()
gen=fleamch(positions)


# In[ ]:


plt.imshow(huh.T,origin="lower")


# In[ ]:


probe_guess.build().diffraction_pattern(max_angle=None).show(power=0.2)


# In[ ]:


measurement.show(power=0.2)


# In[ ]:


np.sum(np.abs(np.fft.fft2(probe))**2)


# In[ ]:





# # Test stitching

# In[ ]:


#from https://github.com/dillanchang/deep-phase-imaging.git
def stitch(objs_pred,pos,mask,num_iter,alpha):                                                                                                                                                                                                                 
    mask_1d = mask.reshape((mask.shape[0]*mask.shape[1],1)).astype(bool)                                                                                                                                                                                         
    pos_r   = np.round(pos).astype('int')                                                                                                                                                                                                                        
    obj     = np.zeros((np.max(pos_r[:,0])+objs_pred.shape[1]+1,np.max(pos_r[:,1])+objs_pred.shape[2]+1))                                                                                                                                                        
    idxs    = list(range(objs_pred.shape[0]))                                                                                                                                                                                                                    
    np.random.shuffle(idxs)
    for i in range(0,num_iter):                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        for idx in range(0,objs_pred.shape[0]):                                                                                                                                                                                                                    
            p = idxs[idx]                                                                                                                                                                                                                                            
            x = pos_r[p,0]                                                                                                                                                                                                                                           
            y = pos_r[p,1]                                                                                                                                                                                                                                           
            a = obj[x:x+objs_pred.shape[1],y:y+objs_pred.shape[2]]      
            a_1d = a.reshape((mask.shape[0]*mask.shape[1],1))           
            a_mean = np.mean(a_1d[mask_1d])  
            b = objs_pred[p,:,:]                                                                                                                                                                                                                                     
            obj_diff = alpha*(b-a+a_mean)*mask                      
            a = a + obj_diff                                                                                                                                                                                                                                         
            obj[x:x+objs_pred.shape[1],y:y+objs_pred.shape[2]] = a  
    return obj                  


# In[ ]:


positions.shape


# In[ ]:


padded_phase_patches.shape


# In[ ]:


mask.astype(bool).shape


# In[ ]:


#padded_phase_patches_zero_mean=padded_phase_patches-padded_phase_patches.mean(axis=(1,2))[None,None].T ---> TOTO JE SPATNE! potreba je udelat prumer pouze tam kde je maska!!!! 
num=padded_phase_patches.shape[0]
padded_phase_patches_means=np.zeros(num)
for i in range(num):
    padded_phase_patches_means[i] = padded_phase_patches[i][mask.astype(bool)].mean()
padded_phase_patches_zero_mean=padded_phase_patches-padded_phase_patches_means[None,None].T

shifts=np.array([positions[:,0]/reconstructions[0][-1][0].calibrations[0].sampling,positions[:,1]/reconstructions[0][-1][0].calibrations[1].sampling]).T # shift in pixels
shifts = shifts - np.array([shifts[:,0].min(),shifts[:,1].min()])
obj_final = stitch(padded_phase_patches_zero_mean[0::3],shifts[0::3],mask,100,1)
plt.figure()
plt.imshow(obj_final.T+0.046,origin="lower")


# In[ ]:


plt.figure()
plt.imshow(padded_phase_patches_zero_mean[0]);plt.colorbar()


# In[ ]:


plt.figure()
plt.imshow(mask)


# In[ ]:


plt.figure()
plt.imshow(np.fft.fftshift(np.angle(reconstructions[0][-1][-1].array)).T+0.055, origin='lower', cmap='gray')


# In[ ]:




