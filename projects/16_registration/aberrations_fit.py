#!/usr/bin/env python
# coding: utf-8

# In[ ]:


get_ipython().run_line_magic('pip', 'freeze')


# In[ ]:


#conda environment: pokus_py4DSTEM 
#abtem version: 4d630003af7a2d33ee63e9a02f1846549d04be00

import os
os.environ["MKL_NUM_THREADS"] = "16"
os.environ["NUMEXPR_NUM_THREADS"] = "16"
os.environ["OMP_NUM_THREADS"] = "16"

import numpy as np
import matplotlib.pyplot as plt
from abtem import *

from abtem.noise import poisson_noise
import scipy.constants as c
from abtem.scan import PositionScan
from abtem.reconstruct import invms
from abtem.measure import bandlimit

from abtem.custom import get_gaussian_spread
from abtem.custom import get_radiation_dose
from abtem.custom import incoherent_scan
from abtem.custom import incoherent_scan_mc
from abtem.custom import sample_probe
from abtem.custom import incoherent_scan_mc_fp
from abtem.custom import incoherent_probe, get_probe_radius
from abtem.custom import crop
from abtem.custom import semiangle_2_sampling_extent,semiangle_gpts_2_extent
from abtem.custom import decimate_measurement
from abtem.utils import fft_crop,fft_shift
import cupy as cp
import h5py
import pathlib

import matplotlib

from sample import gen_rnd_tubes

import sys 
sys.path.append("../07_deep_stem/02_simulated_testing_data_LINK") #07_deep_stem: d27c25c5c9ef530e93488680ca3c1a0b797a2e80
from utils import get_positions, get_downsampled_phase, get_downsampled_mask
from materials.utils import get_potential_from_atoms



# In[ ]:


energy_orig = 22e3
energy = 281e3 #8x: 800e3 #4x: 281e3 #1x: 22e3
fac_lambda=Probe(energy=energy_orig).wavelength/Probe(energy=energy).wavelength
fac_lambda #prislusne pronasobit reciprocal_oversampling.. 


# In[ ]:


from abtem.transfer import scherzer_defocus


# In[ ]:


scherzer_defocus(Cs=5e-3/1e-10,energy=energy)


# In[ ]:


#matplotlib.interactive(True)

#mempool = cp.get_default_memory_pool()


#def run(input_i, output_path,single_patch=False, input_path="data/orig/", input_suffix=".jpg" ,device="gpu"):
if True:
    device = "gpu"
    # Domain
    #energy = 22e3
    px_size = 56e-6
    px_num = 256
    camera_length = 240e-3
    angular_sampling = px_size/camera_length*1e3 # in mrad
    angular_extent = angular_sampling * px_num/2  # only semi angle

    binning = 1
    reciprocal_oversampling = 2*4
    direct_oversampling = 1
    sampling, extent = semiangle_2_sampling_extent(angular_extent,px_num/binning,energy=energy,direct_oversampling=direct_oversampling,reciprocal_oversampling=reciprocal_oversampling)

    # Potential, aberrations, probe

    atoms = gen_rnd_tubes(1)#reciprocal_oversampling)
    atoms.set_chemical_symbols(["Ti" for i in range(len(atoms))])
    _,potential=get_potential_from_atoms(atoms,extent=extent,sampling=sampling)
    potential_p=potential.build()
    if potential_p is None:
        assert(True)
        #return(False)    

    #energy_spread = 0.6
    #Cc = 1.34e-3/1e-10
    #focal_spread = energy_spread/energy * Cc # 1/e width of focal distribution
    temporal_sigma = 0#focal_spread/np.sqrt(2) # std of focal distribution (normal distribution is proportional to e**( 1/2*(x/std)**2) )
    #B_r=1e8

    semiangle_cutoff = 15.0
    alpha=semiangle_cutoff*1e-3

    #aberrations = get_aberrations(scale_all=1,aberrations_set={'C10':350})
    aberrations = {'C10':-7077,'C30':5e-3/1e-10,'C12':1e-5*2000,'C23':100000}
    probe = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff,rolloff=2, **aberrations, device=device,sampling=sampling,extent=extent)

    ##potential_p.project().show()
    ##probe.build().show()
    ##plt.plot(np.abs(probe.build().array[probe.gpts[0]//2,:]).get()**2)

    # Detector, scanning parameters

    detector_p = PixelatedDetector(None)

    #I=50e-12
    spatial_sigma = 0#get_gaussian_spread(probe.ctf.semiangle_cutoff,probe.energy,B_r,I)

    incoh_probe=incoherent_probe(probe,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma)
    r, _, _, _= get_probe_radius(probe_measurement=incoh_probe,current_ratio=0.90)
    d=2*r
    scan_gpts = 32
    scan_sampling = 100/32#d*0.15/8#*np.random.uniform(0.15,0.5)
    positions=get_positions(scan_gpts,np.array(probe.extent),scan_sampling)
    scan_p=PositionScan(positions=positions)

    #|######### Estimating noise

    probe_dummy=probe.copy()
    probe_dummy.sampling=np.array(probe.sampling)*direct_oversampling/2 # 
    incoh_probe_dummy=incoherent_probe(probe_dummy,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma)
    if device=="gpu":
        xp=cp
    else:
        xp=np
    max_dose_1,_=get_radiation_dose(incoh_probe_dummy,xp.array(positions),1,fft=False)


    #el=I*100e-6/c.e# number of electrons in diffraction pattern
    targeted_dose = 5000 # el/angstrom
    el = targeted_dose/max_dose_1
    ##print(el)

    phase_downsampled = get_downsampled_phase(potential_p,direct_oversampling,energy=probe.energy)
    if phase_downsampled is None:
        print("No downsampled phase")
        assert(True)
        #return(None)

    #mask = get_downsampled_mask(probe, direct_oversampling, thr_fac = 0.1, positions = positions, temporal_sigma = 0, spatial_sigma = 0)
    #mask_finer = get_downsampled_mask(probe, direct_oversampling//2, thr_fac = 0.1, positions = positions, temporal_sigma = 0, spatial_sigma = 0)
    
    #|######### Measurement

    #measurement=incoherent_scan_mc(probe,positions,detector_p,potential_p,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma,max_batch_scan=100) 
    measurement=probe.scan(scan_p,detectors=detector_p,potential=potential_p,max_batch=50,pbar=False)

    measurement_crop=crop(measurement,extent=(angular_extent*2+1e-6,angular_extent*2+1e-6))

    # Downsampling diffraction pattern

    measurement_resampled = decimate_measurement(measurement_crop,reciprocal_oversampling)

    #| Apply noise or dont?

    norm=np.mean(np.sum(measurement.array,(-2,-1))) # average of fraction !uncropped! measurements(should be close to one, but it is a bit less then one cuz some electrons go past antialiasing aperture)
    if 1:
        measurement_noisy = poisson_noise(measurement_resampled/norm, dose=el,pixel_area=1)/el # el can be set based on current and dwell time or better it can be parametrized with targeted dose (el/angstron^2), then el=targeted_dose/max_dose_1, max_dose_1 depends on shape of a probe and step size.
    else:
        measurement_noisy = measurement_resampled

    measurement_noisy.show()
    plt.show()

#run()
#    return(True)

##run(jpg_i, output_path)
##from multiprocessing.pool import ThreadPool <- dont use!
#from multiprocessing.pool import Pool
#from functools import partial
#
#MAX_THREADS = 9
#
#import time 
#s=time.time()
#if __name__ == "__main__":
#    with Pool(MAX_THREADS) as p:
#        #tot=p.map(partial(run,output_path="data/export/"), range(0,100))
#        p.map(partial(run,output_path="data/export/"), range(0,10000))
#e=time.time()
#print((e-s),"s")


# In[ ]:


probe.sampling


# In[ ]:


probe.show(power=0.4)


# In[ ]:


plt.figure()
probe.ctf.show()


# In[ ]:


probe.build().diffraction_pattern().show()


# In[ ]:


measurement.show()


# In[ ]:


probe.show(power=0.5)


# In[ ]:


potential.build().project().show()
plt.plot(positions[:,0],positions[:,1],"x",markersize=1,alpha=0.5)


# In[ ]:


#%matplotlibz inline
from tfs_utils.aberrations_fit_utils import *


# In[ ]:


aberrations_real_coefs=get_real_coefs()                                                                                                                                                    
aberrations_complex_coefs=get_complex_coefs(aberrations_real_coefs)                                                                                                                        
series, alpha_x, alpha_y = get_series(aberrations_complex_coefs=aberrations_complex_coefs)                                                                                                 
                                                                                                                                                                                           
lmbda=sp.Symbol("lambda",real=True)                                                                                                                                                        
wave_deviation=2*sp.pi/lmbda*sp.re(sp.nsimplify(series,1e-9))                                                                                                                              
                                                                                                                                                                                           
wave_deviation                                                                                                                                                                             
                                                                                                                                                                                           
#wave_deviation.expand().coeff(aberrations_real_coefs[1,2,0]) # as_coefficient() je pouze na podeleni expression mym koeficientem, coeff je potreba..                                      
                                                                                                                                                                                           
k_x,k_y=sp.symbols('k_x,k_y',real=True)                                                                                                                                                    
wave_deviation_k=wave_deviation.subs(alpha_x,k_x*lmbda)                                                                                                                                    
wave_deviation_k=wave_deviation_k.subs(alpha_y,k_y*lmbda)                                                                                                                                  
wave_deviation_k                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
######                                                                                                                                                                                     
                                                                                                                                                                                           
derivation_kx=wave_deviation_k.diff(k_x)                                                                                                                                                   
derivation_ky=wave_deviation_k.diff(k_y)                                                                                                                                                   
                                                                                                                                                                                           
derivation_kx                                                                                                                                                                              
                                                                                                                                                                                           
derivation_ky                                                                                                                                                                              
                                                                                                                                                                                           
########                                                                                                                                                                                   
                                                                                                                                                                                           
derivation_kx_kx=derivation_kx.diff(k_x).doit()                                                                                                                                            
derivation_kx_ky=derivation_kx.diff(k_y).doit()                                                                                                                                            
derivation_ky_kx=derivation_ky.diff(k_x).doit()                                                                                                                                            
derivation_ky_ky=derivation_ky.diff(k_y).doit() # just do it! doit() - Evaluates unevaluated derivative (without this sp.lambdify() in get_nd_der_basis() failed!)                         


# In[ ]:


series


# In[ ]:


wave_deviation


# In[ ]:


wave_deviation.expand().coeff(aberrations_real_coefs[1,2,0]) # as_coefficient() je pouze na podeleni expression mym koeficientem, coeff je potreba..


# In[ ]:


aberrations_real_coefs[1,2,0]


# In[ ]:


derivation_kx


# In[ ]:


###########################


# In[ ]:


from collections import defaultdict


# In[ ]:


aberrations_real_coefs


# In[ ]:


aberrations_de=defaultdict(int)
for i in aberrations_real_coefs.values():
    aberrations_de[i]=0
aberrations_de[aberrations_real_coefs[(1,0,0)]]=aberrations["C10"] #!!!!!!!! TODO
aberrations_de[aberrations_real_coefs[(3,0,0)]]=aberrations["C30"] #!!!!!!!! TODO
aberrations_de[aberrations_real_coefs[(1,2,0)]]=aberrations["C12"] #!!!!!!!! TODO
aberrations_de[aberrations_real_coefs[(2,3,0)]]=aberrations["C23"] #!!!!!!!! TODO
aberrations_de=dict(aberrations_de)


# In[ ]:


aberrations


# In[ ]:


aberrations_de


# In[ ]:


shifts=sp.lambdify((k_x,k_y,lmbda),derivation_kx.evalf(subs=aberrations_de))


# In[ ]:


derivation_kx.evalf(subs=aberrations_de)


# In[ ]:


shifts(0.1,0,probe.wavelength)


# In[ ]:


derivation_kx.evalf(subs={aberrations_real_coefs[(1,0,0)]:0}).expand()


# In[ ]:


hmm=k_x+k_y


# In[ ]:


hmm.evalf(subs={k_y:0 },chop=True) # hmm?


# In[ ]:


sp.lambdify(k_x,hmm.evalf(subs={k_y: 0.0}))(5)


# In[ ]:


alphas=np.linspace(-semiangle_cutoff*1e-3,semiangle_cutoff*1e-3,100)
dalpha=alphas[1]-alphas[0]
kx=alphas*1/probe.wavelength
y=np.diff(probe.ctf.evaluate_chi(alphas,0))/dalpha/(2*np.pi/probe.wavelength)

plt.figure()
plt.plot(kx[1:],y,"-",label="true")
y_teor=np.real(shifts(kx,0,probe.wavelength))*1/probe.wavelength/(2*np.pi/probe.wavelength) # 1/probe.wavelength je ze zmeny promennych dk/dalpha
plt.plot(kx,y_teor,"--",label="teor")
plt.legend()


# In[ ]:


######################


# In[ ]:


der_kx_kx=sp.lambdify((k_x,k_y,lmbda),derivation_kx_kx.evalf(subs=aberrations_de),"numpy")
der_kx_ky=sp.lambdify((k_x,k_y,lmbda),derivation_kx_ky.evalf(subs=aberrations_de),"numpy")
der_ky_kx=sp.lambdify((k_x,k_y,lmbda),derivation_ky_kx.evalf(subs=aberrations_de),"numpy")
der_ky_ky=sp.lambdify((k_x,k_y,lmbda),derivation_ky_ky.evalf(subs=aberrations_de),"numpy")


# In[ ]:


plt.figure()
y=np.diff(np.diff(probe.ctf.evaluate_chi(alphas,0)))/dalpha**2/(2*np.pi/probe.wavelength)
plt.plot(kx[1:-1],y,"-",label="true")
y_teor=np.real(der_kx_kx(kx,0,probe.wavelength))*1/probe.wavelength**2/(2*np.pi/probe.wavelength) #1/probe.wavelength je ze zmeny promennych dk/dalpha
plt.plot(kx,y_teor,"--",label="teor")


# In[ ]:


#######################


# In[ ]:


2048/(reciprocal_oversampling*2)


# In[ ]:


measurement.shape


# In[ ]:


measurement_noisy.shape


# In[ ]:


#?measurement_decimated = decimate_measurement(measurement,reciprocal_oversampling*2)


# In[ ]:


#measurement_decimated.shape


# In[ ]:


#?measurement_crop_bf=crop(measurement_decimated,extent=(semiangle_cutoff*2,semiangle_cutoff*2))

measurement_decimated = decimate_measurement(measurement_noisy,4)
measurement_crop_bf=crop(measurement_decimated,extent=(semiangle_cutoff*2,semiangle_cutoff*2))


# In[ ]:


measurement_crop_bf.shape


# In[ ]:


measurement_crop_bf[0].show()


# In[ ]:


from abtem.utils import fft_shift


# In[ ]:


img_1_=measurement_crop_bf.array.reshape(scan_gpts,scan_gpts,*measurement_crop_bf.shape[-2:])[:,:,3,3]
shiftx = 0.321
shifty = 0.123
img_2_=np.real(fft_shift(img_1_,np.array([shiftx,shifty])))
img_2_-=np.min(img_2_)
#img_2_=measurement_crop_bf.array.reshape(scan_gpts,scan_gpts,*measurement_crop_bf.shape[-2:])[:,:,32,16]


# In[ ]:


plt.figure()
plt.imshow(img_1_)


# In[ ]:


plt.figure()
plt.imshow(img_2_)


# In[ ]:


img_1=img_1_
img_2=img_2_


# In[ ]:


from py4DSTEM.process.utils.cross_correlate import align_images_fourier
from skimage.registration import phase_cross_correlation


# In[ ]:


get_ipython().run_cell_magic('time', '', 'G_ref=np.fft.fft2(img_1)\nG=np.fft.fft2(img_2)\nasdf=[]\nasdf_2=[]\nerrors_2=[]\nx=range(8,512)\nfor i in x:\n    asdf.append(align_images_fourier(G_ref,G,upsample_factor=i,device="cpu")[1])\n    shift, error, diffphase = phase_cross_correlation(img_1, img_2, upsample_factor=i)\n    asdf_2.append(shift[1])\n    errors_2.append(error)\nplt.figure()\nplt.plot(list(x),asdf,"x",label="py4DSTEM")\nplt.plot(list(x),asdf_2,"x",label="scipy")\nplt.axhline(y=-shifty)\nplt.legend()\n')


# In[ ]:


########################################


# In[ ]:


measurement_crop_bf_array=measurement_crop_bf.array.reshape(scan_gpts,scan_gpts,*measurement_crop_bf.shape[-2:])


# In[ ]:


measurement_crop_bf_array.shape


# In[ ]:


from abtem.utils import spatial_frequencies
diffpat_gpts=measurement_crop_bf_array.shape[-2:]
diffpat_sampling=measurement_crop_bf.calibrations[-1].sampling # reciprocal sampling in mrad
kx, ky = get_spatial_frequencies(diffpat_gpts,diffpat_sampling,probe.wavelength)
#kx = xp.asarray(kx)                                                                                                                                                                    
#ky = xp.asarray(ky) 


# In[ ]:


assert True


# In[ ]:


#%matplotlib inline
plt.figure()
plt.imshow(ky)


# In[ ]:


probe.show()


# In[ ]:


#chck
print(probe.sampling)
print(1/(512*measurement.calibrations[-1].sampling*1e-3*1/probe.wavelength))


# In[ ]:


plt.figure()
mean_image = np.mean(measurement_crop_bf_array,axis=(0,1))
mask = mean_image>mean_image.max()/1e4
plt.imshow(mask)


# In[ ]:


upsampling_factor = 50


# In[ ]:


measured_shifts_scipy = measure_shifts(measurement_crop_bf_array, mask, upsampling_factor=upsampling_factor, reg_tool='scipy')
measured_shifts = measure_shifts(measurement_crop_bf_array, mask,upsampling_factor=upsampling_factor, reg_tool='py4DSTEM')


# In[ ]:


N = measurement_crop_bf_array.shape[-1]
for i in range(N):
    asdf = measurement_crop_bf_array[:,:,i,N//2]
    plt.figure()
    plt.imshow(asdf.T,origin="lower")
    plt.savefig("figs/figs_sch/{:04.0f}.pdf".format(i))
    plt.close()
    


# In[ ]:


#%matplotlib widget
plt.figure()
measured_shifts_apr=measured_shifts_scipy*scan_sampling/(diffpat_sampling*1e-3) # (Delta shift in angstrom) / (delta alpha in rad)
#measured_shifts_apr=measured_shifts*scan_sampling/(diffpat_sampling*1e-3) # (Delta shift in angstrom) / (delta alpha in rad)
measured_shifts_apr_x_x=measured_shifts_apr[:,:,0,0]
plt.imshow(measured_shifts_apr[:,:,0,0]) 


# In[ ]:


#%matplotlib widget
plt.figure()
teor_shifts_apr=np.real(der_kx_kx(kx,ky,probe.wavelength))*1/probe.wavelength**2/(2*np.pi/probe.wavelength)
#plt.plot(kx,y_teor,"--",label="teor")
plt.imshow(teor_shifts_apr)
plt.show()


# In[ ]:


measured_shifts_apr.shape


# In[ ]:


plt.figure()
plt.plot(probe.ctf.evaluate_chi(kx[:,N//2]*1/probe.wavelength,0))
plt.show()


# In[ ]:


plt.figure()
plt.plot(measured_shifts_apr[:,N//2,0,0],".")


# In[ ]:


#%matplotlib widget


# In[ ]:


plt.figure()
plt.plot(teor_shifts_apr[:,N//2],label="teor")
plt.plot(measured_shifts_apr_x_x[:,N//2],label="measured")
plt.legend()


# In[ ]:


teor_shifts_apr[0,8]


# In[ ]:


#################################xx FITOVANI


# In[ ]:


k=np.sqrt(kx**2+ky**2)
mask_fit=k<kx.max()*3/5 # 4/5
plt.figure()
plt.imshow(mask_fit)
kx_fit=kx[mask_fit]
ky_fit=ky[mask_fit]


# In[ ]:


aberrations_real_coefs


# In[ ]:


expr_120=derivation_kx_kx.expand().coeff(aberrations_real_coefs[1,2,0]) # as_coefficient() je pouze na podeleni expression mym koeficientem, coeff je potreba..
expr_120


# In[ ]:


expr_120_fun=sp.lambdify((k_x,k_y,lmbda),expr_120.evalf(subs=aberrations_de),'numpy')
expr_120_fun


# In[ ]:


nd_derivatives = np.array(                                                                                                                                                                 
                        [[derivation_kx_kx,derivation_kx_ky],                                                                                                                              
                        [derivation_ky_kx,derivation_ky_ky]]) # so that [0,0] is kx_kx [0,1] is kx_ky [1,0] is ky_kx and [1,1] is ky,ky                                                    
wave_deviation_nd_der_basis = get_nd_der_basis(aberrations_real_coefs,nd_derivatives,probe.wavelength,diffpat_gpts,diffpat_sampling,k_x,k_y,lmbda)   


# In[ ]:


wave_deviation_nd_der_basis.reshape(wave_deviation_nd_der_basis.shape[0],-1).shape


# In[ ]:


measured_shifts_apr.reshape(-1).shape


# In[ ]:


measured_shifts_apr[mask_fit,:,:].shape


# In[ ]:


wave_deviation_nd_der_basis[:,mask_fit,:,:].shape


# In[ ]:


mask_fit.shape


# In[ ]:


aberrations


# In[ ]:


aberrations_coefs, res = np.linalg.lstsq(
                wave_deviation_nd_der_basis[:,mask_fit,:,:].reshape(wave_deviation_nd_der_basis.shape[0],-1).T, measured_shifts_apr.transpose((0,1,2,3))[mask_fit,:,:].reshape(-1), rcond=None
            )[:2]


# In[ ]:


aberrations_coefs


# In[ ]:


#z baze odhadnout jaka je chyba prislusi dane aberaci: 
#podle toho jak se meni posun druha derivace vlnoplochy dx/dalpha=Defocus*1 
#abych dostal ekvivalentni posun prenasobym dalpha: dx=Defocus*1 * diffpat_sampling 
# pro pripad ostatnich aberaci bude misto *1 vystupovat jiny vyraz (baze) 
# vezmu max baze (basis_max) (nebo nejaky median) a to vezmu na odhad presnosti prislusne aberace: 
# Delta_aberration = scan_sampling/basis_max/diffpat_sampling 
# scan_sampling se da vzit mensi podle toho jaka ja upsampling presnost.

#Pozor vysledek neni chyba ale jakysi odhad nejistoty


# In[ ]:


diffpat_sampling*1e-3


# In[ ]:


basis_max=np.quantile(np.abs(wave_deviation_nd_der_basis)[:,mask_fit,:,:].reshape(wave_deviation_nd_der_basis.shape[0],-1),axis=(1),q=0.75)
expected_sensitivity = scan_sampling/basis_max/(diffpat_sampling*1e-3)*1/(upsampling_factor)
expected_sensitivity #Pozor vysledek neni chyba ale jakysi odhad nejistoty


# In[ ]:


np.log10(22e3)


# In[ ]:


np.round(22e3,int(np.log10(22e3)))


# In[ ]:


aberrations_real_coefs.keys()


# In[ ]:


from uncertainties import unumpy as unp
res=unp.uarray(aberrations_coefs,expected_sensitivity)
for i,_ in enumerate(res):
    print('{}: {:.1u}'.format(list(aberrations_real_coefs.keys())[i],res[i]))


# In[ ]:


fitted_shifts_apr=np.sum( wave_deviation_nd_der_basis*aberrations_coefs[:,None,None,None,None],axis=0)


# In[ ]:


plt.figure()
plt.imshow(fitted_shifts_apr[:,:,0,0]*mask_fit)
plt.figure()
plt.imshow(measured_shifts_apr[:,:,0,0]*mask_fit)
y_=np.real(der_kx_kx(kx,ky,probe.wavelength))*1/probe.wavelength**2/(2*np.pi/probe.wavelength)
plt.figure()
plt.imshow(y_)


# In[ ]:


np.array(list(aberrations_real_coefs.keys()))


# In[ ]:


dict(zip(list(aberrations_real_coefs.keys()),aberrations_coefs))


# In[ ]:


aberrations_fitted=aberration_dict_polar(dict(zip(list(aberrations_real_coefs.keys()),aberrations_coefs)),np.array(list(aberrations_real_coefs.keys())))
aberrations_fitted


# In[ ]:


Probe(energy=energy,semiangle_cutoff=semiangle_cutoff,**aberrations_fitted,sampling=sampling,extent=extent).show()


# In[ ]:


probe.show()


# In[ ]:


aberrations


# In[ ]:


diffpat_sampling = measurement_crop_bf.calibrations[-1].sampling
asdf=example(measurement_crop_bf_array,diffpat_sampling,scan_sampling,probe.wavelength)


# In[ ]:


#exec(open("./aberrations_fit_utils.py").read())


# In[ ]:




