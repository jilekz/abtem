#Py4DSTEM: 
#abtem: 
import os
os.environ["MKL_NUM_THREADS"] = "16"
os.environ["NUMEXPR_NUM_THREADS"] = "16"
os.environ["OMP_NUM_THREADS"] = "16"

import time
import numpy as np
import matplotlib.pyplot as plt
from abtem import *

import scipy.constants as c
from abtem.reconstruct import invms

from abtem.custom import semiangle_2_sampling_extent,semiangle_gpts_2_extent
from abtem.scan import PositionScan

import cupy as cp
import h5py
import pathlib

import matplotlib
from abtem.custom import incoherent_probe, get_probe_radius
from ase.build import nanotube
from abtem.custom import crop
from abtem.noise import poisson_noise
from abtem.custom import decimate_measurement


from ase import Atoms
def get_potential_from_atoms(atoms_in, extent, sampling):
    atoms=atoms_in.copy()
    vacuum_offset=0
    atoms.center(vacuum=0)
    atoms_extent=atoms.cell.diagonal() #x,y,z
    
    vacuum=(extent-atoms_extent)/2 #x,y,z
    atoms.center(vacuum=vacuum[0]+vacuum_offset,axis=(0))
    atoms.center(vacuum=vacuum[1]+vacuum_offset,axis=(1))

    atoms.center(vacuum=2,axis=(2))

    potential = Potential(FrozenPhonons(atoms, sigmas=0.06, num_configs=1), #atoms_p,
                      sampling=sampling,
                      slice_thickness=2,
                      projection='infinite',
                      parametrization='kirkland')
    return(atoms,potential)


def gen_rnd_tubes(reciprocal_oversampling,N_=50,extent=160,seed=2):
    N=N_*reciprocal_oversampling**2
    np.random.seed(seed)
    #cnt_positions=np.random.uniform(-80,80,(N,2))
    cnt_positions=(np.random.beta(2.5,2.5,(N,2))-1)*extent*reciprocal_oversampling
    #cnt_positions[:,0]*=np.random.uniform(0,0.1,N)
    cnt_rotations_y=np.random.uniform(-60,+60,N)
    cnt_rotations_z=np.random.uniform(0,360,N)

    atoms=Atoms()
    for i in range(N):
        cnt = nanotube(5, 4, length=1)
        cnt.rotate(90+cnt_rotations_y[i],"y",center='COM')
        cnt.rotate(cnt_rotations_z[i],"z",center='COM')
        cnt.center(vacuum=1,about=0)
        cnt.translate(np.array([*cnt_positions[i],0]))
        atoms += cnt
    atoms.center(vacuum=2)
    return(atoms)

if __name__ == "__main__":
    s=time.time()
    atoms = gen_rnd_tubes(1)
    atoms.set_chemical_symbols(["Ti" for i in range(len(atoms))])
    show_atoms(atoms,plane="xy")
    _,potential=get_potential_from_atoms(atoms,extent=atoms.cell[0,0]*2,sampling=1)
    potential.project().show()
    plt.show()
    e=time.time()

