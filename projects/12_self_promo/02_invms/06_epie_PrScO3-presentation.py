#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#abtem version: 9752fdc7d61e8f278c561239d5d6803e55e161a0


# # invMS
# multislice ptychography in comparison with single slice ptychography converges much more slowly with increasing number of slices in the sample.. so that increasing convergence parameters alpha and beta is required (parameters alpha=0.2 and beta=0.2 with single slice ptychography took 23 s and with two slices took about twice as much time but the probe was not yet converged)
# 
# 

# In[ ]:


get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np
import matplotlib.pyplot as plt
#from skimage import data

from abtem import GridScan, PixelatedDetector, Potential, Probe, show_atoms, SMatrix, AnnularDetector
#from abtem.reconstruct import epie
from ase.build import mx2
from abtem.structures import orthogonalize_cell
from abtem.measure import bandlimit, Measurement
from abtem.temperature import FrozenPhonons
from abtem.custom import incoherent_scan_mc_fp, get_gaussian_spread


# In[ ]:


from ase.visualize import view
from abtem import PotentialArray
from abtem.utils import energy2sigma
from pathlib import Path


# In[ ]:


from abtem.reconstruct import invms


# PrScO3 using the ePIE algorithm
# 
# We simulate a 4D-STEM dataset of MoS2 with a defect and perform a ptychographic reconstruction using the [ePIE](https://doi.org/10.1016/j.ultramic.2009.05.012) algorithm.

# In[ ]:


import matplotlib
matplotlib.rcParams["figure.figsize"] = (20, 20)
matplotlib.rcParams['lines.linewidth'] = 2
font = {'weight' : 'bold',
        'size'   : 22}
matplotlib.rc('font', **font)


# ## Simulation

# In[ ]:


from ase.io import read
prsco3 = read('./PrScO3_mp-559756_primitive.cif')


# In[ ]:


#atoms=prsco3*(3,3,2)
#atoms=prsco3*(3,3,20)
#view(atoms)


# In[ ]:


##pro atoms = prsco3*(4,4,2) #1nm
#del atoms[221]
#del atoms[221]
#del atoms[201]
#del atoms[201]

atoms = prsco3*(4,4,20) #16nm
#del atoms[2381]
#del atoms[2381]
#del atoms[2361]
#del atoms[2361]

#atoms = prsco3*(4,4,10) #8nm
#del atoms[1181]
#del atoms[1181]
#del atoms[1161]
#del atoms[1161]

#view(atoms)


# In[ ]:


#potential.project().show()


# In[ ]:


get_ipython().run_line_magic('matplotlib', 'inline')
from ase.visualize.plot import plot_atoms
fig, axes = plt.subplots(1, 1, figsize=(10, 5))
plot_atoms(atoms, axes, radii=0.8, rotation=('-70x,-10y,0z'),show_unit_cell=0)
#axes.set_axis_off()
#plt.savefig("atoms.png")
#plt.show()


# In[ ]:


show_atoms(atoms)


# In[ ]:


#proj_pot = potential.project().array
#potential = PotentialArray(proj_pot[None], [80], sampling=potential.sampling)


# How long will single frozen phonon configuration take?
# * on laptop with 2 cores with device='cpu' it takes cca 3.5 min
# * on lenc with 36 cores with device='cpu' it takes cca 2 min with utilasition of all cores (including virtual) (according to this abtem is not yet optimised to utilise such computing power)
# * on lenc with 36 cores with device='gpu' it takes cca 40 sec with utilisation of all cores and gpu 

# In[ ]:


from abtem.custom import semiangle_2_sampling_extent,semiangle_gpts_2_extent
def B2sigma(B):
    return(np.sqrt(B/8/np.pi**2)) # tady byl bug sqrt(3) krat vetsi jsem to mel! a to nema, viz abtem issues.
def get_potential(atoms_in,extent,sampling):                                                                                                                                                                                                                                                                                   
    atoms=atoms_in.copy()                                                                                                                                                                                                                                                                                                      
    vacuum_offset=0                                                                                                                                                                                                                                                                                                            
    atoms.center(vacuum=0) # as atoms.center() will always set vacuum to given value, it is the best to zero it first                                                                                                                                                                                                          
    atoms_extent=atoms.cell.diagonal() #x,y,z                                                                                                                                                                                                                                                                                  
                                                                                                                                                                                                                                                                                                                               
                                                                                                                                                                                                                                                                                                                               
    vacuum=(extent-atoms_extent)/2 #x,y,z                                                                                                                                                                                                                                                                                      
    print(vacuum)                                                                                                                                                                                                                                                                                                              
    print(atoms.cell.diagonal())                                                                                                                                                                                                                                                                                               
    atoms.center(vacuum=vacuum[0]+vacuum_offset,axis=(0))                                                                                                                                                                                                                                                                      
    print(atoms.cell.diagonal())                                                                                                                                                                                                                                                                                               
    atoms.center(vacuum=vacuum[1]+vacuum_offset,axis=(1))                                                                                                                                                                                                                                                                      
    print(atoms.cell.diagonal())                                                                                                                                                                                                                                                                                               
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
    B_Pr = 0.88
    B_Sc = 0.70
    B_O = 1.00

    sigma_Pr = B2sigma(B_Pr)
    sigma_Sc = B2sigma(B_Sc)
    sigma_O = B2sigma(B_O)

    sigmas = {'Pr': sigma_Pr, 'Sc': sigma_Sc, 'O': sigma_O} # standard deviations of thermal vibrations
    num_configs = 20 # number of frozen phonon configurations

    frozen_phonons = FrozenPhonons(atoms, num_configs=num_configs, sigmas=sigmas)
    potential_p = Potential(frozen_phonons,
                      sampling=sampling,                                                                                                                                                                                                                                                                                       
                      slice_thickness=2,                                                                                                                                                                                                                                                                                       
                      projection='infinite',                                                                                                                                                                                                                                                                                   
                      parametrization='kirkland')   
    return potential_p,atoms                                                    


# In[ ]:


get_ipython().run_cell_magic('time', '', 'energy=300e3\nCc = 2e-3/1e-10 # In Angstrom\nfocal_spread=Cc*(0.4/energy)\ntemporal_sigma = focal_spread/np.sqrt(2)\n\n\ndefocus=-200\nsemiangle_cutoff=21.4\npx_num=128\n\n#B_r = ?\nI = 33e-12\nspatial_sigma = 0#get_gaussian_spread(semiangle_cutoff,energy,B_r,I)\n\nangular_extent=semiangle_cutoff*2.5\nreciprocal_oversampling = 1                                                                                                                                                                                                                                                                                               \ndirect_oversampling = 4                                                                                                                                                                                                                                                                                                    \nsampling,extent = semiangle_2_sampling_extent(angular_extent,px_num,energy=energy,direct_oversampling=direct_oversampling,reciprocal_oversampling=reciprocal_oversampling)        \npotential,_ = get_potential(atoms,extent,sampling)\n\nprobe = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy, defocus=defocus*1, Cs=0,focal_spread=focal_spread, device=\'gpu\')\nprobe.grid.match(potential)\n\n\ncenter=np.array(potential.extent)/2\noffset=np.array([16.1,16.6])/2 #== np.array([35,36])*0.46/2 number of steps*sampling/2\n\n#gridscan = GridScan((0,0), np.array(potential.extent), sampling=0.46)\ngridscan = GridScan(center-offset,center+offset, sampling=0.41)\npositions_grid = gridscan.get_positions()\npositions = positions_grid + np.random.uniform(-0.15,0.15,size=positions_grid.shape)\n\n#nam="16_nophonon"\n#filename="04_data/"+nam+".hdf5"\n\ndetector = PixelatedDetector(None,mode="intensity")#max_angle=256*0.43609418678539313*2/2*0.98) # from parameters in paper times 0.99\n\nmeasurement_mc=incoherent_scan_mc_fp(probe,positions,detector,potential,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma,max_batch_scan=1500)\n#measurement = probe.scan(gridscan, [detector], potential, pbar=True)\n')


# In[ ]:


measurement= measurement_mc


# In[ ]:


potential.project().show()


# In[ ]:


get_ipython().run_line_magic('matplotlib', 'inline')
measurement[0].show(power=0.35,cmap="hot")
plt.xlim(-50,50)
plt.ylim(-50,50)
plt.axis("off")
#plt.savefig("cbed.png")


# In[ ]:


1/(9e-3*1/(probe.wavelength))


# In[ ]:


#measurement = Measurement.read(filename)


# In[ ]:


np.shape(gridscan.get_positions())


# In[ ]:


#energy=300e3
#S = SMatrix(semiangle_cutoff=21.4, energy=energy, defocus=-200, Cs=0, expansion_cutoff=25, device='cpu')
#S.grid.match(potential)


#center=np.array(potential.extent)/2
#offset=np.array([16.1,16.6])/2 #== np.array([35,36])*0.46/2 number of steps*sampling/2

##gridscan = GridScan((0,0), np.array(potential.extent), sampling=0.46)
#gridscan = GridScan(center-offset,center+offset, sampling=0.46)                    
#detector = PixelatedDetector()
#measurement = S.scan(gridscan, [detector], potential, pbar=False)


# In[ ]:


#%matplotlib inline
plt.figure()
proj_pot = potential.project().array.astype(np.complex64) # in projected potential in eV*angstrom
interaction_parameter = energy2sigma(energy)
phase_shift=proj_pot*interaction_parameter 
plt.imshow(np.real(phase_shift).T, origin='lower', cmap='gray')
plt.colorbar()
plt.axis("off")

plt.title("Projected potential")
#plt.savefig("06_figs/proj_pot_single_phonon.pdf")


# In[ ]:


probe.show()
plt.title("Probe")
#plt.savefig("06_figs/probe.pdf")


# In[ ]:


probe_plot = Probe(semiangle_cutoff=21.4, energy=energy,defocus=defocus,extent=(22.5,22.5),gpts=potential.gpts)


# In[ ]:


plt.figure()
plot_atoms(atoms,show_unit_cell=0)
plt.axis("off")


# In[ ]:


plt.figure()
plot_atoms(atoms,show_unit_cell=0)


# In[ ]:


potential.project().show()


# In[ ]:


probe_plot.show(cmap="hot")
#plt.axis("off")
#plt.savefig("probe.png")


# In[ ]:


probe.angular_sampling


# In[ ]:


dx=(potential.extent[0]/potential.gpts[0])
max_a=probe.wavelength/dx
max_a


# In[ ]:


probe.wavelength/probe.sampling


# In[ ]:


probe.cutoff_scattering_angles


# In[ ]:


probe.wavelength/probe.sampling/2*2/3


# ## Reconstruction

# appling noise to simulated diffraction data

# In[ ]:


get_ipython().run_cell_magic('time', '', 'from abtem.utils import fft_interpolate_2d\nfrom abtem.noise import poisson_noise\nimport scipy.constants as c\n#band_limited_measurment = fft_interpolate_2d(measurement,[256,256]) # toto je strasne pomale a nenazrane!\n\nmeasurement_intensity = measurement\n\nel=I*1e-3/c.e\nmeasurement_noisy = poisson_noise(measurement_intensity/np.mean(np.sum(measurement_intensity.array,(-2,-1))),pixel_area=1, dose=el)/el\n#band_limited_measurment = bandlimit(measurement_noisy, 80)\n\n#band_limited_measurment[0,0].show(power=1)\n')


# Making probe guess, with given aperture semiangle and beam energy. 

# In[ ]:


probe_guess = Probe(semiangle_cutoff=21.4, energy=energy,defocus=defocus,extent=40,gpts=256)


# In[ ]:


probe_guess.show()
plt.title("Initial probe")
#plt.savefig("06_figs/probe_guess.pdf")


# In[ ]:


print(probe_guess.sampling)


# In[ ]:


measurement_noisy[5].show()


# In[ ]:


get_ipython().run_cell_magic('time', '', "invms_slices=8\nreconstructions_full_invms = invms(measurement_noisy, probe_guess, positions=positions , alpha=0.5, fac=1, beta=1, slices=invms_slices, slice_thickness=20,  maxiter=8, return_iterations=True, fix_com=True,device='gpu')\n#reconstructions_full_invms = invms(measurement_resampled_cropped, probe_guess, positions=positions_centered , alpha=1, fac=0.2, beta=1, slices=invms_slices, slice_thickness=20,  maxiter=8, return_iterations=True, fix_com=True,device='gpu')\n")


# In[ ]:


get_ipython().run_line_magic('matplotlib', 'inline')
plot_every = 2

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions_full_invms[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions_full_invms[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions_full_invms[0][-1][4].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions_full_invms[1][j][-1].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()


# In[ ]:





# In[ ]:


ptp=np.zeros((maxiter,invms_slices))
for i_iter in range(maxiter):
    for i_slice in range(invms_slices):
        ptp[i_iter,i_slice]=np.ptp(np.angle(reconstructions_full_invms[0][i_iter][i_slice].array))
    
for i in range(invms_slices):
    plt.plot(ptp[:,i])


# In[ ]:


plt.figure()
plt.imshow(np.angle(reconstructions_full_invms[0][4][4].array).T, origin='lower', cmap='gray')
plt.axis("off")
#plt.xlim(150,400)
#plt.ylim(150,400)
#plt.savefig("phase.png")


# In[ ]:


#%%time
#invms_slices=8
#reconstructions_full_invms_modes = invms(measurement_noisy, probe_guess,modes = 1, positions=positions, k_modes=0, alpha=0.2, fac=1, beta=1, slices=invms_slices, slice_thickness=10,  maxiter=8*2, return_iterations=True, fix_com=True,device='gpu')


# In[ ]:


#%matplotlib inline
#plot_every = 4
#
#fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions_full_invms_modes[0]) / plot_every)), figsize=(20,20))
#
#for i, j in enumerate(range(0, len(reconstructions_full_invms_modes[0]), plot_every)):
#    axes[0,i].imshow(np.angle(reconstructions_full_invms_modes[0][j][0].array).T, origin='lower', cmap='gray')
#    axes[1,i].imshow(np.abs(reconstructions_full_invms_modes[1][j][0].array).T ** 2, origin='lower', cmap='gray')
#    for ax in (axes[0,i], axes[1,i]):
#        ax.axis('off')
#    
#plt.tight_layout()


# In[ ]:


#plt.imshow(np.angle(reconstructions_full_invms_modes[0][-1][1].array).T, origin='lower', cmap='gray')


# In[ ]:


#plt.imshow(np.angle(reconstructions_full_invms_modes[0][-1][0].array).T, origin='lower', cmap='gray')


# In[ ]:


reconstructions_single_slice = invms(measurement_noisy, probe_guess, positions = positions, alpha=0.5, fac=1, beta=1, maxiter=8, return_iterations=True, fix_com=True,device='gpu')


# In[ ]:


#%matplotlib inline
plot_every = 4

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions_single_slice[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions_single_slice[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions_single_slice[0][j][0].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions_single_slice[1][j][0].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()


# In[ ]:


obj=reconstructions_full_invms[0][-1]

extent=(0,probe_guess.extent[0],0,probe_guess.extent[1])
#img=np.ones_like(obj[-1].array)
img=np.ones_like(obj[-1].array,dtype=float)
for i in range(np.shape(obj)[0]):
    img+=np.angle(obj[i].array)
#    img*=obj[i].array
#plt.imshow(np.angle(img).T, origin='lower', cmap='gray')
plt.imshow(img.T, origin='lower', cmap='gray',extent=extent)
#plt.title("multislice projected phase")
plt.xlabel(r"$x [\mathrm{\AA}]$")
plt.ylabel(r"$y [\mathrm{\AA}]$")
plt.colorbar()
plt.tight_layout()
plt.xlim(7.5,17.5)
plt.ylim(7.5,17.5)
#plt.savefig("/home/jilek/abtem/projects/12_self_promo/02_invms/16nm_invms_zoom.pdf")


# In[ ]:


obj=reconstructions_single_slice[0][-1]
#img=np.ones_like(obj[-1].array)
img=np.ones_like(obj[-1].array,dtype=float)
for i in range(np.shape(obj)[0]):
    img+=np.angle(obj[i].array)
#    img*=obj[i].array
#plt.imshow(np.angle(img).T, origin='lower', cmap='gray')
plt.imshow(img.T, origin='lower', cmap='gray',extent=extent)
#plt.title("Single slice phase")
plt.xlabel(r"$x [\mathrm{\AA}]$")
plt.ylabel(r"$y [\mathrm{\AA}]$")
#plt.savefig("06_figs/"+nam+"nm_poisson_chromatic_"+str(invms_slices)+"slices.pdf")
plt.colorbar()
plt.tight_layout()
plt.xlim(7.5,17.5)
plt.ylim(7.5,17.5)
#plt.savefig("/home/jilek/abtem/projects/12_self_promo/02_invms/16nm_single_slice_zoom.pdf")


# In[ ]:


obj=reconstructions_full_invms[0][-1]

extent=(0,probe_guess.extent[0],0,probe_guess.extent[1])
#img=np.ones_like(obj[-1].array)
img=np.ones_like(obj[-1].array,dtype=float)
for i in range(np.shape(obj)[0]):
    img+=np.angle(obj[i].array)
#    img*=obj[i].array
#plt.imshow(np.angle(img).T, origin='lower', cmap='gray')
plt.imshow(img.T, origin='lower', cmap='gray',extent=extent)
#plt.title("multislice projected phase")
plt.xlabel(r"$x [\mathrm{\AA}]$")
plt.ylabel(r"$y [\mathrm{\AA}]$")
plt.colorbar()
plt.tight_layout()


# In[ ]:


#%matplotlib widget
import ipywidgets
def dummy_plot(i,j):
    print("A")
    plt.imshow(np.angle(reconstructions_full_invms[0][i][j].array).T, origin='lower', cmap='gray')
    print("B"+str(i)+str(j))
#    plt.imshow(np.abs(reconstructions_full_invms[1][i].array).T ** 2, origin='lower', cmap='gray')

    #plt.title("Reconstructed phase of the object")
    
ipywidgets.interact(dummy_plot, i=(0,7),j=(0,7,1))


# In[ ]:


import plotly.express as px


data = reconstructions_full_invms[0][-1]
img = np.zeros((len(data),*np.shape(data[0])))
for i,measu in enumerate(data):
    img[i]=np.angle(measu.array).T

#img=np.concatenate((img,)*100)
fig = px.imshow((img+np.pi/2),height=1000, animation_frame=0, binary_format="jpeg" ,binary_string=True, labels=dict(animation_frame="slice"),binary_compression_level=9)
fig.show()
#with open('true.html', 'w') as f:
#    f.write(fig.to_html(full_html=False, include_plotlyjs=True))
#f.close()


# In[ ]:




