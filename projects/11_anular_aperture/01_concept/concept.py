#!/usr/bin/env python
#|abtem version: 8fb96d40830c8460673b62dc795e7240a4c511d8
import os
os.environ["MKL_NUM_THREADS"] = "2"
os.environ["NUMEXPR_NUM_THREADS"] = "2"
os.environ["OMP_NUM_THREADS"] = "2"


import numpy as np
import matplotlib.pyplot as plt
from abtem import *

from abtem.noise import poisson_noise
import scipy.constants as c
from abtem.scan import PositionScan
from abtem.reconstruct import invms
from abtem.measure import bandlimit
from abtem.aperture import BullseyeAperture

from abtem.custom import get_gaussian_spread
from abtem.custom import get_radiation_dose
from abtem.custom import incoherent_scan
from abtem.custom import incoherent_scan_mc
from abtem.custom import sample_probe
from abtem.custom import incoherent_scan_mc_fp
from abtem.custom import incoherent_probe, get_probe_radius
from abtem.custom import crop
from abtem.custom import semiangle_2_sampling_extent,semiangle_gpts_2_extent
#from utils import get_potential_from_image
from abtem.custom import decimate_measurement
import cupy as cp

from ase.build import graphene
from abtem.structures import orthogonalize_cell
from ase.build import nanotube

#import matplotlib
import pathlib
#matplotlib.rcParams["figure.figsize"] = (10, 10)
#matplotlib.rcParams['lines.linewidth'] = 2
#font = {'weight' : 'bold',
#                'size'   : 15}
#matplotlib.rc('font', **font)

def get_potential(atoms_in,extent,sampling):
    atoms=atoms_in.copy()
    vacuum_offset=0
    atoms.center(vacuum=0) # as atoms.center() will always set vacuum to given value, it is the best to zero it first
    atoms_extent=atoms.cell.diagonal() #x,y,z
    
    
    vacuum=(extent-atoms_extent)/2 #x,y,z
    atoms.center(vacuum=vacuum[0]+vacuum_offset,axis=(0))
    atoms.center(vacuum=vacuum[1]+vacuum_offset,axis=(1))
    
    potential_p = Potential(FrozenPhonons(atoms, sigmas=.06, num_configs=1), #atoms_p,
                      sampling=sampling,
                      #gpts=round(extent/sampling),
                      slice_thickness=2,
                      projection='infinite',
                      parametrization='kirkland')
    return potential_p,atoms


#| 
energy = 200e3
px_size = 100e-6
px_num = 192
camera_length = 2*31.59E-2#31.59e-2#12.2e-2
angular_sampling = px_size/camera_length*1e3 # in mrad
angular_extent = angular_sampling * px_num/2  # only semi angle

binning=1
reciprocal_oversampling=1
direct_oversampling=4
sampling,extent=semiangle_2_sampling_extent(angular_extent,px_num/binning,energy=energy,direct_oversampling=direct_oversampling,reciprocal_oversampling=reciprocal_oversampling)
print(sampling,extent)

atoms = graphene()
atoms = orthogonalize_cell(atoms)
atoms *= (24,16,1)

atoms.center(vacuum=2, axis=2)
atoms.center()

#fig,(ax1,ax2)=plt.subplots(1, 2, figsize=(10,5))
#show_atoms(atoms, ax=ax1)
#show_atoms(atoms, ax=ax2, plane='xz');


atoms_rotated=atoms.copy()
atoms_rotated.rotate(10,"z")
atoms_rotated.center()
atoms_rotated.positions+=np.array([0,0,2])
#fig,(ax1,ax2)=plt.subplots(1, 2, figsize=(10,5))
#show_atoms(atoms_rotated, ax=ax1)
#show_atoms(atoms_rotated, ax=ax2, plane='xz');


atoms_moire=atoms+atoms_rotated
#fig,(ax1,ax2)=plt.subplots(1, 2, figsize=(10,5))
atoms_moire.center(vacuum=2)

#show_atoms(atoms_moire, ax=ax1)
#show_atoms(atoms_moire, ax=ax2, plane='xz');


cnt = nanotube(5, 4, length=1)
cnt.rotate(90,"y",center='COM')
cnt.center(vacuum=1,about=0)
#show_atoms(cnt,plane="xz")
cnt.translate(-cnt.get_center_of_mass()+atoms_moire.get_center_of_mass()+np.array([0,0,3.9]))

cnt2=cnt.copy()
cnt2.rotate(30,"z",center='COM')

cnt3 = nanotube(2, 4, length=1)
cnt3.translate(-cnt3.get_center_of_mass()+atoms_moire.get_center_of_mass()+np.array([0,5,3.9]))
cnt3.rotate(90,"x",center='COM')
#cnt3.rotate(60,"z",center='COM')
#show_atoms(cnt3,plane="xy")

cnt4=cnt.copy()
cnt4.translate(-cnt4.get_center_of_mass()+atoms_moire.get_center_of_mass()+np.array([0,10,3.9]))
cnt5=cnt.copy()
cnt5.translate(-cnt5.get_center_of_mass()+atoms_moire.get_center_of_mass()+np.array([0,10,3.6]))
cnt6=cnt.copy()
cnt6.translate(-cnt6.get_center_of_mass()+atoms_moire.get_center_of_mass()+np.array([0,10,3.3]))
cnt7=cnt.copy()
cnt7.translate(-cnt7.get_center_of_mass()+atoms_moire.get_center_of_mass()+np.array([10,10,3.0]))


atoms_tot=atoms_moire+cnt+cnt2+cnt3+cnt4+cnt5+cnt6+cnt7
atoms_tot.center(vacuum=2)
#show_atoms(atoms_tot)
#plt.show()

#|#Potential

potential_p,_=get_potential(atoms_tot,extent,sampling)
#potential_p.build() <- boze toto nedavat

def run(d_ratio, p_dose = 500, bulls_flag=False,directory=None):
    fig_all,axes_all=plt.subplots(3,2,figsize=(30,30))
    axes_l=axes_all.reshape(-1)

    idx=0

    ax=axes_l[idx];idx+=1
    potential_p.project().show(ax=ax)
    #plt.show()

    #| Probe
    #energy_spread = 0.6
    #Cc = 1.34e-3/1e-10
    #focal_spread = energy_spread/energy * Cc # 1/e width of focal distribution
    temporal_sigma = 0#focal_spread/np.sqrt(2) # std of focal distribution (normal distribution is proportional to e**( 1/2*(x/std)**2) )
    #B_r=1e8

    semiangle_cutoff = 9#20.52
    lambd=Probe(energy=energy).wavelength
    alpha=semiangle_cutoff*1e-3
    defocus=300*13/3
    #aberrations = {"C12":15*1/8*lambd/(semiangle_cutoff*1e-3)**2}
    aberrations = {}

    if bulls_flag:
        aperture = BullseyeAperture(outer_angle=semiangle_cutoff, energy=None, inner_angle=semiangle_cutoff/(np.sqrt(5)/2), num_radials=0, cross=0., rotation=0.)
    else:
        aperture = None
    probe_p = Probe(energy=energy,rolloff = 0, semiangle_cutoff=semiangle_cutoff, aperture=aperture, defocus=defocus,**aberrations, device='gpu',sampling=sampling,extent=extent)

    #potential_p.project().show()
    #probe_p.build().show()
    #plt.figure()
    #plt.plot(np.abs(probe_p.build().array[probe_p.gpts[0]//2,:]).get()**2)
    #plt.show()
    #|
    # # Scanning parameters

    detector_p = PixelatedDetector(None)

    spatial_sigma = 0#get_gaussian_spread(probe_p.ctf.semiangle_cutoff,probe_p.energy,B_r,I)

    incoh_probe=incoherent_probe(probe_p,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma)
    if bulls_flag==True:
        current_ratio=0.7
    else:
        current_ratio=0.9
    r, _, _, _= get_probe_radius(probe_measurement=incoh_probe,current_ratio=current_ratio)
    d=2*r

    scan_gpts = 10
    scan_sampling = 30*d_ratio#d*d_ratio#2.7
    scan_start = np.array(probe_p.extent)/2-scan_sampling*(scan_gpts-1)/2
    scan_end = scan_start+scan_sampling*scan_gpts
    scan_grid = GridScan(scan_start, scan_end, sampling = scan_sampling)# sampling should be lower then 0.7 of probe width
    positions_grid = scan_grid.get_positions()
    positions = positions_grid# + np.random.uniform(-scan_sampling/10,scan_sampling/10,size=positions_grid.shape)
    scan_p=PositionScan(positions=positions)

    ax=axes_l[idx];idx+=1
    ax.imshow(incoh_probe.array,cmap="gray")
    center = (np.abs(incoh_probe.shape)-1)/2
    radius = r/incoh_probe.calibrations[0].sampling
    circle = plt.Circle(center, radius, color='b', fill=False)
    ax.add_patch(circle)
    #|
    #plt.figure()
    #plt.plot(positions_grid[:,0],positions_grid[:,1],"x")
    #plt.plot(positions[:,0],positions[:,1],".")
    #plt.show()
    #| Measurement

    #measurement=incoherent_scan_mc(probe_p,positions,detector_p,potential_p,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma,max_batch_scan=1000)
    measurement=probe_p.scan(scan_p,detectors=detector_p,potential=potential_p,max_batch=100,pbar=False)

    #measurement[0].show(power=0.2)
    #plt.show()
    #| Crop
    measurement_crop=crop(measurement,extent=(angular_extent*2,angular_extent*2))

    #measurement_crop[0].show(power=0.2)
    #plt.show()
    #| Estimating noise


    max_dose_1,dose_map_1=get_radiation_dose(incoh_probe,cp.array(positions),1,fft=False)

    #el=I*100e-6/c.e# number of electrons in diffraction pattern
    targeted_dose = p_dose # el/angstrom
    el = targeted_dose/max_dose_1

    I=50e-12
    t=el*c.e/I*1e3
    #print(t," ms")
    #print(1/(t*1e-3)*1e-3," kHz")

    ax=axes_l[idx];idx+=1
    max_dose,dose_map=get_radiation_dose(incoh_probe,cp.array(positions),el,fft=False)
    dose_map.show(ax=ax)
    ax.set_title("max_dose: "+str(np.round(max_dose))+" el: "+str(np.round(el)))
    #plt.show()


    #| Downsampling diffraction pattern

    measurement_resampled = decimate_measurement(measurement_crop,reciprocal_oversampling)

    #print(measurement_resampled.array.shape)
    #print(measurement_crop.array.shape)

    #| Applying noise

    if 1:
        norm=np.mean(np.sum(measurement.array,(-2,-1))) # average of fraction !uncropped! measurements(should be close to one, but it is a bit less then one cuz some electrons go past antialiasing aperture)
        measurement_noisy = poisson_noise(measurement_crop/norm, dose=el,pixel_area=1)/el # el can be set based on current and dwell time or better it can be parametrized with targeted dose (el/angstron^2), then el=targeted_dose/max_dose_1, max_dose_1 depends on shape of a probe and step size.
    else:
        measurement_noisy = measurement_crop


    ax=axes_l[idx];idx+=1
    measurement_noisy[0].show(ax=ax)

    #| # Reconstruction
    measurement_p=measurement_noisy

    positions_min=np.min(positions,0)
    positions_max=np.max(positions,0)
    position_center=(positions_max+positions_min)/2
    positions_centered = positions - position_center + np.array(probe_p.extent)/reciprocal_oversampling/2 


    probe_guess = probe_p.copy()# Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=defocus, **aberrations)
    modes = 1
    slices = 1
    thickness = 150
    reconstructions=invms(bandlimit(measurement_p,500),probe_guess,max_angle=None,positions=positions_centered,alpha=0.2,fac=1,beta=0.2,maxiter=32,k_modes=1,slices=slices,slice_thickness=thickness/slices,modes=modes,device="gpu",return_iterations=True)

    #probe_guess.show()
    #plt.show()

    #|

    #plot_every =8
    #
    #fig, axes = plt.subplots(1+modes, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))
    #
    #for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    #    axes[0,i].imshow(np.angle(reconstructions[0][j][-1].array).T, origin='lower', cmap='gray')
    #    for mode in range(modes): 
    #        axes[1+mode,i].imshow(np.abs(reconstructions[1][j][mode].array).T ** 2, origin='lower', cmap='gray')
    #    for ax in (axes[0,i],)+tuple([axes[1+mode,i] for mode in range(modes)]):
    #        ax.axis('off')        
    #    
    #plt.tight_layout()
    #
    #plt.show()
    #|

    ax=axes_l[idx];idx+=1
    ax.imshow(np.angle(reconstructions[0][-1][-1].array).T, origin='lower', cmap='gray')
    ax=axes_l[idx];idx+=1
    ax.imshow(np.abs(reconstructions[1][-1][-1].array).T ** 2, origin='lower', cmap='gray')

    directory=pathlib.Path(directory); directory.mkdir(parents=True, exist_ok=True)
    fig_all.savefig(directory/"dose{}_dratio{:.3f}.pdf".format(targeted_dose,d_ratio))
    #|

    #pot=potential_p.project().array
    #diffractogram=np.fft.fftshift(np.abs(np.fft.fft2(pot))**2)
    #plt.figure()
    #plt.imshow(diffractogram**(0.1))
    #probe_p.build().diffraction_pattern().show()
    #plt.show()
    ##|
#run(directory="export/bull/", d_ratio=0.1, p_dose = 500, bulls_flag=True)
#run(directory="export/normal/",)
import itertools
from functools import partial
#adf=map(partial(run,directory="export/bull/",bulls_flag=True), (0.1,),(500,))
#print(adf)
#list(adf)
print("ho")
import time 
from multiprocessing.pool import Pool
MAX_THREADS = 8

d_ratio=np.array([0.05,0.1,0.15,0.20,.25,.30,.35,.40,.45])
p_dose=np.array([500,250,1000,2000])
#d_ratio,p_dose=np.meshgrid(d_ratio,p_dose)
#d_ratio=d_ratio.reshape(-1)
#p_dose=p_dose.reshape(-1)
if __name__ == '__main__':


    s=time.time()
    out_dir="data/export_9mrad_tiny/"
    args=itertools.product(d_ratio,p_dose)
    with Pool(MAX_THREADS) as p:
        p.starmap(partial(run,directory=out_dir+"bull/",bulls_flag=True), args)
        #p.map(partial(run,directory="export/bull/",bulls_flag=True,p_dose=500), (0.2,))
        
    args=itertools.product(d_ratio,p_dose)
    with Pool(MAX_THREADS) as p:
        p.starmap(partial(run,directory=out_dir+"full/",bulls_flag=False), args)
    e=time.time()
    print((e-s),"s")
