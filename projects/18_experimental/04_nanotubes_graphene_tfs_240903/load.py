params={}
params[1]={
    'defocus':0,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':2
}
params[2]={ # toto je velmi jemne navzorkovane.. vyseky jednotlive trubky delaji moire obrazce? třeba N=100 xoff=32 yoff=64
    'defocus':0,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':2
}
params[3]={
    'defocus':1000,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':2
}
params[4]={
    'defocus':0,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':2 #ano i s 1 to jede.. N=48 xoff=0 yoff=64 single slice nebo xoff=16 yoff=16 (invms() funguje nejlepe ale reconstruction_g taky jede)
}
params[5]={
    'defocus':0,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':2 #! 1 uz zacne artefaktovat # multislice shows two tubes
}
params[6]={
    'defocus':0,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':2 #! 1 uz zacne artefaktovat # multislice shows two tubes
}
params[7]={
    'defocus':1000,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':4
}
params[8]={
    'defocus':8000,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':8
}
params[9]={
    'defocus':1000,
    'semiangle_cutoff':9,
    'crop':(14,None,7,-7),
    'thr':2
}
params[10]={ #n
    'defocus':0,
    'semiangle_cutoff':6,
    'crop':(15,-1,11,-5),
    'thr':2
}
params[11]={ # pro pripad orezu dat na 64 a offsetu o 63 v x a y se objevuji nejake divne detaily
    'defocus':0,
    'semiangle_cutoff':6,
    'crop':(11,-1,10,-2),
    'thr':2
}
params[12]={ #n
    'defocus':0,
    'semiangle_cutoff':6,
    'crop':(11,-1,10,-2),
    'thr':2
}
params[13]={ #n
    'defocus':0,
    'semiangle_cutoff':6,
    'crop':(11,-1,10,-2),
    'thr':2
}
params[14]={ 
    'defocus':16000,
    'semiangle_cutoff':6,
    'crop':(11,-1,10,-2),
    'thr':2
}
params[15]={ #n
    'defocus':0,
    'semiangle_cutoff':6,
    'crop':(11,-1,10,-2),
    'thr':2
}
params[16]={ #n
    'defocus':0,
    'semiangle_cutoff':12,
    'crop':(11,-1,10,-2),
    'thr':2
}
params[17]={ #n
    'defocus':0,
    'semiangle_cutoff':12,
    'crop':(11,-1,10,-2),
    'thr':2
}
params[18]={ 
    'defocus':4000,
    'semiangle_cutoff':12,
    'crop':(11,-1,4,-8),
    'thr':8
}
params[19]={ # pro pripad orezu dat na 64 a offsetu o 63 v x a y se objevuji nejake divne detaily
    'defocus':5000,
    'semiangle_cutoff':12,
    'crop':(11,-1,4,-8),
    'thr':8
}

import pathlib
import datetime
from tfs_utils.pak_to_numpy_load_and_save import load_metadata_from_xml, rapid_pak_data_load
def load_data(data_idx,save_dir):
    path_list = list( pathlib.Path(r"/home/jilek/tfs_data/data/240903_nanotubes/").glob("240903_nanotubes_{:02}*".format(data_idx)) )
    if len(path_list) !=1:
        raise ValueError()
    else:
        path=path_list[0]
    path_tif = r"/home/jilek/tfs_data/data/240903_nanotubes/jilek/240903_nanotubes/240903_0{:02}.tif".format(data_idx)
    
    fig_folder = pathlib.Path(save_dir)/datetime.date.today().strftime("%y%m%d")/"{:02}".format(data_idx)
    fig_folder.mkdir(exist_ok=True,parents=True)
    
    
    #Load metadata from xml file
    metadata = load_metadata_from_xml(path)
    
    # Define pattern_size from metadata
    pattern_size = (metadata.PatternHeight,metadata.PatternWidth)
    # Define size from metadata
    size = (metadata.Height,metadata.Width)
    # Load data from .pak files
    data = rapid_pak_data_load(path, pattern_size, size)
    
    data = data.transpose(0,1,3,2) #!!
    
    return(data,metadata,path_tif,fig_folder)