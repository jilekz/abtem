#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#conda environment: pokus_py4DSTEM 
#abtem version: 4d630003af7a2d33ee63e9a02f1846549d04be00

import os
os.environ["MKL_NUM_THREADS"] = "16"
os.environ["NUMEXPR_NUM_THREADS"] = "16"
os.environ["OMP_NUM_THREADS"] = "16"

import numpy as np
import matplotlib.pyplot as plt
from abtem import *

from abtem.noise import poisson_noise
import scipy.constants as c
from abtem.scan import PositionScan
from abtem.reconstruct import invms
from abtem.measure import bandlimit

from abtem.custom import get_gaussian_spread
from abtem.custom import get_radiation_dose
from abtem.custom import incoherent_scan
from abtem.custom import incoherent_scan_mc
from abtem.custom import sample_probe
from abtem.custom import incoherent_scan_mc_fp
from abtem.custom import incoherent_probe, get_probe_radius
from abtem.custom import crop
from abtem.custom import semiangle_2_sampling_extent,semiangle_gpts_2_extent
from abtem.custom import decimate_measurement
from abtem.utils import fft_crop,fft_shift
import cupy as cp
import h5py
import pathlib

import matplotlib

import sys 
sys.path.append("../16_registration")
from sample import gen_rnd_tubes

sys.path.append("../07_deep_stem/02_simulated_testing_data_LINK") #07_deep_stem: d27c25c5c9ef530e93488680ca3c1a0b797a2e80
from utils import get_positions, get_downsampled_phase, get_downsampled_mask
from materials.utils import get_potential_from_atoms

from py4DSTEM.process.utils.cross_correlate import align_images_fourier
from skimage.registration import phase_cross_correlation

import py4DSTEM


# In[ ]:


from abtem.custom import pad
from abtem.custom import get_probe_radius_,get_probe_radius, radial_histogram
from abtem.measure import Measurement,calibrations_from_grid
from ase.build import graphene
from abtem.structures import orthogonalize_cell
from natsort import natsorted
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
matplotlib.rcParams["image.interpolation"] = 'none'

matplotlib.rcParams.update({'figure.autolayout': True}),
matplotlib.pyplot.rcParams['figure.autolayout'] = True   #Automaticky nastaví velikost grafu, aby se vešel do obrazu,
matplotlib.pyplot.rcParams['figure.figsize'] = 12, 7     #Velikost obrázku - šířka, výška (v palcích),
matplotlib.pyplot.rcParams['axes.labelsize'] = 25        #Velikost názvů os,
matplotlib.pyplot.rcParams['axes.titlesize'] = 25        #Velikost nadpisu,
matplotlib.pyplot.rcParams['font.size'] = 25             #Velikost hodnot na osách,
matplotlib.pyplot.rcParams['lines.linewidth'] = 1      #Tloušťka čar,
matplotlib.pyplot.rcParams['lines.markersize'] = 12      #Velikost bodů,
matplotlib.pyplot.rcParams['legend.fontsize'] = 25       #Velikost textu v legendě,
#matplotlib.pyplot.rcParams['text.usetex'] = True         #LaTeX bude použit pro psaní všech textů,
#matplotlib.pyplot.rcParams['text.latex.unicode'] = True  #Použije latexové balíky pro unicode řetězce,
matplotlib.pyplot.rcParams['font.family'] = "serif"      #Nastaví rodinu fontů na 'serif',
#matplotlib.pyplot.rcParams['font.family'] = 'sans-serif'
#matplotlib.pyplot.rcParams['font.serif'] = "m"          #Do této rodiny patří i např. Times New Roman,
matplotlib.pyplot.rcParams['xtick.major.pad'] = 10.0     #Vzdálenost čísel na x-ové ose od osy,
matplotlib.pyplot.rcParams['ytick.major.pad'] = 10.0     #Vzdálenost čísel na y-ové ose od osy


# In[ ]:


import datetime
fig_folder = pathlib.Path("figs")/(datetime.date.today().strftime("%y%m%d")+"iCOM_nanotubes")
fig_folder.mkdir(exist_ok=True,parents=True)


# In[ ]:


from abtem.transfer import scherzer_defocus


# In[ ]:


scherzer_defocus(energy=30e3,Cs=0.88e-3/1e-10)


# In[ ]:


get_ipython().run_line_magic('matplotlib', 'inline')


# In[ ]:


energy = 30e3 #8x: 800e3 #4x: 281e3 #1x: 22e3


# In[ ]:


from abtem.utils import energy2wavelength


# In[ ]:


aberrations


# In[ ]:


temporal_sigma


# In[ ]:


#bez chroma: 
10 3.92
11 3.64
12 3.358
13 3.217
14 3.217
15 3.500
#s chroma:
11 3.78
12 3.570
13 3.57
14 3.641
#######x


# In[ ]:


energy_spread = 0.6
Cc = 1.34e-3/1e-10
focal_spread = energy_spread/energy * Cc
temporal_sigma = focal_spread/np.sqrt(2)
defocuses,diameters = plot_radius_on_defocus(extent=40,energy=energy,sampling=0.1,semiangle_cutoff=11,aberrations={'C30':0.88e-3/1e-10},temporal_sigma=temporal_sigma,current_ratio=0.5)
idx_min = np.argmin(diameters)
idx_chosen = np.argmin(np.abs(diameters[idx_min:]-extent/2*0.7))
defocus_chosen = defocuses[idx_min:][idx_chosen]
print(diameters[idx_min])


# In[ ]:


Probe(energy=energy,semiangle_cutoff=16,defocus=scherzer_defocus(energy=energy,Cs=0.88e-3/1e-10),Cs=0.88e-3/1e-10,focal_spread=focal_spread).ctf.show()


# In[ ]:


def plot_phase_on_defocus(energy,semiangle_cutoff,aberrations,gpts=512):
    aberrations_=aberrations.copy()
    extent=1/(semiangle_cutoff*2*1e-3*1/energy2wavelength(energy)/gpts)
    defocus_diff=extent/50/(semiangle_cutoff*1e-3)
    defocuses = np.linspace(-defocus_diff,defocus_diff,200)
    phases = np.zeros_like(defocuses)
    for i,defocus in enumerate(defocuses):
        aberrations_['defocus']=defocus
        probe=Probe(semiangle_cutoff=semiangle_cutoff,energy=energy,gpts=gpts,extent=extent,rolloff=0.1,**aberrations_)
        alpha,phi=probe.get_scattering_angles()
        chi=probe.ctf.evaluate_chi(alpha=alpha,phi=phi)
        aperture=probe.ctf.evaluate_aperture(alpha=alpha,phi=phi)
        phases[i]=np.ptp(aperture*chi)

    return(defocuses,phases)


# In[ ]:


def plot_radius_on_defocus(extent,energy,sampling,semiangle_cutoff,aberrations,current_ratio=0.95,temporal_sigma=0):
    aberrations_=aberrations.copy()
    defocus_diff=extent/2/(semiangle_cutoff*1e-3)
    defocuses = np.linspace(-defocus_diff,defocus_diff,100)
    diameters = np.zeros_like(defocuses)
    for i,defocus in enumerate(defocuses):
        aberrations_['defocus']=defocus
        probe = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff,rolloff=2, **aberrations_, device='gpu',sampling=sampling,extent=extent)

        incoh_probe = incoherent_probe(probe,temporal_sigma=temporal_sigma,spatial_sigma=0)
        r, _, _, _= get_probe_radius(probe_measurement=incoh_probe,current_ratio=current_ratio)
        diameters[i]=2*r

    return(defocuses,diameters)
#defocuses,diameters = plot_radius_on_defocus(extent=350,energy=energy,sampling=0.3,semiangle_cutoff=16,aberrations={'C30':0.88e-3/1e-10})
#plt.figure()
#plt.plot(defocuses,diameters)
#plt.show()
#defocuses,diameters = plot_radius_on_defocus(extent=350,energy=energy,sampling=0.3,semiangle_cutoff=16,aberrations={'C30':0.88e-3/1e-10})
#idx_min = np.argmin(diameters)
#idx_chosen = np.argmin(np.abs(diameters[idx_min:]-extent/2*0.7))
#defocus_chosen = defocuses[idx_min:][idx_chosen]


# In[ ]:


wavelength=energy2wavelength(energy)
def a2s(a): #angle to spatial frequency
    s=a*1e-3*1/wavelength
    return(s)

def s2a(s):
    a=s/(1/wavelength)*1e3
    return(a)


# In[ ]:


#%matplotlib inline


# In[ ]:


#matplotlib.interactive(True)

#mempool = cp.get_default_memory_pool()


#def run(input_i, output_path,single_patch=False, input_path="data/orig/", input_suffix=".jpg" ,device="gpu"):
if True:
    device = "gpu"
    # Domain
    #energy = 30e3
    px_size = 56e-6
    px_num = 256
    camera_length = 240e-3
    angular_sampling = px_size/camera_length*1e3 # in mrad
    angular_extent = angular_sampling * px_num/2  # only semi angle

    binning = 1
    reciprocal_oversampling = 1
    direct_oversampling = 4
    sampling, extent = semiangle_2_sampling_extent(angular_extent,px_num/binning,energy=energy,direct_oversampling=direct_oversampling,reciprocal_oversampling=reciprocal_oversampling)

    reconstruction_direct_oversampling = 1
    comparison_direct_oversampling = int(direct_oversampling/reconstruction_direct_oversampling)
    # Potential, aberrations, probe

    atoms_ = graphene()
    atoms_ = orthogonalize_cell(atoms_)
    atoms_ *= (24,16*2,1)
    
    atoms_.center(vacuum=2, axis=2)
    atoms_.center()
    
    atoms_rotated=atoms_.copy()
    atoms_rotated.rotate(10,"z")
    atoms_rotated.center()
    atoms_rotated.positions+=np.array([0,0,2])
    
    atoms_moire=atoms_+atoms_rotated
    atoms_moire.center(vacuum=2)

    atoms_tubes = gen_rnd_tubes(1,N_=40,seed=38)#reciprocal_oversampling)

    atoms_moire_extent = np.array(np.diagonal(atoms_moire.cell)); atoms_moire_extent[2]=0; atoms_moire_extent[1]=0
    atoms_moire.translate(-atoms_moire.get_center_of_mass()+atoms_tubes.get_center_of_mass()+atoms_moire_extent/2)
    atoms=atoms_moire+atoms_tubes
    #atoms.set_chemical_symbols(["Ti" for i in range(len(atoms))])
    _,potential=get_potential_from_atoms(atoms,extent=extent,sampling=sampling)
    potential_p=potential.build()

    random_shape=potential_p.project().array.shape
    random_modulus = np.max(potential_p.project().array)
    random_phase=np.random.uniform(0,2*np.pi,random_shape)
    random_ft=random_modulus*np.exp(1j*random_phase)
    random_ft[:random_shape[0]//2,:]=0
    random_ft[random_shape[0]//2,random_shape[1]//2:]=0
    random_ft[:,0]=0
    random=np.real(np.fft.ifft2(np.fft.fftshift(random_ft)))*0.25e3

    if 1:
        potential_p=potential.build()
    else:
        potential_p=PotentialArray(random[None],slice_thicknesses=4,extent=extent,sampling=sampling)
    
    if potential_p is None:
        assert(True)
        #return(False)    

    energy_spread = 0.6
    Cc = 1.34e-3/1e-10
    focal_spread = energy_spread/energy * Cc # 1/e width of focal distribution
    temporal_sigma = focal_spread/np.sqrt(2) # std of focal distribution (normal distribution is proportional to e**( 1/2*(x/std)**2) )
    B_r=1e8

    #fac=# 0.5,1,2,2.5 nebo fixne 2.5 a menit semiangle_cutoff...
    semiangle_cutoff = 12#*fac#nebo fixne 6*fac,
    alpha=semiangle_cutoff*1e-3

    aberrations = {'C30':0.88e-3/1e-10,'C12':0*2000,'C23':0*100000}
    defocuses,diameters = plot_radius_on_defocus(extent=extent,energy=energy,sampling=sampling,semiangle_cutoff=semiangle_cutoff,aberrations=aberrations)
    idx_min = np.argmin(diameters)
    idx_chosen = np.argmin(np.abs(diameters[idx_min:]-extent/2*0.7))
    defocus_chosen = defocuses[idx_min:][idx_chosen]
    
    #aberrations = get_aberrations(scale_all=1,aberrations_set={'C10':350})
    aberrations['C10']=-defocus_chosen
    probe = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff,rolloff=2, **aberrations, device=device,sampling=sampling,extent=extent)

    ##potential_p.project().show()
    ##probe.build().show()
    ##plt.plot(np.abs(probe.build().array[probe.gpts[0]//2,:]).get()**2)

    # Detector, scanning parameters

    detector_p = PixelatedDetector(None)

    I=50e-12
    spatial_sigma = get_gaussian_spread(probe.ctf.semiangle_cutoff,probe.energy,B_r,I)

    incoh_probe=incoherent_probe(probe,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma)
    r, _, _, _= get_probe_radius(probe_measurement=incoh_probe,current_ratio=0.95)
    d=2*r
    #scan_gpts = 16
    scan_sampling = d*0.1#100/16#d*0.15#100/16#d*0.15/8#*np.random.uniform(0.15,0.5)
    print('scan_sampling: ',scan_sampling,'defocus: ', defocus_chosen)
    #scan_sampling = 1/(2*semiangle_cutoff*1e-3*1/probe.wavelength)
    scan_gpts = int(extent/2*1.5 /scan_sampling)
    positions=get_positions(scan_gpts,np.array(probe.extent),scan_sampling)
    scan_p=PositionScan(positions=positions)

    #|######### Estimating noise

    probe_dummy=probe.copy()
    probe_dummy.sampling=np.array(probe.sampling)*direct_oversampling/2 # 
    incoh_probe_dummy=incoherent_probe(probe_dummy,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma)
    if device=="gpu":
        xp=cp
    else:
        xp=np
    max_dose_1, radiation_map=get_radiation_dose(incoh_probe_dummy,xp.array(positions),1,fft=False)

    plt.figure()
    radiation_map.show()
    plt.show()


    #el=I*100e-6/c.e# number of electrons in diffraction pattern
    targeted_dose = 20#5000/500*20*fac**2#/500 # el/angstrom
    el = targeted_dose/max_dose_1
    print("d: ",d);print("el: ",el);print("t: ", el*c.e/I)

    phase_downsampled = get_downsampled_phase(potential_p,comparison_direct_oversampling,energy=probe.energy)
    if phase_downsampled is None:
        print("No downsampled phase")
        assert(True)
        #return(None)

    probe_dummy_mask=Probe(energy=energy, semiangle_cutoff=probe.ctf.semiangle_cutoff, **aberrations,extent=extent,sampling=sampling)
    mask = get_downsampled_mask(probe_dummy_mask, comparison_direct_oversampling, thr_fac = 0.1, positions = positions, temporal_sigma = 0, spatial_sigma = 0)
    #mask_finer = get_downsampled_mask(probe, direct_oversampling//2, thr_fac = 0.1, positions = positions, temporal_sigma = 0, spatial_sigma = 0)
    
    #|######### Measurement

    measurement=incoherent_scan_mc(probe,positions,detector_p,potential_p,temporal_sigma=0*temporal_sigma,spatial_sigma=0*spatial_sigma,max_batch_scan=256)
    #measurement=probe.scan(scan_p,detectors=detector_p,potential=potential_p,max_batch=50,pbar=False)

    measurement_crop=crop(measurement,extent=(angular_extent*2+1e-6,angular_extent*2+1e-6))

    # Downsampling diffraction pattern

    measurement_resampled = decimate_measurement(measurement_crop,reciprocal_oversampling)

    #| Apply noise or dont?

    norm=np.mean(np.sum(measurement.array,(-2,-1))) # average of fraction !uncropped! measurements(should be close to one, but it is a bit less then one cuz some electrons go past antialiasing aperture)
    if 1:
        measurement_noisy = poisson_noise(measurement_resampled/norm, dose=el,pixel_area=1)/el # el can be set based on current and dwell time or better it can be parametrized with targeted dose (el/angstron^2), then el=targeted_dose/max_dose_1, max_dose_1 depends on shape of a probe and step size.
    else:
        measurement_noisy = measurement_resampled

    measurement_noisy.show()
    plt.show()


# In[ ]:


from ase.visualize.plot import plot_atoms


# In[ ]:


fig,ax=plt.subplots(1,1)
atoms_asdf=atoms.copy()
atoms_asdf.cell=np.eye(3)*1000
atoms_asdf.center()
show_atoms(atoms_asdf,ax=ax,scale_atoms=0.15)
p=atoms_asdf.get_center_of_mass()[:2]
o=90
bar_len=40
scalebar = AnchoredSizeBar(plt.gca().transData,
                           bar_len, '{} nm'.format(bar_len//10), 'lower right', 
                           pad=0.1,
                           color='black',
                           frameon=False,
                           size_vertical=4)
                           #fontproperties=fontprops)

ax.add_artist(scalebar)
plt.xlim(p[0]-1,p[0]+1)
plt.ylim(p[1]-o,p[1]+o)
ax.axis('off')
#fig.savefig("figs/atomic_model.pdf")


# In[ ]:


from abtem.utils import energy2sigma


# In[ ]:


ex=potential.sampling[0]*potential.gpts[0]
ext=(0,ex,0,ex)
plt.imshow(potential.project().array.T*energy2sigma(energy=energy),extent=ext,cmap="gray",origin="lower")
scalebar = AnchoredSizeBar(plt.gca().transData,
                           bar_len, '{} nm'.format(bar_len//10), 'lower right', 
                           pad=0.1,
                           color='white',
                           frameon=False,
                           size_vertical=4)
                           #fontproperties=fontprops)

plt.gca().add_artist(scalebar)
plt.colorbar()
plt.xlim(60,250)
plt.ylim(50,240)
plt.axis('off')
#plt.savefig('figs/projected_potential.pdf')


# In[ ]:


plt.imshow(np.fft.fftshift(np.abs(np.fft.fft2(potential.project().array)))**0.5)


# In[ ]:


1/(1/probe.wavelength*8e-3)


# In[ ]:


#ADF kirkland


# In[ ]:


0.87*(aberrations['C30']*probe.wavelength)**(0.5) #kirkland defocus


# In[ ]:


1.37*(probe.wavelength/aberrations['C30'])**(1/4) #kirkland angle


# In[ ]:


0.43*(C30*probe.wavelength**3)**(1/4) #d_min


# In[ ]:


#BF kirkland (scherzer)


# In[ ]:


(6*probe.wavelength/aberrations['C30'])**(1/4) #kirkland angle


# In[ ]:


0.67*(C30*probe.wavelength**3)**(1/4) #d_min


# In[ ]:


Probe(energy=energy,semiangle_cutoff=16,defocus=scherzer_defocus(energy=energy,Cs=C30),C30=C30).ctf.show()


# In[ ]:


aberrations


# In[ ]:


defocuses,phases=plot_phase_on_defocus(energy=energy,semiangle_cutoff=11.2,aberrations={'C30':8800000},gpts=512)
idx_min=np.argmin(phases)
defocus_chosen = defocuses[idx_min]
print(defocus_chosen)
print(phases[idx_min])


# In[ ]:


np.pi/4


# In[ ]:


#%matplotlib inline


# In[ ]:


random_shape=potential_p.project().array.shape
random_modulus = np.max(potential_p.project().array)
random_phase=np.random.uniform(0,2*np.pi,random_shape)
random_ft=random_modulus*np.exp(1j*random_phase)
random_ft[:random_shape[0]//2,:]=0
random_ft[random_shape[0]//2,random_shape[1]//2:]=0
random_ft[:,0]=0
plt.figure()
plt.imshow(np.abs(random_ft))
random=np.real(np.fft.ifft2(np.fft.fftshift(random_ft)))#*1e3


# In[ ]:


#### plt.figure()
plt.figure()
plt.imshow(np.fft.fftshift(np.abs(np.fft.fft2(random))))
plt.colorbar()


# In[ ]:


random_shape=(1024,1024)


# In[ ]:


#%matplotlib widget


# In[ ]:


np.array(atoms_moire_extent)[2]=0


# In[ ]:


probe.show(power=0.4)


# In[ ]:


plt.figure()
probe.ctf.show()


# In[ ]:


scan_sampling


# In[ ]:


1/(1e-3*(1/probe.wavelength))


# In[ ]:


measurement.show(power=0.5)


# In[ ]:


plt.close()


# In[ ]:


fig,ax=plt.subplots()
potential.build().project().show(ax=ax)
ax.plot(positions[:,0],positions[:,1],"x",markersize=1,alpha=0.5)


# In[ ]:


measurement_decimated = decimate_measurement(measurement_noisy,1)
measurement_decimated.show()
measurement_decimated = pad(measurement_decimated,extent=measurement_decimated.shape[-2]*measurement_decimated.calibrations[-2].sampling*reconstruction_direct_oversampling)
measurement_decimated.show()


# In[ ]:


measurement_decimated.shape


# In[ ]:


#%matplotlib widget


# In[ ]:


measurement_decimated.show(power=0.1)


# In[ ]:


xxx=np.linspace(-1,1,256)
yyy,xxx=np.meshgrid(xxx,xxx)
massk=np.sqrt(xxx**2+yyy**2)<4/29.6
plt.figure()
plt.imshow(massk)
plt.figure()
plt.imshow(np.sum(measurement_decimated.array,axis=0)**0.2)

plt.figure()
plt.imshow(np.real(np.fft.ifft2(np.fft.fft2(result)*np.fft.fftshift(massk))).T,origin='lower')


# In[ ]:


xxx=np.linspace(-1,1,256)
yyy,xxx=np.meshgrid(xxx,xxx)
massk=(5/29.6<np.sqrt(xxx**2+yyy**2))*(np.sqrt(xxx**2+yyy**2)<10/29.6)
plt.figure()
plt.imshow(massk)
plt.figure()
plt.imshow(np.mean(measurement_decimated.array,axis=0)**0.2)

plt.figure()
plt.imshow(np.real(np.fft.ifft2(np.fft.fft2(phase_downsampled)*np.fft.fftshift(massk))).T,origin='lower')


# In[ ]:


result


# In[ ]:


get_ipython().run_cell_magic('time', '', "probe_guess = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=-aberrations['C10'],C30=aberrations['C30'],extent=40,gpts=256)\nreconstructions = invms(measurement_decimated, probe_guess, positions = positions , modes=1,k_modes=1, alpha=0.004+0*0.0005, fac=1, beta=0*0.1, slices=1, slice_thickness=21,  maxiter=8*4, return_iterations=True, fix_com=True,device='gpu')\n")


# In[ ]:


#%matplotlib inline
plot_every = 8

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))
for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][0].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions[1][j][0].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()


# In[ ]:


result=np.angle(reconstructions[0][-1][0].array)


# In[ ]:


result.shape

measurement_decimated.calibrations[-1].sampling
# In[ ]:


probe.sampling


# In[ ]:


measurement_decimated.calibrations[1].offset


# In[ ]:


result_sampling = 1/(measurement_decimated.calibrations[-1].sampling*measurement_decimated.shape[-1]*1e-3*1/probe.wavelength)
result_extent = (0,result_sampling*measurement_decimated.shape[-2],0,result_sampling*measurement_decimated.shape[-1])


# In[ ]:


#plt.figure(figsize=(,20))
plt.imshow(result.T,cmap="gray",origin="lower",extent=result_extent)
plt.axis("off")
_,x_right = plt.xlim()
_,y_right = plt.ylim()
offset=60
plt.xlim(offset,x_right-offset)
plt.ylim(offset,y_right-offset)
bar_len=40 # in angstrom
scalebar = AnchoredSizeBar(plt.gca().transData,
                           bar_len, '{} nm'.format(bar_len//10), 'lower right', 
                           pad=0.1,
                           color='white',
                           frameon=False,
                           size_vertical=4)
                           #fontproperties=fontprops)

plt.gca().add_artist(scalebar)
plt.colorbar()
plt.savefig(fig_folder/"semiangle_{}_dose_{}.pdf".format(semiangle_cutoff,targeted_dose))


# In[ ]:


np.mean(positions,axis=0)


# In[ ]:


if 0:
    from abtem.reconstruct_g import MixedStatePtychographicOperator
    
    mixed_states_ptycho_operator = MixedStatePtychographicOperator(
        measurement_decimated,
        energy=energy,
        semiangle_cutoff=semiangle_cutoff,
        defocus=-aberrations['C10'],
        C30=aberrations['C30'],
        positions=positions,
        num_probes=1,
        device="gpu",
        parameters={"object_px_padding": (100, 100)},
    ).preprocess()
    
    (
        mxrpie_objects,
        mxrpie_probes,
        mxrpie_positions,
        mxrpie_sse,
    ) = mixed_states_ptycho_operator.reconstruct(
        max_iterations=8,
        random_seed=1,
        return_iterations=True,
        verbose=True,
        parameters={"alpha":1, "beta":1, "object_step_size":0.1,"probe_step_size":0*0.1},
        warmup_update_steps=mixed_states_ptycho_operator._num_diffraction_patterns * 100,
    )


# In[ ]:


#mxrpie_objects[t].angle().array[40:-41,40:-41].shape


# In[ ]:


337-256


# In[ ]:


measurement_decimated.shape


# In[ ]:


if 0:
    #fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
    t=-1
    ex=mxrpie_objects[0].calibrations[0].sampling*mxrpie_objects[0].array.shape[0]
    ext=(0,ex,0,ex)
    plt.figure()
    plt.imshow(mxrpie_objects[t].angle().array,cmap="inferno",extent=ext)#,origin="lower")
    plt.xlabel("x [Å]");plt.ylabel("y [Å]")
    #plt.clim(-0.5,0.9)
    #plt.clim(-0.3,0.2)
    #plt.axis("off")
    #plt.savefig(fig_folder/"10_reconstructied_phase.pdf")
    ex=mxrpie_probes[0][0].calibrations[0].sampling*mxrpie_probes[0][0].array.shape[0]
    ext=(0,ex,0,ex)
    plt.figure()
    plt.imshow(mxrpie_probes[t][0].intensity().array,cmap="gray",extent=ext)
    plt.xlabel("x [Å]");plt.ylabel("y [Å]")
    #plt.axis("off")
    #plt.savefig(fig_folder/"11_reconstructed_probe.pdf")
    #fig.tight_layout()


# In[ ]:


probe_dummy_comparison_mask = Probe(energy=energy, semiangle_cutoff=8, defocus=4000,extent=extent,sampling=sampling)
positions_comparison = get_positions(8,np.array(probe_dummy_comparison_mask.extent),6)
mask_comparison = get_downsampled_mask(probe_dummy_comparison_mask, comparison_direct_oversampling, thr_fac = 0.1, positions = positions_comparison , temporal_sigma = 0, spatial_sigma = 0)
plt.figure()
plt.imshow(mask_comparison)


# In[ ]:


mask = get_downsampled_mask(probe_dummy_mask, comparison_direct_oversampling, thr_fac = 0.3, positions = positions, temporal_sigma = 0, spatial_sigma = 0)
plt.figure()
plt.imshow(mask)


# In[ ]:


result_=result - np.mean(result*mask_comparison/mask_comparison.sum()) + 1e-6# aby frc nebylo pro nizke hodnoty random..
phase_downsampled_=phase_downsampled - np.mean(phase_downsampled*mask_comparison/mask_comparison.sum()) + 1e-6


# In[ ]:


def get_FRC_CTF(result,mask,phase_downsampled,h5file_path, registrate=False,thr=np.pi/20000,gaussian_sigma=10*comparison_direct_oversampling,real_space_sampling_=1/(measurement_decimated.calibrations[-1].sampling*measurement_decimated.shape[-1]*1e-3*(1/probe.wavelength))):
    #Given reconstruction result, mask and groundtruth 'phase_downsampled' give me FRC and CTF
    
    #Following crops ground truth to the reconstruction extent. e.g. If the result of the reconstruction is only an inset of the groundtrouth.. so I can change decimation of measurement as I wish.. but the maximal scattering angle should be the same as in the groundtruth..
    gpts = result.shape[0]
    center = phase_downsampled.shape[0]//2
    sel_slice = slice(center-gpts//2,center+gpts//2)
    phase_downsampled_crop = phase_downsampled[sel_slice,sel_slice]
    mask_crop = mask[sel_slice,sel_slice]
    
    from scipy.ndimage import gaussian_filter
    mask_taper = gaussian_filter(mask_crop*1.0, sigma=gaussian_sigma)
    
    #Register
    G_ref = np.fft.fft2(phase_downsampled_crop)
    G = np.fft.fft2(result)
    upsample_factor = 50
    shift_ = align_images_fourier(G_ref,G,upsample_factor=upsample_factor,device="cpu")
    shift, error, diffphase = phase_cross_correlation(G_ref, G, upsample_factor=upsample_factor,space='fourier')
    print("shift: ",shift)
    
    #Shift
    from abtem.utils import fft_shift
    if registrate:
        result_shifted=np.real(fft_shift(result,shift))
    else:
        result_shifted=np.real(fft_shift(result,np.array([0,0]))) # nebudeme registrovat 
    
    #hmm? Doing nothing?
    #center_patch = result_shifted.shape[0]//2
    #gpts_patch = mask_taper.shape[0]
    #sel_patch_slice = slice(center_patch-gpts_patch//2,center_patch+gpts_patch//2)
    #result_shifted_=result_shifted[sel_patch_slice,sel_patch_slice]*mask_taper#*mask_crop
    #phase_downsampled_crop_=phase_downsampled_crop[sel_patch_slice,sel_patch_slice]*mask_taper#*mask_crop
    result_shifted_=result_shifted*mask_taper#*mask_crop
    phase_downsampled_crop_=phase_downsampled_crop*mask_taper#*mask_crop
    
    plt.figure()
    plt.imshow((phase_downsampled_crop_).T,origin="lower",cmap="gray")
    
    plt.figure()
    plt.imshow((result_shifted_).T,origin="lower",cmap="gray")

    real_space_sampling=real_space_sampling_
    
    q,frc,half_bit=py4DSTEM.process.phase.utils.fourier_ring_correlation(phase_downsampled_crop_, result_shifted_, pixel_size=(real_space_sampling,)*2, bin_size=None, sigma=None, align_images=False, upsample_factor=16, device='cpu', plot_frc=True, frc_color='red', half_bit_color='blue')

    plt.figure()
    plt.plot(q/(1/probe.wavelength)*1e3,frc,"r",label="FRC")
    #plt.plot(q/(1/probe.wavelength)*1e3,half_bit,"b",label="half bit")
    plt.axhline(y=0.143,label='FRC = 0.143')
    plt.legend()
    
    ########CTF
    ctf=np.abs(np.fft.fft2(result_shifted_))/(np.abs(np.fft.fft2(phase_downsampled_crop_)))
    plt.figure()
    plt.title("CTF orig")
    q_,I_,_ = py4DSTEM.phase.utils.return_1D_profile(ctf,pixel_size=(real_space_sampling,)*2)
    plt.plot(q_/(1/probe.wavelength)*1e3,I_,".")
    plt.show()
    
    mask_normalisation = np.size(mask_taper)/np.sum(mask_taper>.1) # normalizace kvuli omezememu fov. # mask_normalisation -> za predpokladu, ze by maska byla uplne hranata.. (coz by uz kvuli frc nebylo dobre), tak by se jednalo o simulaci na mensim foV a mohl bych treba obrazek oriznout, dostal bych vlastne umerne zvetseny pixel v reciprokem prostoru a tim padem by se mi intenzita v tomto zvetsenem pixelu nascitala z okoli.. o kolik by se zvetsilo je dane velikosti orezu: np.size(mask_taper)/np.size(mask_taper>0) ...uplne nevim jak by se to melo udelat spravne pro pripad nehranate masky, ale je to v tomto pripade asi jedno...
    normalisation = np.prod(phase_downsampled_crop_.shape) 
    orig = np.fft.fftshift(np.abs( np.fft.fft2(phase_downsampled_crop_)))/normalisation*mask_normalisation #...normalizace
    print("mask_normalisation: ",mask_normalisation)
    print("normalisation: ",normalisation)
    plt.figure()
    plt.imshow(mask_taper)
    plt.show()
    mask_hist = orig>thr    #mask_hist = cor>0.5
    bins_hist = 128
    plt.figure()
    plt.imshow(mask_hist)
    ring_brightness, radius_edges = radial_histogram(np.fft.fftshift(ctf),mask_hist,bins=bins_hist)
    radius = (radius_edges[1:]+radius_edges[0:-1])/2
    plt.figure()
    plt.plot(radius,ring_brightness,'.')
    
    ring_brightness_, radius_edges_ = radial_histogram(np.ones_like(ctf),mask_hist,bins=bins_hist)
    
    sampling = 1/real_space_sampling/ctf.shape[0]/(1/probe.wavelength)*1e3
    
    delta_radius=radius[1]-radius[0]
    N_mezikruzi = ( np.pi*((radius+delta_radius)*sampling)**2 - np.pi*(radius*sampling)**2 )/sampling**2 # pocet pixelu, ktere jsou v mezikruzi radius az radius + delta_radius
    
    plt.figure()
    plt.plot(radius*sampling,N_mezikruzi)
    plt.plot(radius*sampling,ring_brightness_)
    plt.figure()
    plt.plot(radius*sampling,ring_brightness_/(N_mezikruzi))
    
    plt.figure()
    #mask_thr=ring_brightness_>6
    mask_thr=(ring_brightness_/N_mezikruzi)>0.05#5
    ctf_hist=ring_brightness/ring_brightness_
    plt.plot(radius[mask_thr]*sampling,ctf_hist[mask_thr],'.')
    plt.ylim(0,None)#1.5)

    frc_x = q/(1/probe.wavelength)*1e3,
    frc_y = frc
    ctf_x = radius[mask_thr]*sampling
    ctf_y = ctf_hist[mask_thr]

    with h5py.File(h5file_path, 'w') as f:
        f.create_dataset("semiangle_cutoff",data= semiangle_cutoff)
        f.create_dataset("C10",data=aberrations["C10"])
        f.create_dataset("C30",data=aberrations["C30"])
        f.create_dataset("targeted_dose", data=targeted_dose)
        f.create_dataset("frc_x",data=frc_x)
        f.create_dataset("frc_y",data=frc_y)
        f.create_dataset("ctf_x",data=ctf_x)
        f.create_dataset("ctf_y",data=ctf_y)

#get_FRC_CTF(result=result, mask=mask, phase_downsampled=phase_downsampled)
#get_FRC_CTF(result=mxrpie_objects[-1].angle().array, mask=mask_comparison, phase_downsampled=phase_downsampled)


# In[ ]:


h5file_path = fig_folder/"hdfs"/'semiangle_{}_dose_{}.h5'.format(semiangle_cutoff,targeted_dose)
h5file_path.parent.mkdir(exist_ok=True,parents=True)
get_FRC_CTF(result=result_, mask=mask_comparison, h5file_path=h5file_path, phase_downsampled=phase_downsampled_,gaussian_sigma=10,thr=np.pi/30000)


# In[ ]:


## get_FRC_CTF(result=mxrpie_objects[t].angle().array[40:-40,40:-40], mask=mask_comparison, phase_downsampled=phase_downsampled,fig_folder=fig_folder)


# In[ ]:


hdfs_path="figs/241111_tubes/hdfs/"
#hdfs_path=fig_folder/'hdfs'


hdfs=list(pathlib.Path(hdfs_path).glob('*.h5'))
hdfs=natsorted(hdfs)

fig_frc,ax_frc=plt.subplots(1,1)
fig_ctf,ax_ctf=plt.subplots(1,1)
plot_dose = 100.0
colors=["r","g","b","k"]
i_color=0
for file in hdfs:
    with h5py.File(file, 'r') as f:
        if f['targeted_dose'][()] == plot_dose:
            if f['semiangle_cutoff'][()]>13: # skip 15 mrad
                continue
            ax_frc.plot(f['frc_x'][()][0] ,f['frc_y'][()],label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ),color=colors[i_color])
            ax_ctf.plot(f['ctf_x'][()] ,f['ctf_y'][()],".",label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ),color=colors[i_color])
            i_color+=1
ax_frc.set_xlabel(r"$\alpha$ [mrad]")
ax_frc.set_ylabel(r"FRC")
ax_frc.axhline(y=0.143,linestyle="--",color="k",label='FRC = 0.143')


ax_ctf.legend()
ax_ctf.set_xlabel(r"$\alpha$ [mrad]")
ax_ctf.set_ylabel(r"CTF")
ax_frc.legend()
fig_frc.savefig("figs/241111_tubes/frc_dose_{}.pdf".format(plot_dose))
fig_ctf.savefig("figs/241111_tubes/ctf_dose_{}.pdf".format(plot_dose))


# In[ ]:


hdfs_path="figs/241104/hdfs/"
#hdfs_path=fig_folder/'hdfs'


hdfs=list(pathlib.Path(hdfs_path).glob('*.h5'))
hdfs=natsorted(hdfs)

fig_frc,ax_frc=plt.subplots(1,1)
fig_ctf,ax_ctf=plt.subplots(1,1)
plot_dose = 1000.0
colors=["r","g","b","k"]
i_color=0
for file in hdfs:
    with h5py.File(file, 'r') as f:
        if f['targeted_dose'][()] == plot_dose:
            ax_frc.plot(f['frc_x'][()][0] ,f['frc_y'][()],label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ),color=colors[i_color])
            ax_ctf.plot(f['ctf_x'][()] ,f['ctf_y'][()],".",label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ),color=colors[i_color])
            i_color+=1
ax_frc.set_xlabel(r"$\alpha$ [mrad]")
ax_frc.set_ylabel(r"FRC")
ax_frc.axhline(y=0.143,linestyle="--",color="k",label='FRC = 0.143')


ax_ctf.legend()
ax_ctf.set_xlabel(r"$\alpha$ [mrad]")
ax_ctf.set_ylabel(r"CTF")
ax_frc.legend()
#fig_frc.savefig("figs/241104/frc_dose_{}.pdf".format(plot_dose))
#fig_ctf.savefig("figs/241104/ctf_dose_{}.pdf".format(plot_dose))


# In[ ]:


hdfs_path="figs/241111_fix_dose/hdfs/"
#hdfs_path=fig_folder/'hdfs'


hdfs=list(pathlib.Path(hdfs_path).glob('*.h5'))
hdfs=natsorted(hdfs)

fig_frc,ax_frc=plt.subplots(1,1)
fig_ctf,ax_ctf=plt.subplots(1,1)
plot_dose = 1000.0
i_color=0
colors=['r','g','b','k']
for file in hdfs:
    with h5py.File(file, 'r') as f:
        #if f['targeted_dose'][()] == plot_dose:
            if f['semiangle_cutoff'][()]>13: # skip 15 mrad
                continue
            ax_frc.plot(f['frc_x'][()][0] ,f['frc_y'][()],label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ),color=colors[i_color])
            ax_ctf.plot(f['ctf_x'][()] ,f['ctf_y'][()],".-",label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ),color=colors[i_color])
            i_color+=1
ax_frc.set_xlabel(r"$\alpha$ [mrad]")
ax_frc.set_ylabel(r"FRC")
ax_frc.axhline(y=0.143,linestyle="--",color="k",label='FRC = 0.143')
ax_frc.legend()

secax_ctf = ax_ctf.secondary_xaxis('top', functions=(a2s, s2a))
secax_ctf.set_xlabel(r'$k$ [$\mathrm{\AA}^{-1}$]')

ax_ctf.legend()
ax_ctf.set_xlabel(r"$\alpha$ [mrad]")
ax_ctf.set_ylabel(r"|CTF|")
ax_ctf.set_xlim(None,30)
#fig_frc.savefig("figs/241111_fix_dose/frc_dose_{}.pdf".format(plot_dose))
#fig_ctf.savefig("figs/241111_fix_dose/ctf_dose_{}_dual.pdf".format(plot_dose))


# In[ ]:


#BF contrast
#BF contrast
#BF contrast
#BF contrast


# In[ ]:


#%matplotlib widget


# In[ ]:


aberrations = {'C30':0.88e-3/1e-10,'C12':0*2000,'C23':0*100000}
defocuses,diameters = plot_radius_on_defocus(extent=extent,energy=energy,sampling=sampling,semiangle_cutoff=12.4,aberrations=aberrations)
plt.plot(defocuses,diameters)


# In[ ]:


Probe(energy=energy,sampling=sampling,extent=extent,C30=aberrations['C30'],semiangle_cutoff=12.4,defocus=900).show()


# In[ ]:


Probe(energy=energy,extent=extent/2,**aberrations, gpts=256,semiangle_cutoff=12,defocus=629).show()


# In[ ]:


if True:
        semiangle_cutoff =  6#8#16    #8#20

        aberrations = {'C30':0.88e-3/1e-10,'C12':0*2000,'C23':0*100000}
        defocuses,diameters = plot_radius_on_defocus(extent=100,energy=energy,sampling=sampling,semiangle_cutoff=semiangle_cutoff,aberrations=aberrations,current_ratio=0.50)
        idx_min = np.argmin(diameters)
        defocus_chosen = defocuses[idx_min]
        plt.plot(defocuses,diameters)
        print(defocus_chosen)


# In[ ]:


defocuses,phases=plot_phase_on_defocus(energy=energy,semiangle_cutoff=3,aberrations=aberrations,gpts=256)
idx_min=np.argmin(phases)
defocuses[idx_min]


# In[ ]:


diameters


# In[ ]:


measurement.calibrations[1].sampling


# In[ ]:


extent/probe.ctf.nyquist_sampling


# In[ ]:


#matplotlib.interactive(True)

#mempool = cp.get_default_memory_pool()


#def run(input_i, output_path,single_patch=False, input_path="data/orig/", input_suffix=".jpg" ,device="gpu"):
if True:
    semiangle_cutoff = 3 #8#16    #8#20

    aberrations = {'C30':0.88e-3/1e-10,'C12':0*2000,'C23':0*100000}
    #defocuses,diameters = plot_radius_on_defocus(extent=extent,energy=energy,sampling=sampling,semiangle_cutoff=semiangle_cutoff,aberrations=aberrations,current_ratio=0.50)
    #idx_min = np.argmin(diameters)
    #defocus_chosen = defocuses[idx_min]

    defocuses,phases=plot_phase_on_defocus(energy=energy,semiangle_cutoff=semiangle_cutoff,aberrations=aberrations,gpts=256)
    idx_min=np.argmin(phases)
    defocus_chosen = defocuses[idx_min]
    print(defocus_chosen)
    
    #aberrations = get_aberrations(scale_all=1,aberrations_set={'C10':350})
    aberrations['C10']=-defocus_chosen
    probe = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff,rolloff=2, **aberrations, device=device,sampling=sampling,extent=extent)

    ##potential_p.project().show()
    ##probe.build().show()
    ##plt.plot(np.abs(probe.build().array[probe.gpts[0]//2,:]).get()**2)

    # Detector, scanning parameters

    detector_p = PixelatedDetector(None)

    I=50e-12
    spatial_sigma = get_gaussian_spread(probe.ctf.semiangle_cutoff,probe.energy,B_r,I)

    reciprocal_sampling=1/extent/(1/probe.wavelength)*1e3 # v mrad
    gpts=int(np.floor((4*semiangle_cutoff*1.1)/reciprocal_sampling))
    if gpts%2 == 1:
        gpts=gpts+1 # trochu vetsi vzorkovani (z x1.1 na trosku vice) abychom meli nakonec sudy pocet pixelu.
    scan_sampling = 1/( gpts*reciprocal_sampling*1e-3 * 1/probe.wavelength) # deset procent za idealnim vzorkovanim a tak abych mel krok v reciprokem prostoru stejny jako v groundtruth
    scan_gpts=int(200//scan_sampling)
    if scan_gpts%2 == 0:
        scan_gpts=scan_gpts-1
    #scan_sampling = sampling*direct_oversampling
    #scan_sampling = 1/(2*semiangle_cutoff*1e-3*1/probe.wavelength)
    #scan_gpts = 179#int(extent * 0.7/scan_sampling) # musi to byt liche.. abych mel skenovani zarovnane do stedu na 0.5 pixelu, lichy pixel doplnim pozdeji.. ;) 
    positions=get_positions(scan_gpts,np.array(probe.extent),scan_sampling)
    scan_p=PositionScan(positions=positions)

    ##

    phase_downsampled = get_downsampled_phase(potential_p,comparison_direct_oversampling,energy=probe.energy)
    if phase_downsampled is None:
        print("No downsampled phase")
        assert(True)
        #return(None)

    probe_dummy_mask=Probe(energy=energy, semiangle_cutoff=probe.ctf.semiangle_cutoff, **aberrations,extent=extent,sampling=sampling)
    mask = get_downsampled_mask(probe_dummy_mask, comparison_direct_oversampling, thr_fac = 0.1, positions = positions, temporal_sigma = 0, spatial_sigma = 0)
    #mask_finer = get_downsampled_mask(probe, direct_oversampling//2, thr_fac = 0.1, positions = positions, temporal_sigma = 0, spatial_sigma = 0)
    
    #|######### Measurement

    measurement=incoherent_scan_mc(probe,positions,detector_p,potential_p,temporal_sigma=0*temporal_sigma,spatial_sigma=0*spatial_sigma,max_batch_scan=256)
    #measurement=probe.scan(scan_p,detectors=detector_p,potential=potential_p,max_batch=50,pbar=False)

    measurement_crop=crop(measurement,extent=(angular_extent*2+1e-6,angular_extent*2+1e-6))

    # Downsampling diffraction pattern

    measurement_resampled = decimate_measurement(measurement_crop,reciprocal_oversampling)

    #| Apply noise or dont?



# In[ ]:


scan_sampling


# In[ ]:


probe.sampling


# In[ ]:


delta_alpha=measurement_noisy.calibrations[-1].sampling


# In[ ]:


1/(delta_alpha*1e-3*1/probe.wavelength)


# In[ ]:


if True:
    targeted_dose=5#5000/500*1000
    el=targeted_dose*scan_sampling**2
    print("el: ",el);print("t: ", el*c.e/I)
    
    norm=np.mean(np.sum(measurement.array,(-2,-1))) # average of fraction !uncropped! measurements(should be close to one, but it is a bit less then one cuz some electrons go past antialiasing aperture)
    if 1:
        measurement_noisy = poisson_noise(measurement_resampled/norm, dose=el,pixel_area=1)/el # el can be set based on current and dwell time or better it can be parametrized with targeted dose (el/angstron^2), then el=targeted_dose/max_dose_1, max_dose_1 depends on shape of a probe and step size.
    else:
        measurement_noisy = measurement_resampled

    measurement_noisy.show()
    plt.show()


# In[ ]:


import py4DSTEM 
N_dp=int(np.sqrt(measurement_noisy.array.shape[0]))
mee_=measurement_noisy.array.reshape(N_dp,N_dp,*measurement_noisy.shape[-2:]).transpose((0,1,2,3))
diff=gpts-N_dp
pad=diff//2
odd = diff%2
mee = np.pad(mee_,( (pad+odd,pad), (pad+odd,pad), (0,0),(0,0) ),mode='mean')
ca=py4DSTEM.data.calibration.Calibration()
ca["Q_pixel_size"]=np.squeeze(measurement_noisy.calibrations[1].sampling)
ca["R_pixel_size"]=scan_sampling
ca["Q_pixel_units"]="mrad"
ca["R_pixel_units"]="A"

dataset = py4DSTEM.DataCube(
    data=mee,
    calibration=ca
    
)


# In[ ]:


mee.shape


# In[ ]:


gpts


# In[ ]:


N_dp


# In[ ]:


gt_len=phase_downsampled.shape[0]
cut=int((gt_len-gpts)/2)
phase_downsampled_twice=np.real(np.fft.ifft2(np.fft.fftshift(np.fft.fftshift(np.fft.fft2(phase_downsampled))[cut:-cut,cut:-cut])))#pravdepodobne chybi normalizace


# In[ ]:


mean_dp=dataset.get_dp_mean()


# In[ ]:


py4DSTEM.show(mean_dp)


# In[ ]:


# Position the detector

# set the geometry by hand
center = np.array(mee.shape[-2:])/2
radius = semiangle_cutoff/ca["Q_pixel_size"]*0.8

# overlay selected detector position over mean dp
dataset.position_detector(
    mode = 'circle',
    geometry = (
        center,
        radius
    ),returnfig=True) #hmm to ze chybi return hodnota by mozna stalo za to reportnout... 

#fig.show()
fig=plt.gcf()
ax=plt.gca()
ax.axis("off")
#fig.savefig(fig_folder/"03_bf_detector.pdf")


# In[ ]:


# Capture the virtual BF

plt.figure()
# compute
dataset.get_virtual_image(
    mode = 'circle',
    geometry = (center,radius),
    name = 'bright_field',       # the output will be stored in `datacube`'s tree with this name
)

#py4DSTEM.show( dataset.tree('bright_field') , cmap="inferno")
im=dataset.tree('bright_field').data
ex=scan_sampling*im.shape[0]
extent_=(0,ex,0,ex)
plt.imshow(im,extent=extent_,cmap="inferno")
plt.xlabel("x [Å]");plt.ylabel("y [Å]")
#plt.savefig(fig_folder/"04_bf_radius{}px.pdf".format(radius))


# In[ ]:


plt.imshow(im,extent=extent_,cmap="inferno")


# In[ ]:


im_=im-im.mean()


# In[ ]:


x = np.linspace(-1,1,gpts)
y,x = np.meshgrid(x,x)
mask_comparison=np.sqrt(y**2+x**2)<0.35


# In[ ]:


mask_comparison.shape


# In[ ]:


im_.shape


# In[ ]:


phase_downsampled_twice.shape


# In[ ]:


h5file_path = fig_folder/"hdfs_bf"/'semiangle_{}_dose_{}.h5'.format(semiangle_cutoff,targeted_dose)
h5file_path.parent.mkdir(exist_ok=True,parents=True)
get_FRC_CTF(result=-im_, mask=mask_comparison, h5file_path=h5file_path, phase_downsampled=phase_downsampled_twice,registrate=False,thr=np.pi/30000,gaussian_sigma=4,real_space_sampling_=scan_sampling)


# In[ ]:


probe.wavelength/5.8


# In[ ]:


dpc = py4DSTEM.process.phase.DPC(
    datacube=dataset,
    energy=energy,
    verbose=True,
).preprocess(
    force_com_rotation=0,
    force_com_transpose=False,
)


# In[ ]:


py4DSTEM.show(
    np.linalg.norm([dpc._com_x,dpc._com_y],axis=0),
    cmap='inferno',
    vmin=0.0,
    vmax=0.999,
    ticks=False,
    scalebar=True,
)



# In[ ]:


plt.figure()
plt.imshow(np.linalg.norm([dpc._com_x,dpc._com_y],axis=0)*np.pi*2)
plt.colorbar()


# In[ ]:


dpc._reciprocal_sampling


# In[ ]:


measurement_noisy.calibrations[-1].sampling*1e-3*1/probe.wavelength


# In[ ]:


plt.figure()
plt.imshow(np.linalg.norm([dpc._com_x,dpc._com_y],axis=0),extent=extent_,cmap="inferno")
plt.xlabel("x [Å]");plt.ylabel("y [Å]")
#plt.savefig(fig_folder/"05_COM_mag.pdf")


# In[ ]:


dpc.sampling


# In[ ]:


rec=dpc.reconstruct(
    max_iter=32*4,
    reset=True,
    #stopping_criterion=1e-50,
    gaussian_filter_sigma = 0.000,#0.375, # in pixels since we haven't calibrated
)
rec.visualize(
    figsize=(6,7),
    cbar=True,
);


# In[ ]:


from abtem.utils import  energy2sigma


# In[ ]:


interaction_parameter = energy2sigma(energy=energy)


# In[ ]:


plt.figure()
#plt.imshow(potential.project().array*interaction_parameter)
plt.imshow(phase_downsampled)
#plt.imshow(rec.object_phase.T)
plt.colorbar()


# In[ ]:


#%matplotlib widget


# In[ ]:


xxx=(phase_downsampled[1:,1:]-phase_downsampled[:-1,1:])/scan_sampling
yyy=(phase_downsampled[1:,1:]-phase_downsampled[1:,:-1])/scan_sampling
plt.imshow(np.sqrt(xxx**2+yyy**2))
plt.colorbar()


# In[ ]:


plt.figure()
plt.imshow(rec.object_phase.T,origin="lower",extent=extent_,cmap="inferno")
plt.colorbar()
plt.xlabel("x [Å]");plt.ylabel("y [Å]")
#plt.savefig(fig_folder/"05_iCOM.pdf")


# In[ ]:


#plt.figure(figsize=(,20))
plt.imshow(rec.object_phase.T*2*np.pi,origin="lower",extent=extent_,cmap="gray")
plt.axis("off")
_,x_right = plt.xlim()
_,y_right = plt.ylim()
offset=60
plt.xlim(offset,x_right-offset)
plt.ylim(offset,y_right-offset)
bar_len=40 # in angstrom
scalebar = AnchoredSizeBar(plt.gca().transData,
                           bar_len, '{} nm'.format(bar_len//10), 'lower right', 
                           pad=0.1,
                           color='white',
                           frameon=False,
                           size_vertical=4)
                           #fontproperties=fontprops)

plt.gca().add_artist(scalebar)
plt.colorbar()
plt.savefig(fig_folder/"icom_semiangle_{}_dose_{}_defocus{}.pdf".format(semiangle_cutoff,targeted_dose,defocus_chosen))


h5file_path = fig_folder/"hdfs_icom_results"/'result_semiangle_{}_dose_{}.h5'.format(semiangle_cutoff,targeted_dose)
h5file_path.parent.mkdir(exist_ok=True,parents=True)
with h5py.File(h5file_path, 'w') as f:
        f.create_dataset("semiangle_cutoff", data= semiangle_cutoff)
        f.create_dataset("targeted_dose", data=targeted_dose)
        f.create_dataset("result_sampling", data=result_sampling)
        f.create_dataset("result", data=rec.object_phase.T*2*np.pi)
        f.create_dataset("C10", data=aberrations['C10'])
        f.create_dataset("C30", data=aberrations['C30'])


# In[ ]:


img=rec.object_phase*2*np.pi
pp=1
line=np.linspace(-1,1,img.shape[0])
y,x=np.meshgrid(line,line)
maskk=np.sqrt(x**2+y**2)<0.5
img_mod=np.real(np.fft.ifft2(np.fft.fftshift(np.pad(np.fft.fftshift(np.fft.fft2(img))*maskk,((pp,pp),(pp,pp))))))
plt.imshow(img_mod.T,origin="lower")


# In[ ]:


h5file_path = fig_folder/"hdfs_icom"/'semiangle_{}_dose_{}.h5'.format(semiangle_cutoff,targeted_dose)
h5file_path.parent.mkdir(exist_ok=True,parents=True)
res_=rec.object_phase*2*np.pi#-0.005
res=res_-np.sum(res_*mask_comparison)/np.sum(mask_comparison)+1e-4
asdfasdf=phase_downsampled_twice-np.sum(phase_downsampled_twice*mask_comparison)/np.sum(mask_comparison)+1e-4
get_FRC_CTF(result=res, mask=mask_comparison, h5file_path = h5file_path, phase_downsampled=asdfasdf+0*phase_downsampled_twice, registrate=False,thr=np.pi/3000,gaussian_sigma=4,real_space_sampling_=scan_sampling)


# In[ ]:


1/(6e-3*1/probe.wavelength)


# In[ ]:


h5file_path


# In[ ]:


from scipy import interpolate


# In[ ]:


from sklearn.metrics import r2_score


# In[ ]:


def r2_score_custom(y,f):
    SS_res = np.sum ( (y-f)**2)
    SS_tot=np.sum((y-y.mean())**2)
    return(1-SS_res/SS_tot)


# In[ ]:


paths=[]
path="figs/241113/hdfs_icom/semiangle_3_dose_10000.0.h5" # bez sumu
paths.append(path)
path="figs/241113/hdfs_icom/semiangle_6_dose_10000.0.h5" # bez sumu
paths.append(path)
path="figs/241113/hdfs_icom/semiangle_12_dose_10000.0.h5" #bez sumu
paths.append(path)

#path="figs/241113/hdfs_icom/semiangle_12.1_dose_100000000.0.h5";paths.append(path) # bez sumu
#path="figs/241113/hdfs_icom/semiangle_12.1_dose_10000.0.h5";paths.append(path) # s sumem
plt.figure(figsize=(12,7.5))
colors=["r","g","b","k"]
for i_path,path in enumerate(paths):
    with h5py.File(path,"r") as f:
        ctf_y=f["ctf_y"][()]
        ctf_x=f["ctf_x"][()]
        semiangle_cutoff_ = f["semiangle_cutoff"][()]
        C30 = f["C30"][()]
        C10 = f["C10"][()]
        print(C30)
        print(C10)
    
    probe_iCOM_plot = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff_, gpts=np.array(probe.gpts), extent=np.array(probe.extent)*4,C30=C30,C10=C10)
    iCOM_CTF=np.abs( np.conjugate(np.fft.fft2(np.abs(probe_iCOM_plot.build().array)**2)) ) * np.product(probe_iCOM_plot.gpts)
    
    q_,I_,_ = py4DSTEM.phase.utils.return_1D_profile(iCOM_CTF,pixel_size=probe_iCOM_plot.sampling)
    teor_ctf_x = q_/(1/probe_iCOM_plot.wavelength)*1e3
    plt.plot(teor_ctf_x, I_,"--",color=colors[i_path],label=r"$\alpha_\mathrm{{a}}={}\,$mrad theor".format( int(semiangle_cutoff_) ))
    plt.plot(ctf_x,ctf_y,".",color=colors[i_path],label=r"$\alpha_\mathrm{{a}}={}\,$mrad".format( int(semiangle_cutoff_) ))

    f = interpolate.interp1d(teor_ctf_x, I_)
    teor_comp_x = ctf_x

    comp_y = ctf_y
    teor_comp_y = f(teor_comp_x)

    mask=np.logical_and(teor_comp_y > 0.01,ctf_y > 0.01)

    comp_y_masked=comp_y[mask]
    teor_comp_x_masked=teor_comp_x[mask]
    teor_comp_y_masked=teor_comp_y[mask]

    print("semiangle cutoff: ", semiangle_cutoff_)
    print(r2_score(teor_comp_y_masked, comp_y_masked, force_finite=False))
    print(r2_score_custom(teor_comp_y_masked, comp_y_masked))

    plt.plot(teor_comp_x_masked,teor_comp_y_masked,color="k")
    

secax_ctf = plt.gca().secondary_xaxis('top', functions=(a2s, s2a))
secax_ctf.set_xlabel(r'$k$ [$\mathrm{\AA}^{-1}$]')
plt.xlim(-1,30)
plt.legend()
plt.xlabel(r"$\alpha$ [mrad]")
plt.ylabel(r"|CTF| $\cdot 2\pi$")
#plt.savefig("figs/241113/ctf_theor_dose_inf_dual.pdf")
plt.show()


# In[ ]:


fig_frc,ax_frc=plt.subplots(1,1)
fig_ctf,ax_ctf=plt.subplots(1,1)
for file in paths:
    with h5py.File(file, 'r') as f:
        #if f['targeted_dose'][()] == plot_dose:
            ax_frc.plot(f['frc_x'][()][0] ,f['frc_y'][()],label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ))
            ax_ctf.plot(f['ctf_x'][()] ,f['ctf_y'][()],".",label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ))
ax_frc.set_xlabel(r"$\alpha$ [mrad]")
ax_frc.set_ylabel(r"FRC")
ax_frc.axhline(y=0.143,linestyle="--",color="k",label='FRC = 0.143')


ax_ctf.legend()
ax_ctf.set_xlabel(r"$\alpha$ [mrad]")
ax_ctf.set_ylabel(r"CTF $\cdot 2\pi$")
ax_frc.legend()
#fig_frc.savefig("figs/241113/frc_dose_{}.pdf".format("inf"))
#fig_ctf.savefig("figs/241113/ctf_dose_{}.pdf".format("inf"))


# In[ ]:


hdfs


# In[ ]:


hdfs_path="figs/241114iCOM_nanotubes/hdfs_icom/"
#hdfs_path=fig_folder/'hdfs'

hdfs=list(pathlib.Path(hdfs_path).glob('*.h5'))
hdfs=natsorted(hdfs)

colors=["r","g","b","k"]
plot_dose=80.0
fig_frc,ax_frc=plt.subplots(1,1)
fig_ctf,ax_ctf=plt.subplots(1,1)
i_color=0
for file in hdfs:
    with h5py.File(file, 'r') as f:
        if f['targeted_dose'][()] == plot_dose:
            ax_frc.plot(f['frc_x'][()][0] ,f['frc_y'][()],label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ),color=colors[i_color])
            ax_ctf.plot(f['ctf_x'][()] ,f['ctf_y'][()],".",label=r'$\alpha_\mathrm{{a}} = {}\,$mrad'.format(f['semiangle_cutoff'][()] ),color=colors[i_color])
            i_color+=1
ax_frc.set_xlabel(r"$\alpha$ [mrad]")
ax_frc.set_ylabel(r"FRC")
ax_frc.axhline(y=0.143,linestyle="--",color="k",label='FRC = 0.143')


ax_ctf.legend()
ax_ctf.set_xlabel(r"$\alpha$ [mrad]")
ax_ctf.set_ylabel(r"CTF")
ax_frc.legend()
#fig_frc.savefig("figs/241114iCOM_nanotubes/frc_dose_{}.pdf".format(plot_dose))
#fig_ctf.savefig("figs/241114iCOM_nanotubes/ctf_dose_{}.pdf".format(plot_dose))


# In[ ]:


#STARE
#STARE
#STARE


# In[ ]:


#Following crops ground truth to the reconstruction extent. e.g. If the result of the reconstruction is only an inset of the groundtrouth.. so I can change decimation of measurement as I wish.. but the maximal scattering angle should be the same as in the groundtruth..
gpts = result.shape[0]
center = phase_downsampled.shape[0]//2
sel_slice = slice(center-gpts//2,center+gpts//2)
phase_downsampled_crop = phase_downsampled[sel_slice,sel_slice]
mask_crop = mask[sel_slice,sel_slice]

from scipy.ndimage import gaussian_filter
mask_taper = gaussian_filter(mask_crop*1.0, sigma=10*comparison_direct_oversampling)


# In[ ]:


mask_crop.shape


# In[ ]:


plt.figure()
plt.imshow(mask_taper)


# In[ ]:


plt.figure()
plt.imshow(phase_downsampled_crop.T,origin="lower",cmap="gray")


# In[ ]:


phase_downsampled_crop.shape


# In[ ]:


G_ref = np.fft.fft2(phase_downsampled_crop)
G = np.fft.fft2(result)
upsample_factor = 50
shift_ = align_images_fourier(G_ref,G,upsample_factor=upsample_factor,device="cpu")
shift, error, diffphase = phase_cross_correlation(G_ref, G, upsample_factor=upsample_factor,space='fourier')


# In[ ]:


potential.project().show()


# In[ ]:


#hmm?
from abtem.utils import fft_shift
result_shifted=np.real(fft_shift(result,shift))

center_patch = result_shifted.shape[0]//2
gpts_patch = mask_taper.shape[0]
sel_patch_slice = slice(center_patch-gpts_patch//2,center_patch+gpts_patch//2)
result_shifted_=result_shifted[sel_patch_slice,sel_patch_slice]*mask_taper#*mask_crop
phase_downsampled_crop_=phase_downsampled_crop[sel_patch_slice,sel_patch_slice]*mask_taper#*mask_crop


# In[ ]:


plt.figure()
plt.imshow((phase_downsampled_crop_).T,origin="lower",cmap="gray")


# In[ ]:


plt.figure()
plt.imshow((result_shifted_).T,origin="lower",cmap="gray")


# In[ ]:


real_space_sampling=1/(measurement_decimated.calibrations[-1].sampling*measurement_decimated.shape[-1]*1e-3*(1/probe.wavelength))


# In[ ]:


real_space_sampling


# In[ ]:


q,frc,half_bit=py4DSTEM.process.phase.utils.fourier_ring_correlation(phase_downsampled_crop_, result_shifted_, pixel_size=(real_space_sampling,)*2, bin_size=None, sigma=None, align_images=False, upsample_factor=16, device='cpu', plot_frc=True, frc_color='red', half_bit_color='blue')


# In[ ]:


plt.figure()
plt.plot(q/(1/probe.wavelength)*1e3,frc,"r",label="FRC")
plt.plot(q/(1/probe.wavelength)*1e3,half_bit,"b",label="half bit")
plt.legend()


# In[ ]:


plt.figure()
plt.plot(q/(1/probe.wavelength)*1e3,frc,"r",label="FRC")
plt.plot(q/(1/probe.wavelength)*1e3,half_bit,"b",label="half bit")
plt.legend()


# In[ ]:


1/(20e-3*1/probe.wavelength)


# In[ ]:


0.4/(1/probe.wavelength)


# In[ ]:


semiangle_cutoff


# In[ ]:


#################################################################################################################
# CTF comparison - Low-dose phase retrieval of biological specimens using cryo-electron ptychography by Liqi Zhou
#################################################################################################################


# In[ ]:


plt.figure()
from abtem.utils import spatial_frequencies
kx_,ky_ = spatial_frequencies(result_shifted_.shape,(real_space_sampling,real_space_sampling))
kx_,ky_ = np.meshgrid(kx_,ky_)

alpha_x = np.fft.fftshift(kx_)*probe.wavelength*1e3
alpha_y = np.fft.fftshift(ky_)*probe.wavelength*1e3
alpha=np.sqrt(alpha_x**2+alpha_y**2)
mask_block_zero=alpha>1.5

plt.imshow(np.fft.fftshift(np.abs(np.fft.fft2(result_shifted_))))
plt.figure()
plt.imshow(np.fft.fftshift(np.abs(np.fft.fft2(phase_downsampled_crop_)))*mask_block_zero)


# In[ ]:


np.array([1,2,3,4])[slice(1,1+2)]


# In[ ]:


def correlation_map(array_1,array_2,pattern_size_px=3):
    assert(array_1.shape == array_2.shape)
    assert(pattern_size_px%2 == 1)
    ret = np.zeros(array_1.shape)
    pad_width=(pattern_size_px-1)//2
    array_1_p=np.pad(array_1,pad_width=((pad_width,pad_width),(pad_width,pad_width)))
    for i in range(ret.shape[0]):
        for j in range(ret.shape[0]):
            i_slice = slice(i,i+pattern_size_px)
            j_slice = slice(j,j+pattern_size_px)
            ar_1 = array_1[i_slice,j_slice].reshape(-1)
            ar_2 = array_2[i_slice,j_slice].reshape(-1)
            c_mixed = np.real(np.dot(ar_1,np.conjugate(ar_2)))
            c_1 = np.sum(np.abs(ar_1)**2)
            c_2 = np.sum(np.abs(ar_2)**2)
            ret[i,j] = c_mixed/(c_1*c_2)**0.5
            
    return(ret)

cor = correlation_map(np.fft.fftshift(np.fft.fft2(phase_downsampled_crop_)),np.fft.fftshift(np.fft.fft2(result_shifted_)),pattern_size_px=3)
plt.figure()
plt.imshow(cor>0.8)
    #for pattern_size_px > 
    
    #pad_width=3-array_1.shape[0]%3
    #new_array=np.pad(np.array([[1,1],[1,1]]),pad_width=((0,pad_width),(0,pad_width)))
    


# In[ ]:


# np.pad(np.array([[1,1],[1,1]]),pad_width=((0,0),(0,1)))


# In[ ]:


#potential.project().array.shape


# In[ ]:


pot_array=potential.project().array
pot_ex = potential.extent[0]
alpha_nyq=1/pot_ex*pot_array.shape[0]/(1/probe.wavelength)*1e3
pot_fft = np.fft.fftshift(np.abs(np.fft.fft2(pot_array)))[pot_array.shape[0]//2,:]


# In[ ]:


plt.figure()
xx=np.linspace(-alpha_nyq/direct_oversampling,alpha_nyq/direct_oversampling,result_shifted_.shape[1])
y=np.fft.fftshift(np.abs(np.fft.fft2(result_shifted_)))[64,:]
plt.plot(xx,y/y.max()/5) # svevolne podelim 5 aby mi krivky pekne sedly na porovnani okem.. 
yy=np.fft.fftshift(np.abs(np.fft.fft2(phase_downsampled_crop_)))[64,:]
plt.plot(xx,yy/yy.max())
plt.plot(np.linspace(-alpha_nyq,alpha_nyq,len(pot_fft)),pot_fft/pot_fft.max())
plt.yscale('log')


# In[ ]:


plt.figure()
for i in [0]:#,1,2,4,8,16,32]:
    ctf=np.abs(np.fft.fft2(result_shifted_))/(np.abs(np.fft.fft2(phase_downsampled_crop_)) +i )
    q,I,_ = py4DSTEM.phase.utils.return_1D_profile(ctf,pixel_size=(real_space_sampling,)*2)
    plt.plot(q/(1/probe.wavelength)*1e3,I,".")
#plt.ylim(-1,1)


# In[ ]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.figure()
plt.imshow(np.log(np.abs((np.abs(np.fft.fft2(phase_downsampled_crop_))))))


# In[ ]:


plt.figure()
plt.imshow(np.fft.fftshift(ctf),extent=((alpha_nyq/direct_oversampling,-alpha_nyq/direct_oversampling)*2))


# In[ ]:


#orig.max()


# In[ ]:


phase_downsampled_crop_


# In[ ]:


#phase_downsampled_crop_tmp
#plt.figure()
#plt.imshow(orig_tmp)


# In[ ]:


croparino=fft_crop(np.fft.fft2(phase_downsampled_crop_),(1024,)*2)
plt.figure()
plt.imshow(np.abs(np.fft.ifft2(croparino))*np.size(croparino)/np.size(phase_downsampled_crop_)) # toto je 100% jasne,.. sice to je analogie problemu co mam nize, ale je to s hranatou maskou.. nicmene ja mam masku dobre prostorove vymezenou, tak je snad skoro hranata..
plt.figure()
plt.imshow(phase_downsampled_crop_)


# In[ ]:


x=np.linspace(0,1,1024,endpoint=False)
y=np.sin(np.pi*2*32*x)
ff=np.fft.fft(y)/len(x)
yy=np.array([y for _ in range(len(x))])
p=512
#yy=np.pad(yy,((p,p),(p,p)))
from abtem.utils import fft_crop

#ff2=fft_crop(np.fft.fftshift(yy),(2048,)*2)
ff2=np.fft.fft2(yy)#/np.size(yy)
plt.figure()
plt.imshow(np.fft.fftshift(np.abs(ff2))/len(x)**2)#*np.size(yy)/len(x)**2)
print(np.mean(np.abs(yy)))
np.mean(np.abs(yy))


# In[ ]:


plt.figure()
plt.imshow(np.abs(ff2)**2)


# In[ ]:


plt.figure()
plt.imshow(yy)


# In[ ]:


plt.figure()
mask_gaussian = np.exp(-((x-0.50)/2.5e-1)**20000000)
y_masked=y*mask_gaussian
plt.plot(x,y_masked)
plt.figure()
ff_masked_lin=np.fft.fftshift(np.fft.fft(y_masked))/np.size(y_masked)
plt.plot(x,np.abs(ff_masked_lin))
plt.figure()
plt.plot(mask_gaussian)
plt.figure()
plt.plot(np.fft.fftshift(np.abs(np.fft.fft(mask_gaussian)))/np.size(mask_gaussian),'.')


# In[ ]:


print(phase_downsampled_crop_.max())
normalisation = np.size(phase_downsampled_crop_)
print(normalisation)
orig = np.fft.fftshift(np.abs( np.fft.fft2(phase_downsampled_crop_)))/normalisation #...normalizace
print(orig.max())
np.mean(phase_downsampled_crop_)


# In[ ]:


phase_downsampled_crop_.shape


# In[ ]:


#print(phase_downsampled_crop_tmp.max())
#normalisation_tmp = np.size(phase_downsampled_crop_tmp)
#print(normalisation_tmp)
#orig_tmp = np.fft.fftshift(np.abs( np.fft.fft2(phase_downsampled_crop_tmp)))/normalisation_tmp #...normalizace
#print(orig_tmp.max())
#np.mean(phase_downsampled_crop_tmp)


# In[ ]:


np.mean(phase_downsampled_crop_)


# In[ ]:


#np.mean(phase_downsampled_crop_tmp)


# In[ ]:


plt.figure()
plt.imshow(phase_downsampled_crop_)
plt.figure()
#plt.imshow(phase_downsampled_crop_tmp)


# In[ ]:


plt.figure()
plt.imshow(orig>0.004)
#plt.figure()
#plt.imshow(orig_tmp>0.001)


# In[ ]:


plt.figure()
y=orig[63,:]
x = np.linspace(0,1,len(y))
plt.plot(x,y) 
#y=orig_tmp[127,:]*4
#x = np.linspace(0,1,len(y))
#plt.plot(x,y) 


# In[ ]:


plt.figure()
plt.imshow(mask_taper>0.1)


# In[ ]:


np.size(mask_taper)/np.sum(mask_taper>0.1)


# In[ ]:


mask_normalisation = np.size(mask_taper)/np.sum(mask_taper>.1) # normalizace kvuli omezememu fov. # mask_normalisation -> za predpokladu, ze by maska byla uplne hranata.. (coz by uz kvuli frc nebylo dobre), tak by se jednalo o simulaci na mensim foV a mohl bych treba obrazek oriznout, dostal bych vlastne umerne zvetseny pixel v reciprokem prostoru a tim padem by se mi intenzita v tomto zvetsenem pixelu nascitala z okoli.. o kolik by se zvetsilo je dane velikosti orezu: np.size(mask_taper)/np.size(mask_taper>0) ...uplne nevim jak by se to melo udelat spravne pro pripad nehranate masky, ale je to v tomto pripade asi jedno...
normalisation = np.prod(phase_downsampled_crop_.shape) 
orig = np.fft.fftshift(np.abs( np.fft.fft2(phase_downsampled_crop_)))/normalisation*mask_normalisation #...normalizace
mask_hist = orig>np.pi/2000
#mask_hist = cor>0.5
bins_hist = 64
plt.figure()
plt.imshow(mask_hist)
ring_brightness, radius_edges = radial_histogram(np.fft.fftshift(ctf),mask_hist,bins=bins_hist)
radius = (radius_edges[1:]+radius_edges[0:-1])/2
plt.figure()
plt.plot(radius,ring_brightness,'.')


# In[ ]:


ring_brightness_, radius_edges_ = radial_histogram(np.ones_like(ctf),mask_hist,bins=bins_hist)


# In[ ]:


plt.figure()
sampling = 1/real_space_sampling/ctf.shape[0]/(1/probe.wavelength)*1e3
plt.plot(radius*sampling,ring_brightness_,'.')


# In[ ]:


delta_radius=radius[1]-radius[0]
N_mezikruzi = ( np.pi*((radius+delta_radius)*sampling)**2 - np.pi*(radius*sampling)**2 )/sampling**2 # pocet pixelu, ktere jsou v mezikruzi radius az radius + delta_radius


# In[ ]:


plt.figure()
plt.plot(radius*sampling,N_mezikruzi)
plt.plot(radius*sampling,ring_brightness_)
plt.figure()
plt.plot(radius*sampling,ring_brightness_/(N_mezikruzi))


# In[ ]:


plt.figure()
#mask_thr=ring_brightness_>6
mask_thr=(ring_brightness_/N_mezikruzi)>0.05
ctf_hist=ring_brightness/ring_brightness_
plt.plot(radius[mask_thr]*sampling,ctf_hist[mask_thr],'.')
plt.ylim(0,1.5)


# In[ ]:


#CTF Nakonec mi vyhrala metoda na ziskani CTF: vypocitat standardne CTF, ale nez se udela radialni prumer,
# tak ignoruji ty polomery, ktere maji malo reprezentantu (mene jak ??0.4?? z maximalniho poctu pro dany polomer), 
# reprezanty zahazuji, kdyz maji FFT(faze) mensi nez nejaky ??svevolny?? threshold (rozptyluji malo). 

#CTF podle korelace casti obrazku se nezda ze by fungovalo nejlepe...


# In[ ]:


################################################################################
################################################################################
################################################################################


# In[ ]:


### Fourier shell correlation threshold criteria by Marin van Heel, Michael Schatz
#########################################################################################
# nebo obrázek Non-unique games over compact groups and orientation estimation in cryo-EM
#########################################################################################


# In[ ]:


from skimage import data
N=100
camera = data.camera()
px_x,px_y=camera.shape
fac=4
camera = np.average(camera.reshape(px_x//fac,fac,px_y//fac,fac),axis=(1,3)).reshape(px_x//fac,px_y//fac)


# In[ ]:


camera_shift=np.real(fft_shift(camera,-np.array([4,2])))
align_images_fourier(np.fft.fft2(camera),np.fft.fft2(camera_shift),upsample_factor=1000)


# In[ ]:


rnd_images=np.random.uniform(-1,1,(N,)+camera.shape)
registered=np.zeros_like(rnd_images)
for i in range(N):
    image = rnd_images[i]
    shift = align_images_fourier(np.fft.fft2(camera),np.fft.fft2(image),upsample_factor=1000)#phase_cross_correlation(camera, image, upsample_factor=10)
    print(shift)
    registered[i] = np.real(fft_shift(image,shift))


# In[ ]:


plt.figure()
plt.imshow(np.sum(registered,axis=0))


# In[ ]:


#########################################################################################
#Pouze externi knihovny
#########################################################################################


# In[ ]:


from skimage import data
from scipy.ndimage import fourier_shift

def shift_image(image,shift):
    return np.real(np.fft.ifft2(fourier_shift(np.fft.fft2(image),shift=shift)))
N=400
camera = data.camera()
px_x,px_y=camera.shape
fac=4
camera = np.average(camera.reshape(px_x//fac,fac,px_y//fac,fac),axis=(1,3)).reshape(px_x//fac,px_y//fac)


# In[ ]:


rnd_images=np.random.uniform(-1,1,(N,)+camera.shape)
registered=np.zeros_like(rnd_images)
for i in range(N):
    image = rnd_images[i]
    print(shift)
    shift = align_images_fourier(np.fft.fft2(camera),np.fft.fft2(image),upsample_factor=4)#phase_cross_correlation(camera, image, upsample_factor=10)
    print(shift)
    registered[i] = shift_image(image,shift)


# In[ ]:


plt.figure()
plt.imshow(np.sum(registered,axis=0))

