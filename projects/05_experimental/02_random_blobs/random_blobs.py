#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os
#os.environ["MKL_NUM_THREADS"] = "16"
#os.environ["NUMEXPR_NUM_THREADS"] = "16"
#os.environ["OMP_NUM_THREADS"] = "16"


# In[ ]:


import numpy as np
import matplotlib.pyplot as plt
from abtem import *
from abtem.utils import GaussianDistribution
from ase.io import read
from ase.build import surface, bulk
from tqdm.auto import tqdm
from ase.build import graphene

from tqdm.auto import tqdm
from abtem.structures import orthogonalize_cell
from abtem.measure import center_of_mass


# In[ ]:


from ase.build import nanotube


# In[ ]:


from abtem.detect import AnnularDetector
from abtem.noise import poisson_noise
import scipy.constants as c
from abtem.reconstruct import invms
from abtem.utils import energy2sigma                                                                                                                                                           
from abtem.custom import incoherent_scan,incoherent_scan_mc, get_gaussian_spread
import ase
from abtem.custom import incoherent_probe, get_probe_radius
from abtem.custom import get_radiation_dose
from abtem.scan import PositionScan


# In[ ]:


import matplotlib
matplotlib.rcParams["figure.figsize"] = (15, 15)
matplotlib.rcParams['lines.linewidth'] = 2
font = {'weight' : 'bold',
                'size'   : 15}
matplotlib.rc('font', **font)


# # Projection potential of silicon nitride Si$_3$N$_4$

# In[ ]:


from ase.io import read
#si3n4 = read('Si3N4_symetrized.cif')
si3n4 = read('Si3N4.cif') #https://materialsproject.org/materials/mp-988
si3n4 = orthogonalize_cell(si3n4)*(1,1,1)
show_atoms(si3n4)
show_atoms(si3n4,plane="xz")


# In[ ]:


np.sum(si3n4.get_masses())*c.atomic_mass/(si3n4.get_volume()*1e-10**3)*1000/100**3


# In[ ]:


si3n4_dimensions=np.array(si3n4.cell).diagonal()
print(si3n4_dimensions)


# In[ ]:


si3n4.get_volume()


# In[ ]:


potential_si3n4 = Potential(si3n4,
                      sampling=0.1,
                      slice_thickness=1,
                      projection='infinite',
                      parametrization='kirkland')


# In[ ]:


mean_potential_si3n4=np.mean(potential_si3n4.build().project().array)/si3n4_dimensions[2] #Projection potential per projection length
print(mean_potential_si3n4)


# # Projection potential of amorphous SiO2 (silica)

# In[ ]:


density_sio2_a = 2.196 *1e-3/(1e-2)**3 #in kg/m^-3 # source https://en.wikipedia.org/wiki/Silicon_dioxide


# In[ ]:


m = (ase.Atom("Si").mass + 2*ase.Atom("O").mass)*c.atomic_mass #Mass in kg of 1x Si and 2x O
v = m/density_sio2_a #volume in meters cubed containint 1x Si and 2x O


# In[ ]:


a = (v)**(1/3)/1e-10 # size of box side in angstrom containing 1x Si and 2x O


# In[ ]:


sio2_a = ase.Atoms(symbols=["Si","O","O"],positions=[[a/2,a*1/4,a/2],[a/2,a/2,a/2],[a/2,a*3/4,a/2]],cell=[a,a,a]) # the atomic structure is not physical, it sole purpose is for calculating an estimate of average potential in amorphous sio2


# In[ ]:


sio2_a_dimensions=np.array(sio2_a.cell).diagonal()


# In[ ]:


show_atoms(sio2_a,plane="yz")


# In[ ]:


sio2_a.get_masses().sum()*c.atomic_mass/(sio2_a.get_volume()*1e-10**3)


# In[ ]:


density_sio2_a


# In[ ]:


potential_sio2_a = Potential(sio2_a,
                      sampling=0.1,
                      slice_thickness=3,
                      projection='infinite',
                      parametrization='kirkland')


# In[ ]:


mean_potential_sio2_a=np.mean(potential_sio2_a.build().project().array)/sio2_a_dimensions[2] #Projection potential per projection length
print(mean_potential_sio2_a)


# # Reconstruction

# In[ ]:


mean_potential = mean_potential_sio2_a


# In[ ]:


probe_dummy=Probe(energy=30e3)


# In[ ]:


#params for sweep
select=4
semiangles=np.array([1.5,3,6,12,24])
extents=1/(2*semiangles*1e-3/256*1/probe_dummy.wavelength)
defocuses =  np.ones_like(semiangles)*6000#uncomment. 40000*6.5/semiangles*extents/extents[2] #np.array([320000, 160000,  80000,  40000])
nyq_samplings = 1/(2*semiangles*1e-3*1/probe_dummy.wavelength)
samplings = nyq_samplings/4 


# In[ ]:


defocuses


# In[ ]:


extents


# In[ ]:


gptss=(extents/samplings).astype(int)
print(gptss)


# In[ ]:


slice_thickness = 4
sampling = samplings[select]
num_slices = 51
gpts=np.array((gptss[select],)*2)
extent_x = sampling*gpts[0]


# In[ ]:


def get_blob(shape,xx,yy,A0,A1,A2,phi1,phi2):
    #https://stackoverflow.com/questions/54828017/c-create-random-shaped-blob-objects
    blob=np.zeros(shape)
    r_sq=xx**2+yy**2
    phi=np.arctan2(yy,xx)
    
    radius = A0 + A1*np.cos(1*phi+phi1) + A2*np.cos(16*phi+phi2)
    radius_sq=radius**2
    blob[r_sq<radius_sq]=1
    return(blob)
def get_piled_blobs(potential,A0_max,fov_fac=1): # modifies potential.array # fov_fac - if you want eg. to make the potential the same but on two times larger field of view put fac=0.5
    A0_min=A0_max/4
    A0=np.random.uniform(0,1)**8*(A0_max-A0_min)+A0_min#np.random.uniform(A0_max/4,A0_max)
    A1_max=A0/2
    A1=np.random.uniform(0,A1_max)
    phi1=np.random.uniform(0,2*np.pi)
    A2=np.random.uniform(0,A1/5)
    phi2=np.random.uniform(0,2*np.pi)
    offset=A0+A1
    
    dx=potential.sampling[0]
    num = int((A0+A1+A2)*2/dx)
    lin = np.linspace(-0.5*dx*num,0.5*dx*num,num,endpoint=False)
    xx,yy = np.meshgrid(lin,lin)
    new_sh = (num,)*2
    pot_sh = np.shape(potential.array[0])
    
    pot_height=potential.slice_thicknesses[0]*potential.num_slices
    
    if fov_fac==1:
        fov_shift=0
    if fov_fac!=0:
        fov_shift=int(np.floor(pot_sh[0]/2-pot_sh[0]*fov_fac/2))
    idx_x = int(np.floor(np.random.uniform(0,1)*fov_fac*(pot_sh[0]-new_sh[0]))+fov_shift)#np.random.randint(0,pot_sh[0]-new_sh[0])
    idx_y = int(np.floor(np.random.uniform(0,1)*fov_fac*(pot_sh[1]-new_sh[1]))+fov_shift)#np.random.randint(0,pot_sh[1]-new_sh[1])
    
    R = (A0+A1)/(A0_max+A1_max)*pot_height/2 # R is radius given as fraction of pot_height/2
    xs = np.linspace(-1,1,potential.num_slices)*pot_height/2; # spherical shape
    
    for idx in range(potential.num_slices):
        x = xs[idx]
        if abs(x) > R: # if sphere is smaller than height of potential given potential slice should be zero
            fac = 0 
        else:
            fac = np.sqrt(1-(x/R)**2) 
            
        potential.array[idx][idx_x:idx_x+new_sh[0],idx_y:idx_y+new_sh[1]] += get_blob(new_sh,xx,yy,A0*fac,A1*fac,A2*fac,phi1,phi2)
    
    return 


# In[ ]:


energy = 30e3
semiangle_cutoff = semiangles[select]
aberrations = {
    'defocus': defocuses[select],
    'C30': 0.88e-3/1e-10,
    'C50': 0.81e-3/1e-10}

#Cc = 1.34e-3/1e-10


# In[ ]:


#%%time
array=np.zeros((num_slices,)+tuple(gpts),dtype=np.float64)
potential = PotentialArray(array=array,slice_thicknesses=np.array([slice_thickness]*num_slices),extent=gpts*sampling,sampling=sampling)
np.random.seed(seed=25)
for i in range(int(80*extents[select]/extents[2])):
    get_piled_blobs(potential,A0_max=100)
#for i in range(int(50*extents[select]/extents[2])):
#    get_piled_blobs(potential,A0_max=100)

#phase_shift=np.pi
interaction_parameter = energy2sigma(energy)                                                                                                                                               
proj_pot_val_eva = mean_potential*slice_thickness#phase_shift/interaction_parameter/potential.num_slices
potential.array[:] *= proj_pot_val_eva
print(proj_pot_val_eva*potential.num_slices*interaction_parameter) # phase shift


# In[ ]:


potential.project().show()


# In[ ]:


#from abtem.custom import get_potential
#potential_phase_plate = get_potential(phase_shift=phase_shift,extent=extent_dummy,energy=energy,gpts=gpts,num=num)


# In[ ]:


#probe = SMatrix(energy=energy,interpolation=1,semiangle_cutoff=semiangle_cutoff, expansion_cutoff=semiangle_cutoff*2+0.5,**aberrations, device='gpu')
probe = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff,**aberrations, device='gpu')
probe.grid.match(potential)

#probe.build().collapse().show();
probe.show()


# In[ ]:


plt.figure()
plt.imshow(np.abs(np.fft.fftshift(np.fft.fft2(probe.build().array.get() ))))


# In[ ]:


# maximal slice size
dr=1/(probe.ctf.semiangle_cutoff*1e-3*1/probe.wavelength)
print(2*(dr)**2/probe.wavelength)


# In[ ]:


# what should be simulation extent for given semiangle - 6.5 mrad with 256 pixels in detector.
# also this limits the maximal probe size.. it should fit with enough reserve into the calculation domain
extent=1/(2*6.5e-3/256*1/probe.wavelength)
print("extent: ",extent)
# resulting sampling for arbitrary choise of 512 simulation pixels.. note that sampling should be well bellow minimal wavelength which is expected to be in the wave function (probe before the sample has maximal wevelength given by aperture semiangle!).. 
print("sampling: ",extent/512)


# In[ ]:


#given maximal spatial frequency by aperture angle; maxmal details will have period 0.5 angstrom
1/(2*6.5e-3*1/probe.wavelength)


# In[ ]:


energy_spread = 1.6
Cc = 1.34e-3/1e-10
focal_spread = energy_spread/probe.energy * Cc # 1/e width of focal distribution
temporal_sigma = focal_spread/np.sqrt(2) # std of focal distribution (normal distribution is proportional to e**( 1/2*(x/std)**2) )
I=100e-12
B_r=1e8

spatial_sigma = get_gaussian_spread(probe.ctf.semiangle_cutoff,probe.energy,B_r,I)


# In[ ]:


incoh_probe=incoherent_probe(probe,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma)
incoh_probe.show()

r95, _, _, _= get_probe_radius(probe_measurement=incoh_probe,current_ratio=0.95)
d95=2*r95
print(r95)


# In[ ]:


detector=PixelatedDetector(None)
scan_sampling= d95*0.3/2#120*extents[select]/extents[2]/2# sampling should be at least 0.7 of probe width
scan_points = 10 # number of scan points in one direction
offset=np.array([0,0])
start=np.array(potential.extent)/2-scan_sampling*scan_points//2+offset
end=np.array(potential.extent)/2+scan_sampling*scan_points//2+offset
scan_=GridScan(start,end,sampling=scan_sampling)
positions=scan_.get_positions()+np.random.uniform(-5,5)
scan = PositionScan(positions)
measurement = probe.scan(scan,detector,potential)


# In[ ]:


temporal_sigma


# In[ ]:


measurement_incoherent = incoherent_scan_mc(probe,positions=positions,detector=detector,potential=potential,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma*0)
#measurement_incoherent = incoherent_scan(probe,scan,detector_p=detector,potential_p=potential,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma*0)


# In[ ]:


measurement_incoherent[7].show(power=0.2)


# In[ ]:


measurement[7].show(power=0.2)


# el=6280000/9/10/10*20*1000
# pixel_area=1
# measurement_noisy = poisson_noise(measurement_incoherent/np.mean(np.sum(measurement_incoherent.array,(-2,-1))), dose=el,pixel_area=pixel_area)/el

# In[ ]:


measurement_noisy[7].show(power=0.2)


# In[ ]:


max_dose, dose_map  = get_radiation_dose(incoh_probe,positions,el)
dose_map.show(ax=plt.gca());print(max_dose)


# In[ ]:


probe_guess=probe.copy()
#probe_guess.ctf.C30=0
probe_guess.ctf.defocus=probe.ctf.defocus*1.3
#probe_guess.ctf.C12=7000
probe_guess.show()


# In[ ]:


reconstructions = invms(measurement_noisy,probe_guess,positions,fac=0.2,alpha=0.3,beta=1,maxiter=64*4,slices=1,return_iterations=True,device="gpu")


# In[ ]:


#%matplotlib inline
plot_every = 32

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))
for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][0].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions[1][j][0].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()


# In[ ]:


plt.imshow((np.angle(reconstructions[0][-1][0].array)).T, origin='lower', cmap='gray')


# In[ ]:


plt.imshow(np.abs(reconstructions[1][-1][0].array).T ** 2, origin='lower', cmap='gray')


# In[ ]:


probe.show()


# In[ ]:


plt.figure()
plt.imshow(np.abs(np.fft.fftshift(np.fft.fft2(reconstructions[1][-1][0].array))).T**2, origin='lower', cmap='gray')


# In[ ]:


plt.figure()
plt.imshow(np.angle(np.fft.fftshift(np.fft.fft2(reconstructions[1][-1][0].array))).T, origin='lower', cmap='gray')
#plt.xlim(400,600)
#plt.ylim(400,600)


# In[ ]:


thr=probe.build().array.get(); 


# In[ ]:


plt.figure()
plt.imshow(np.abs(thr)**2)
#plt.xlim(400,600)
#plt.ylim(400,600)


# In[ ]:


plt.figure()
plt.imshow(np.angle(np.fft.fftshift(np.fft.fft2( thr ))).T, origin='lower', cmap='gray')
#plt.xlim(400,600)
#plt.ylim(400,600)


# In[ ]:


#%matplotlib widget
plt.imshow(np.angle(reconstructions[1][-1][0].array).T, origin='lower', cmap='gray')


# In[ ]:


2*6.5/probe.angular_sampling[0]


# In[ ]:


Q0=50e-12*1e-3
Q0/c.e


# In[ ]:


Q0=50*200**2*np.pi/9


# # Sweep

# In[ ]:


from pathlib import Path
import sys
import os
def semiangle_gpts_2_extent(semiangle,gpts):
    #from given semiangle and gpts what is extent of calculation box in direct space
    probe_dummy=Probe(energy=30e3)
    extent=1/(2*semiangle*1e-3/gpts*1/probe_dummy.wavelength)
    return(extent)

def mute(func):
    def wrapper(*args, **kwargs):
        import io
        from contextlib import redirect_stdout
        captured_output = io.StringIO()

        with redirect_stdout(captured_output): #chatGPT construct...
            result = func(*args, **kwargs)
        return(result)
    
    return(wrapper)
muted_invms = mute(invms)

def run(semiangle,defocus=None,path=None,modes=1,I=100e-12,t=0.2e-3,temporal_sigma_flag=0,spatial_sigma_flag=0,C30_fac=1,defocus_fac=1,samples=None):
    #modes is usless without the change of beta and maxiter params to invms... probe modes will probably not converge.
    
    #ploting
    fig, axes = plt.subplots( 1,5, figsize=(40,8))
    #plt.tight_layout()
    
    
    #potential params
    probe_dummy=Probe(energy=30e3)
    extent=semiangle_gpts_2_extent(semiangle,256)
    par=(6.5,256) # params 6.5 mrad and 256 gpts at which defocus was set to be as is (so that probe is aprox 1/3 of calculation extent):
    if defocus == None:
        defocus =  40000*6.5/semiangle*extent/semiangle_gpts_2_extent(*par) 
    nyq_sampling = 1/(2*semiangle*1e-3*1/probe_dummy.wavelength)
    sampling = nyq_sampling/4 

    gpts_x=(extent/sampling).astype(int)


    mean_potential = mean_potential_sio2_a
    slice_thickness = 4
    num_slices = 51
    gpts=np.array((gpts_x,)*2)

    #beam params
    energy = 30e3
    semiangle_cutoff = semiangle
    aberrations = {
        'defocus': defocus,
        'C30': 0.88e-3/1e-10,
        'C50': 0.81e-3/1e-10}


    #filling potential with blobs
    array=np.zeros((num_slices,)+tuple(gpts),dtype=np.float64)
    potential = PotentialArray(array=array,slice_thicknesses=np.array([slice_thickness]*num_slices),extent=gpts*sampling,sampling=sampling)
    np.random.seed(seed=25)
    for i in range(int(80*extent/semiangle_gpts_2_extent(*par))):
        get_piled_blobs(potential,A0_max=100)

    interaction_parameter = energy2sigma(energy)                                                                                                                                               
    proj_pot_val_eva = mean_potential*slice_thickness#phase_shift/interaction_parameter/potential.num_slices
    potential.array[:] *= proj_pot_val_eva
    print("max phase shift with no overlap",proj_pot_val_eva*potential.num_slices*interaction_parameter) # phase shift
    print("max phase shift with in project potential",np.max(potential.project().array)*interaction_parameter) # phase shift


    #
    #potential.project().show(ax=axes[0])
    extent_imshow=(0,extent,0,extent)
    im=axes[0].imshow(interaction_parameter*potential.project().array.T,origin="lower",cmap="gray",extent=extent_imshow);fig.colorbar(im,ax=axes[0])
    axes[0].set_title("thickness: "+str(int(potential.num_slices*slice_thickness))+" Å")
    

    #
    probe = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff,**aberrations, device='gpu')
    probe.grid.match(potential)
    #probe.show()


    #another beam params - partial coherence
    energy_spread = 1.6
    Cc = 1.34e-3/1e-10
    focal_spread = energy_spread/probe.energy * Cc # 1/e width of focal distribution
    temporal_sigma_ = focal_spread/np.sqrt(2) # std of focal distribution (normal distribution is proportional to e**( 1/2*(x/std)**2) )
    #I=100e-12
    B_r=1e8

    spatial_sigma_ = get_gaussian_spread(probe.ctf.semiangle_cutoff,probe.energy,B_r,I)

    
    #
    coh_name=""
    if temporal_sigma_flag != 0:
        temporal_sigma=temporal_sigma_
    else:
        temporal_sigma = 0
        coh_name="_temporalcoh"
    if spatial_sigma_flag != 0:
        spatial_sigma=spatial_sigma_
    else:
        spatial_sigma = 0
        coh_name="_spatialcoh"
    if temporal_sigma_flag == 0 and spatial_sigma_flag == 0:
        coh_name = "_coh"
    
    #probe width
    incoh_probe=incoherent_probe(probe,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma)
    r95, _, _, _= get_probe_radius(probe_measurement=incoh_probe,current_ratio=0.95)
    d95=2*r95
    incoh_probe.show(ax=axes[1])
    axes[1].set_title("d95: "+str(int(d95))+" Å"+"   defocus: "+str(int(defocus/1e4))+r" $\mu$m"+r"    $\alpha$: "+str(semiangle)+" mrad")
    print("d95: ",d95)


    #scan
    detector=PixelatedDetector(None)
    scan_sampling= d95*0.3/2#120*extent/semiangle_gpts_2_extent(*par)/2# sampling should be at least 0.7 of probe width
    scan_points = 10 # number of scan points in one direction
    offset=np.array([0,0])
    start=np.array(potential.extent)/2-scan_sampling*scan_points//2+offset
    end=np.array(potential.extent)/2+scan_sampling*scan_points//2+offset
    scan_=GridScan(start,end,sampling=scan_sampling)
    positions=scan_.get_positions()+np.random.uniform(-5,5)
    scan = PositionScan(positions)
    
    #caching
    guess_str = ""
    if C30_fac != 1 or defocus_fac !=1: # if initial guess is different from original parameters
        guess_str="C30fac{}_defocusfac{}".format(C30_fac,defocus_fac)
        print("guess_str: ", guess_str)
    name="semi{0}_defocus{1:.0f}_I{2:.0f}pA_t{3:.0f}us".format(semiangle_cutoff,defocus,I/1e-12,t/1e-6)
    name=name+guess_str+coh_name
    name_cache="semi{0}_defocus{1:.0f}_I{2:.0f}pA".format(semiangle_cutoff,defocus,I/1e-12)
    name_cache=name_cache+coh_name
    
    if path == None:
        path = "report/figs/"
        
    
    hdf5_file_name = Path.home().as_posix()+"/cache/02_random_blobs/"+Path(path).parent.as_posix()+"/" + name_cache + ".hdf5"
    print("hdf5_filename: ", hdf5_file_name)
    if not Path(hdf5_file_name).exists():
        Path(hdf5_file_name).parent.mkdir(parents=True, exist_ok=True)
        measurement_incoherent = incoherent_scan_mc(probe,positions=positions,detector=detector,potential=potential,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma,samples=samples)
        measurement_incoherent.write(hdf5_file_name)
    else:
        measurement_incoherent = Measurement.read(hdf5_file_name)


    #noise
    el=int(I*t/c.e) #el=6280000/9/10/10*20 = 100e-12*0.2e-3/c.e
    pixel_area=1
    measurement_noisy = poisson_noise(measurement_incoherent/np.mean(np.sum(measurement_incoherent.array,(-2,-1))), dose=el,pixel_area=pixel_area)/el

    #
    measurement_noisy[7].show(power=0.2,ax=axes[3])
    axes[3].set_title("# el: "+str(int(el//1e3))+"k")

    #dose
    max_dose, dose_map  = get_radiation_dose(incoh_probe,positions,el)
    dose_map.show(ax=axes[2]);print("max_dose: ",max_dose)
    axes[2].set_title("maximal dose: "+str(int(max_dose))+"el/Å$^2$")


    #reconstruction 
    probe_guess=probe.copy()
    probe_guess.ctf.C30=probe.ctf.C30*C30_fac
    probe_guess.ctf.defocus=probe.ctf.defocus*defocus_fac
    #probe_guess.ctf.C12=7000
    #probe_guess.show()

    reconstructions = muted_invms(measurement_noisy,probe_guess,positions,fac=1,alpha=0.3,beta=0.3,maxiter=64,slices=1,modes=modes,return_iterations=True,device="gpu")
    


    #result
    axes[4].imshow((np.angle(reconstructions[0][-1][0].array)).T, origin='lower', cmap='gray',extent=extent_imshow);fig.colorbar(im,ax=axes[4])
    axes[4].set_xlabel('x [Å]')
    axes[4].set_ylabel('y [Å]') 
    
    plt.savefig(path+name+".pdf")





# In[ ]:


semiangles=np.array([3,6,12])
t0=0.2e-3


# In[ ]:


for semiangle in semiangles:
    run(semiangle,t=t0,temporal_sigma_flag=1,spatial_sigma_flag=1)


# In[ ]:


for semiangle in semiangles:
    run(semiangle,t=t0/2,temporal_sigma_flag=1,spatial_sigma_flag=1)


# In[ ]:


for semiangle in semiangles:
    run(semiangle,t=t0/4,temporal_sigma_flag=1,spatial_sigma_flag=1)


# In[ ]:


for semiangle in semiangles:
    run(semiangle,t=t0/8,temporal_sigma_flag=1,spatial_sigma_flag=1)


# In[ ]:


for semiangle in semiangles:
    run(semiangle,t=t0/16,temporal_sigma_flag=1,spatial_sigma_flag=1)


# In[ ]:


for semiangle in semiangles:
    run(semiangle,t=t0*2,temporal_sigma_flag=1,spatial_sigma_flag=1)


# In[ ]:


for t_fraction in [1,2,4,8,16]:
    for semiangle in semiangles:
        run(semiangle, I=200e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1)


# In[ ]:


for t_fraction in [0.5,1,2,4,8,16]:
    for semiangle in semiangles:
        run(semiangle, I=1000e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1)


# In[ ]:


for t_fraction in [0.5,1,2,4,8,16]:
    for semiangle in semiangles:
        run(semiangle, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=0.9)


# In[ ]:


for t_fraction in [0.5,1,2,4,8,16]:
    for semiangle in semiangles:
        run(semiangle, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=0.8)


# In[ ]:


for t_fraction in [0.5,1,2,4,8,16]:
    for semiangle in semiangles:
        run(semiangle, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=0.7)


# In[ ]:


for t_fraction in [0.5,1,2,4,8,16]:
    for semiangle in semiangles:
        run(semiangle, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=0.6)


# In[ ]:


for t_fraction in [0.5,1,2,4,8,16]:
    for semiangle in semiangles:
        run(semiangle, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=0.5)


# In[ ]:


for defocus_fac in [0.5,0.6,0.7,0.8,0.9]:
    for t_fraction in [0.5,1,2,4,8,16]:
        for semiangle in semiangles:
            run(semiangle, I=1000e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


for defocus_fac in [1,0.9,0.8,0.7,0.5]:
    for t_fraction in [0.25,0.5,1,2,4,8,16]:
#    for t_fraction in [4,8,16]:
            run(24, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


for defocus_fac in [1,0.9,0.8,0.7,0.5]:
    for t_fraction in [0.25,0.5,1,2,4,8,16]:
#    for t_fraction in [4,8,16]:
            run(24, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=0,spatial_sigma_flag=0,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


for defocus_fac in [1,0.9,0.8,0.7,0.5]:
    for t_fraction in [1,2,4,8,16,32,64]:
#    for t_fraction in [4,8,16]:
            run(24, I=1000e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


for defocus_fac in [1,0.9,0.8,0.7,0.5]:
    for t_fraction in [0.25,0.5,1,2,4,8,16]:
            run(18, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


for defocus_fac in [1,0.9,0.8,0.7,0.5]:
    for t_fraction in [0.25,0.5,1,2,4,8,16]:
            run(18, I=100e-12,t=t0/t_fraction,temporal_sigma_flag=0,spatial_sigma_flag=0,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


for defocus_fac in [1,0.9,0.8,0.7,0.5]:
    for t_fraction in [0.25,0.5,1,2,4,8,16]:
            run(24, I=100e-12,path="report/higher_defocus/",defocus=6000,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


for defocus_fac in [1,0.9,0.8,0.7,0.5]:
    for t_fraction in [0.25,0.5,1,2,4,8,16]:
            run(24, I=1000e-12,path="report/higher_defocus/",defocus=6000,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=1,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


for defocus_fac in [1,0.9,0.8,0.7,0.5]:
    for t_fraction in [0.25,0.5,1,2,4,8,16]:
            run(24, I=1000e-12,path="report/higher_defocus/",defocus=6000,t=t0/t_fraction,temporal_sigma_flag=0,spatial_sigma_flag=0,C30_fac=0,defocus_fac=defocus_fac)


# In[ ]:


#for defocus_fac in [1,0.9,0.8,0.7,0.5]:
for defocus_fac in [1]:
    for t_fraction in [0.25,0.5,1,2,4,8,16]:
            run(24, I=1000e-12,path="report/higher_defocus/",defocus=6000,samples=50,t=t0/t_fraction,temporal_sigma_flag=1,spatial_sigma_flag=0,C30_fac=1,defocus_fac=defocus_fac)

