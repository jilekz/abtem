## NEjstarsi testy:
1) temporal decoherence
single mode case; s 1x temporal_sigma: hmm ptylab dokonverguje i ze spatneho pocatecniho odhadu! asi kvuli momentum enhanced PIE -> mPIE; moje invms je jenom rPIE a to uplne neuspeje pri spatnem odhadu pocatecni stopy. pro 5x je moje rekonstrukce se spravnym pocatecnim odhadem neostra,
multi mode case s 1x moje uspeje ale jejich pro 1x dela stejne stopy pro vsechny mody a pro 5x neuspeje vubec. 
TLDR: single mode je ptylab mPIE robustnejsi na pocatecni odhad; multiple mode temporal nekoherenci resi lepe moje invms, jejich je potreba snizit konvergencni parametry aby zacaly mody vetsi jak prvni konvergovat. 

5x temporal decoherence a velika radiacni davka (3120754537.2303815)
MOJE:
moje multimode rPIE f=0.2 alpha=beta=0.9 uspeje ale moje ePIE f=1 je rozmazane. 
JEJICH:
jejich multimode mPIE s alpha=beta=0.9 uspeje i s mabmilion modama (arbitrary: orthogonalisation frequency = 4)

2) spatial decoherence
ePIE s velkou radiacni davkou vytvari neostre vysledky, kdezto se snizujici radiacni davkou se rekonstrukce nahle (podivne) zlepsi a s jeste mensi radiacni davkou se zacnou jeste vice zhorsovat. u mPIE toto neni! 

## mPIE (PtyLab implementace) 
 - Vypada to, ze se mPIE vyhne nejakemu lokalnimu minimu, ktere vznikne pri vysoke! radiacni davce s nekoherentnim simulovanim. Tomuto lokalnimu minimu se nevyhne epie (ani PtyLab ani moje implementace) 
 - Prislo mi, ze mPIE (PtyLab implementace) se lepe chovalo pri spatnem pocatecnim odhadu nezli ePIE (moje implementace)
 - Z hlediska sumu jsem nic moc rozdil nevidel.. mPIE i ePIE zacnou mit problem rekonstruovat vice modu pri nizke radiacni davce, ale to neni nijak prekvapujici. 

## Po druhem pohledu na mPIE
 - Myslel jsem si ze mPIE pomohlo vyresit takovy ten problem, ze s vetsi radiacni davkou bylo k rekonstrukci zapotrebi vice modu, kdezto kdyz se radiacni davka trochu snizila, tak pak stacil jenom jeden mod. Bohuzel kdyz jsem to chtel zopakovat, tak jsem spis videl ze se ePIE a mPIE chovaji zhlediska teto radiacni davky stejne. :( -> tedy ze je potreba vice modu. (pro 5x temporal) -> Deus Ex: to zlepseni je asi jenom pro Spatial decoherence!
 - Pozn.: pri inicializaci modu stop v PtyLab je potreba mody odlisit treba fazi.. jinak se nekde objevi NaN. 
 - Pozn.: mPIE v PtyLab je i rPIE.. viz parametry betaProbe betaObject alphaProbe alphaObject.

