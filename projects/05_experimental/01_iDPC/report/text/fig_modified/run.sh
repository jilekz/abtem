#scp -r jilek@lenc.isibrno.cz:/home/jilek/abtem/projects/05_experimental/01_iDPC/report/figs /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/
#scp -r jilek@lenc.isibrno.cz:/home/jilek/abtem/projects/05_experimental/01_iDPC/report/02_figs_sampling_restriction_on_defocus /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/
#scp -r jilek@lenc.isibrno.cz:/home/jilek/abtem/projects/05_experimental/01_iDPC/report/atoms.pdf /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/
pdfcrop --clip ../../figs/det_30mrad/01_coh_50pA/5e-14/semi24.0_defocus3729_I50pA_t1000us-object.pdf det_30mrad/semi24.0_defocus3729_I50pA_t1000us-object-coh.pdf
pdfcrop --clip ../../figs/det_30mrad/04_partial_both_50pA/5e-14/semi24.0_defocus3729_I50pA_t1000us-object.pdf det_30mrad/semi24.0_defocus3729_I50pA_t1000us-object-partial50.pdf
pdfcrop --clip ../../figs/det_30mrad/07_partial_both_400pA/5e-14/semi24.0_defocus3729_I400pA_t125us-object.pdf det_30mrad/semi24.0_defocus3729_I400pA_t125us-object-partial400.pdf

pdfcrop --clip ../../figs/det_60mrad/01_coh_50pA/5e-14/semi18.0_defocus2042_I50pA_t1000us-object.pdf det_60mrad/semi18.0_defocus2042_I50pA_t1000us-object-coh.pdf 
pdfcrop --clip ../../figs/det_60mrad/04_partial_both_50pA/5e-14/semi18.0_defocus2042_I50pA_t1000us-object.pdf det_60mrad/semi18.0_defocus2042_I50pA_t1000us-object-partial50.pdf  
pdfcrop --clip ../../figs/det_60mrad/07_partial_both_400pA/5e-14/semi18.0_defocus2042_I400pA_t125us-object.pdf det_60mrad/semi18.0_defocus2042_I400pA_t125us-object-partial400.pdf

pdfcrop --clip ../../figs/det_90mrad/01_coh_50pA/5e-14/semi18.0_defocus1971_I50pA_t1000us-object.pdf            det_90mrad/semi18.0_defocus1971_I50pA_t1000us-object-coh.pdf
pdfcrop --clip ../../figs/det_90mrad/04_partial_both_50pA/5e-14/semi18.0_defocus1971_I50pA_t1000us-object.pdf  det_90mrad/semi18.0_defocus1971_I50pA_t1000us-object-partial50.pdf
pdfcrop --clip ../../figs/det_90mrad/07_partial_both_400pA/5e-14/semi18.0_defocus1971_I400pA_t125us-object.pdf det_90mrad/semi18.0_defocus1971_I400pA_t125us-object-partial400.pdf

#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/03_partial_spatial/100ms/semi12.5_defocus2000-object-modes4.pdf 03_partial_spatial/100ms/semi12.5_defocus2000-object-modes4.pdf
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/03_partial_spatial/100ms/semi15.5_defocus1613-object-modes4.pdf 03_partial_spatial/100ms/semi15.5_defocus1613-object-modes4.pdf
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/03_partial_spatial/100ms/semi3.5_defocus7143-object-modes4.pdf 03_partial_spatial/100ms/semi3.5_defocus7143-object-modes4.pdf
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/03_partial_spatial/100ms/semi6.5_defocus3846-object-modes4.pdf 03_partial_spatial/100ms/semi6.5_defocus3846-object-modes4.pdf
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/03_partial_spatial/100ms/semi9.5_defocus2632-object-modes4.pdf 03_partial_spatial/100ms/semi9.5_defocus2632-object-modes4.pdf
#
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/02_partial_temporal/100ms/semi12.5_defocus2000-object-modes4.pdf 02_partial_temporal/100ms/semi12.5_defocus2000-object-modes4.pdf 
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/02_partial_temporal/100ms/semi15.5_defocus1613-object-modes4.pdf 02_partial_temporal/100ms/semi15.5_defocus1613-object-modes4.pdf 
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/02_partial_temporal/100ms/semi3.5_defocus7143-object-modes4.pdf 02_partial_temporal/100ms/semi3.5_defocus7143-object-modes4.pdf
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/02_partial_temporal/100ms/semi6.5_defocus3846-object-modes4.pdf 02_partial_temporal/100ms/semi6.5_defocus3846-object-modes4.pdf
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/02_partial_temporal/100ms/semi9.5_defocus2632-object-modes4.pdf 02_partial_temporal/100ms/semi9.5_defocus2632-object-modes4.pdf
#
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/100ms/semi12.5_defocus2000-object-modes4.pdf 04_partial_both/100ms/semi12.5_defocus2000-object-modes4.pdf 
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/100ms/semi15.5_defocus1613-object-modes4.pdf 04_partial_both/100ms/semi15.5_defocus1613-object-modes4.pdf 
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/100ms/semi3.5_defocus7143-object-modes4.pdf 04_partial_both/100ms/semi3.5_defocus7143-object-modes4.pdf
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/100ms/semi6.5_defocus3846-object-modes4.pdf  04_partial_both/100ms/semi6.5_defocus3846-object-modes4.pdf  
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/100ms/semi9.5_defocus2632-object-modes4.pdf  04_partial_both/100ms/semi9.5_defocus2632-object-modes4.pdf  
#
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi12.5_defocus2000-object-modes4.pdf 04_partial_both/1ms/semi12.5_defocus2000-object-modes4.pdf 
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi15.5_defocus1613-object-modes4.pdf 04_partial_both/1ms/semi15.5_defocus1613-object-modes4.pdf 
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi3.5_defocus7143-object-modes4.pdf 04_partial_both/1ms/semi3.5_defocus7143-object-modes4.pdf
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi6.5_defocus3846-object-modes4.pdf  04_partial_both/1ms/semi6.5_defocus3846-object-modes4.pdf  
#pdfcrop --margins '-1025 0 0 -1140' --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi9.5_defocus2632-object-modes4.pdf  04_partial_both/1ms/semi9.5_defocus2632-object-modes4.pdf  
#
#
##INCOHERENT PROBES
#pdfcrop  --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi12.5_defocus2000-probe_incoh.pdf 04_partial_both/1ms/semi12.5_defocus2000-probe_incoh.pdf 
#pdfcrop  --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi15.5_defocus1613-probe_incoh.pdf 04_partial_both/1ms/semi15.5_defocus1613-probe_incoh.pdf 
#pdfcrop  --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi3.5_defocus7143-probe_incoh.pdf 04_partial_both/1ms/semi3.5_defocus7143-probe_incoh.pdf
#pdfcrop  --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi6.5_defocus3846-probe_incoh.pdf  04_partial_both/1ms/semi6.5_defocus3846-probe_incoh.pdf  
#pdfcrop  --clip /home/zvonek/el/abtem/projects/05_experimental/01_iDPC/report/figs/04_partial_both/1ms/semi9.5_defocus2632-probe_incoh.pdf  04_partial_both/1ms/semi9.5_defocus2632-probe_incoh.pdf  
