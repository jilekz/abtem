#!/usr/bin/env python
# coding: utf-8

# In[1]:


#abtem version: 7a663e35aea0aa787257840f232dcaee4a734ae7
import os
#os.environ["MKL_NUM_THREADS"] = "16"
#os.environ["NUMEXPR_NUM_THREADS"] = "16"c
#os.environ["OMP_NUM_THREADS"] = "16"


# In[2]:


import numpy as np
import matplotlib.pyplot as plt
from abtem import *
from abtem.utils import GaussianDistribution
from ase.io import read
from ase.build import surface, bulk
from tqdm.auto import tqdm
from ase.build import graphene

from tqdm.auto import tqdm
from abtem.structures import orthogonalize_cell
from abtem.measure import center_of_mass


# In[3]:


from ase.build import nanotube


# In[4]:


from abtem.detect import AnnularDetector
from abtem.noise import poisson_noise
import scipy.constants as c
from scipy.optimize import minimize
from collections import defaultdict
from abtem.scan import PositionScan
from abtem.custom import get_gaussian_spread
from abtem.reconstruct import invms

from abtem.noise import poisson_noise
import scipy.constants as c
from abtem.measure import bandlimit
from abtem.custom import get_radiation_dose

from abtem.custom import incoherent_scan
from abtem.waves import _multislice
import cupy as cp
from abtem.custom import incoherent_scan_mc
from abtem.custom import incoherent_probe
from abtem.custom import sample_probe
from abtem.custom import incoherent_scan_mc_fp


# In[5]:


import py4DSTEM


# In[6]:


import matplotlib
matplotlib.rcParams["figure.figsize"] = (10, 10)
matplotlib.rcParams['lines.linewidth'] = 2
font = {'weight' : 'bold',
                'size'   : 15}
matplotlib.rc('font', **font)


# # Partial coherence in 4D-STEM

# In[7]:


atoms = graphene()
atoms = orthogonalize_cell(atoms)
atoms *= (24,16,1)

atoms.center(vacuum=2, axis=2)
atoms.center()

fig,(ax1,ax2)=plt.subplots(1, 2, figsize=(10,5))
show_atoms(atoms, ax=ax1)
show_atoms(atoms, ax=ax2, plane='xz');


# In[8]:


atoms_rotated=atoms.copy()
atoms_rotated.rotate(10,"z")
atoms_rotated.center()
atoms_rotated.positions+=np.array([0,0,2])
fig,(ax1,ax2)=plt.subplots(1, 2, figsize=(10,5))
show_atoms(atoms_rotated, ax=ax1)
show_atoms(atoms_rotated, ax=ax2, plane='xz');


# In[9]:


atoms_moire=atoms+atoms_rotated
fig,(ax1,ax2)=plt.subplots(1, 2, figsize=(10,5))
atoms_moire.center(vacuum=2)

show_atoms(atoms_moire, ax=ax1)
show_atoms(atoms_moire, ax=ax2, plane='xz');


# In[10]:


cnt = nanotube(5, 4, length=1)
cnt.rotate(90,"y",center='COM')
cnt.center(vacuum=1,about=0)
#show_atoms(cnt,plane="xz")
cnt.translate(-cnt.get_center_of_mass()+atoms_moire.get_center_of_mass()+np.array([0,0,3.9]))

cnt2=cnt.copy()
cnt2.rotate(30,"z",center='COM')

cnt3 = nanotube(2, 4, length=1)
cnt3.translate(-cnt3.get_center_of_mass()+atoms_moire.get_center_of_mass()+np.array([0,5,3.9]))
cnt3.rotate(90,"x",center='COM')
#cnt3.rotate(60,"z",center='COM')
show_atoms(cnt3,plane="xy")


# In[11]:


atoms_tot=atoms_moire+cnt+cnt2+cnt3
atoms_tot.center(vacuum=2)
show_atoms(atoms_tot)


# In[12]:


energy = 30e3

energy_spread = 0.6
Cc = 1.34e-3/1e-10
focal_spread = energy_spread/energy * Cc # 1/e width of focal distribution
temporal_sigma = focal_spread/np.sqrt(2) # std of focal distribution (normal distribution is proportional to e**( 1/2*(x/std)**2) )
B_r=1e8



semiangle_cutoff = 12.5
aberrations = {
    'defocus': 400,
    'C30': 0.88e-3/1e-10,
    'C50': 0.81e-3/1e-10}


# In[13]:


#probe = SMatrix(energy=energy,interpolation=1,semiangle_cutoff=semiangle_cutoff, expansion_cutoff=semiangle_cutoff*2+0.5,**aberrations, device='gpu')
probe = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff,**aberrations, device='gpu')
potential = Potential(FrozenPhonons(atoms_tot, sigmas=.06, num_configs=1), #atoms,
                      sampling=.1,
                      slice_thickness=0.25,
                      projection='infinite',
                      parametrization='kirkland')
probe.grid.match(potential)

#probe.build().collapse().show();
probe.show()


# In[14]:


probe.ctf.nyquist_sampling*0.9


# In[15]:


scan = GridScan((potential.extent[0]*2.5/10,potential.extent[0]*2.5/10), (potential.extent[1]*7.5/10, potential.extent[1]*7.5/10), sampling = 1.2)
detector = PixelatedDetector()
detector_segmented = SegmentedDetector(0,semiangle_cutoff*1.5,1,4)


# In[16]:


print(potential.extent)
print(potential.gpts)


# In[17]:


10/33.8


# In[18]:


potential.build().project().show()


# In[19]:


f=2
(potential.extent[0]*(5-f)/10,potential.extent[0]*(5-f)/10), (potential.extent[1]*(5+f)/10, potential.extent[1]*(5+f)/10)


# In[20]:


#S=probe.multislice(potential,pbar=True)
#S=S.downsample()


# In[21]:


#measurement,measurement_segmented = probe.scan(scan, [detector,detector_segmented], potential, pbar=True)
#measurement_segmented = probe.scan(scan, detector_segmented, potential, pbar=True)
from abtem.detect import FlexibleAnnularDetector
measurement = probe.scan(scan, detector, potential, pbar=True,max_batch=400)
#measurement,measurement_segmented = S.scan(scan, [detector], pbar=True)

#from abtem.device import copy_to_device
#from abtem.waves import SMatrixArray
#import cupy as cp
#try:
#    del S_gpu
#except:
#    print("nope")
##S_gpu=S.transfer("gpu")
#S_gpu = SMatrixArray(array=cp.array(copy_to_device(S.array, "gpu")),                                                        
#                              k=S.k.copy(),                                                                                 
#                              ctf=S.ctf.copy(),                                                                             
#                              extent=S.extent,                                                                              
#                              offset=S.offset,                                                                              
#                              interpolated_gpts=S.interpolated_gpts,                                                        
#                              energy=S.energy,                                                                              
#                              antialias_aperture=S.antialias_aperture,                                                      
#                              device="gpu")  
#
#measurement,measurement_segmented = S_gpu.scan(scan, detector, pbar=True)


# In[22]:


get_ipython().run_cell_magic('time', '', '\nI_reg=1e-9 \nel=I_reg*1e-7/c.e # charge: 1nA times 1us \npixel_area=1 #effectively disabled \nmeasurement_noisy = poisson_noise(measurement/np.mean(np.sum(measurement.array,(-2,-1))), dose=el,pixel_area=pixel_area)/el\n')


# In[23]:


#annular_detector=AnnularDetector(inner=20,outer=150)
annular_detector=AnnularDetector(inner=12.5,outer=150)
ress=annular_detector.integrate(measurement_noisy)


# In[24]:


el


# In[25]:


50e-12*1e-3/c.e


# In[26]:


#%matplotlib widget
measurement_noisy[0,0].show(power=0.2)


# In[27]:


ress.show()


# In[28]:


plt.figure()
plt.imshow(-ress.array.T,origin="lower",cmap="gray")


# In[29]:


com = center_of_mass(measurement_noisy, return_magnitude=False)



cmap = 'gray'
com_mag=com[0].copy()
com_mag.array=np.sqrt(com[0].array**2+com[1].array**2)
com_mag.show(cmap=cmap, vmin=0, vmax=.7)


# In[30]:


plt.figure()
plt.imshow(com[0].array.T,origin="lower")
plt.figure()
plt.imshow(-com[1].array.T,origin="lower")


# In[31]:


get_ipython().run_line_magic('matplotlib', 'widget')


# In[32]:


dataset = py4DSTEM.DataCube(
    data=measurement_noisy.array
)
dataset


# In[33]:


dataset.get_dp_mean();
py4DSTEM.show(
    dataset.tree('dp_mean'),
    ticks=False
)


# In[34]:


probe_semiangle, probe_qx0, probe_qy0 = dataset.get_probe_size()

print('Estimated probe radius =', '%.2f' % probe_semiangle, 'pixels')


# In[35]:


dpc = py4DSTEM.process.phase.DPCReconstruction(
    datacube=dataset,
    energy=80e3,
    verbose=True,
).preprocess()


# In[36]:


py4DSTEM.show(
    np.linalg.norm([dpc.com_x,dpc.com_y],axis=0),
    cmap='inferno',
    vmin=0.7,
    vmax=0.999,
    ticks=False,
    scalebar=True,
)


# In[37]:


dpc.reconstruct(
    max_iter=32,
    reset=True,
    gaussian_filter_sigma = 0.375, # in pixels since we haven't calibrated
).visualize(
    figsize=(6,7),
    cbar=True,
);


# In[38]:


semiangle_cutoff


# # Ptychography - choosing parameters

# In[41]:


from abtem.measure import Calibration
def crop(measurement, extent=None, margin=None):                                                                     
                                                                                                                           
    old_extent = (measurement.calibration_limits[-2][1] - measurement.calibration_limits[-2][0],                                           
                  measurement.calibration_limits[-1][1] - measurement.calibration_limits[-1][0]) 
    
    
    print(old_extent)    
    
    new_shape = (int(np.floor(extent[0] / old_extent[0] * measurement.shape[-2])),
                 int(np.floor(extent[1] / old_extent[1] * measurement.shape[-1])))
    
    print(new_shape)
    
    offset = (int(np.floor(measurement.shape[-2]/2))-new_shape[0]//2,
              int(np.floor(measurement.shape[-1]/2))-new_shape[1]//2)
    
    print(offset)
    
    array = measurement.array[..., offset[0]:(new_shape[0]+offset[0]), offset[1]:(new_shape[1]+offset[1])]                                                
    cal2=Calibration(offset=measurement.calibrations[-2].extent(offset[0])[1],sampling=measurement.calibrations[-2].sampling,units="mrad",name="alpha_x")
    cal3=Calibration(offset=measurement.calibrations[-1].extent(offset[1])[1],sampling=measurement.calibrations[-1].sampling,units="mrad",name="alpha_y")
    print(np.shape(array))
    if len(np.shape(array))==4:
        calibrations=tuple(measurement.calibrations[:2])+(cal2,)+(cal3,)
    elif len(np.shape(array))==3:
        calibrations=(None,)+(cal2,)+(cal3,)
    else:
        raise ValueError("wrong shape"+str(len(np.shape(array))))
    print(np.shape(calibrations))
    return measurement.__class__(array, calibrations=calibrations)

def plot_probe_profile(probe,defocus_low,defocus_up,num,**kwarg):
    gpts=probe.gpts[1]
    sampling=probe.sampling
    ctf=probe.ctf
    semiangle_cutoff=probe.ctf.semiangle_cutoff
    
    res=np.zeros((num,gpts))
    
    gpts_f=int(semiangle_cutoff*2/probe.angular_sampling[0])*2
    res_f=np.zeros((num,gpts_f))
    for idx,defocus in enumerate(np.linspace(defocus_low,defocus_up,num)):
        ctf.defocus=defocus
        pr=Probe(energy=probe.energy,semiangle_cutoff=semiangle_cutoff,sampling=sampling,ctf=ctf,gpts=gpts)
        pr_proj=np.sum(np.abs(pr.build().array)**2,0)
        res[idx]=pr_proj
        
        res_f[idx]=np.real(pr.ctf.evaluate_aberrations(np.linspace(-semiangle_cutoff*1e-3,semiangle_cutoff*1e-3,gpts_f),0))
    fig, axes = plt.subplots(1,2)
    axes[0].imshow(res,extent=(probe.extent[0],0,defocus_up,defocus_low),aspect=probe.extent[0]/(defocus_up-defocus_low),**kwarg)
    axes[1].imshow(res_f,extent=(semiangle_cutoff,-semiangle_cutoff,defocus_up,defocus_low),aspect=semiangle_cutoff*2/(defocus_up-defocus_low),**kwarg)
    


# In[42]:


#What value of angular sampling should be used?


# In[43]:


def semiangle_gpts_2_extent(semiangle,gpts):
    #from given semiangle (maximal semiangle detected) and gpts what is extent of calculation box in direct space
    probe_dummy=Probe(energy=30e3)
    extent=1/(2*semiangle*1e-3/gpts*1/probe_dummy.wavelength)
    return(extent)

def semiangle_2_sampling_extent(semiangle,gpts,energy=30e3):
    #params:
    # semiangle - semiangle which will be detected on the detector with a given number of pixels "gpts"
    # gpts
    # energy 
    #returns: (params defining direct and reciprocal sampling)
    # sampling - what should be sufficient sampling (4x better than sampling which correspond to waves formed with the highes spatial frequency given by `semiangle`)
    # extent - corresponding extent
    probe_dummy=Probe(energy=energy)
    extent=semiangle_gpts_2_extent(semiangle,gpts)
    nyq_sampling = 1/(2*semiangle*1e-3*1/probe_dummy.wavelength)
    sampling = nyq_sampling/4 # note: the scattering on eg crystal will produce diffraction discs in a dark field (in my case all those discs are in a dark field). To make the point: in my case diffraction discs lie outside of a bright field disk so nyquist frequency which correspond only to bright field edges, cannot describe the higher order scattering! (A note should be also taken with regard to antialiasing aperture which efectively reduces the maximal transmited wavelength to 2/3 of the maximal nyquist frequency which is defined by sampling in direct space)
    gpts_x=(extent/sampling).astype(int) #it is obviously 4 times more than number of pixels in detectors when targeted sampling is nyq_sampling/4 
    return(sampling,extent)

def get_reciprocal_sampling(probe):
    #params:
    # probe - for ploting its ctf, the probe needs to have probe.semiangle_cutoff
    #returns:
    # None?
    probe_semiangle = probe.ctf.semiangle_cutoff
    num=int(probe_semiangle/probe.angular_sampling[0])
    x = np.linspace(-probe_semiangle*1e-3,probe_semiangle*1e-3,num)
    y = np.real(probe.ctf.evaluate_aberrations(x,0))
    #ax0.plot(x*1e3,y)
    #ax0.set_xlabel(r"$\alpha_x$ [mrad]")
    #ax0.set_ylabel("$\mathrm{Re}(e^{-ik\chi})$ [rad]")

    chi=probe.ctf.evaluate_chi(x,0)
    #ax1.plot(x[1:]*1e3,chi[1:]-chi[0:-1])
    #ax1.set_xlabel(r"$\alpha_x$ [mrad]")
    #ax1.set_ylabel(r"$\Delta\chi$ [rad]")
    #ax1.set_xlim(ax0.get_xlim())
    return(x,y,chi)

def plot_reciprocal_sampling(probe,ax0,ax1):
    x,y,chi=get_reciprocal_sampling(probe)
    print("number of ploted pixels: ",len(x))
    ax0.plot(x*1e3,y)
    ax0.set_xlabel(r"$\alpha_x$ [mrad]")
    ax0.set_ylabel("$\mathrm{Re}(e^{-ik\chi})$ [rad]")

    ax1.plot(x[1:]*1e3,chi[1:]-chi[0:-1])
    ax1.set_xlabel(r"$\alpha_x$ [mrad]")
    ax1.set_ylabel(r"$\Delta\chi$ [rad]")
    ax1.set_ylim(-np.pi,np.pi)
    ax1.set_xlim(ax0.get_xlim())
    return


# In[44]:


def func(defocus,probe_in):
    offset=np.pi*1/3 # pi/2 would correspond to probe which total extent would take 50% of calculation box. pi/3 correspond to 33%.. 
    probe_dummy=probe_in.copy()
    probe_dummy.ctf.defocus=defocus
    _,_,chi=get_reciprocal_sampling(probe_dummy)
    delta_chi=chi[1:]-chi[0:-1]
    ret=np.abs(np.max(np.abs(delta_chi))-offset) # finding minimum, but this minimum (of max(abs(delta_chi)) ) is larger or equals to offset value.
    #ret=np.max(np.abs(to_minimalize))
    return(ret)
    
semiangles_detector=[90,60,30]
semiangles_cutoff=np.linspace(3,30,10)
sweep_params = defaultdict(list)
gpts_detector=256
for semiangle_detector in semiangles_detector:
    for semiangle_cutoff in semiangles_cutoff:
        sampling,extent=semiangle_2_sampling_extent(semiangle=semiangle_detector,gpts=gpts_detector,energy=energy)

        probe_dummy=Probe(energy=energy,semiangle_cutoff=semiangle_cutoff,sampling=sampling,extent=extent,defocus=0,C30=aberrations["C30"],C50=aberrations["C50"])
        res=minimize(func,(3000),method="Nelder-Mead",args=probe_dummy)
        if res.success != True:
             raise ValueError("minimalisation err")
        if res.fun+np.pi*1/3 > np.pi/2:
             continue
        if semiangle_detector<semiangle_cutoff:
            continue
        
        defocus=res.x[0]
        probe_min = Probe(energy=energy,semiangle_cutoff=semiangle_cutoff,sampling=sampling,extent=extent,defocus=defocus,C30=aberrations["C30"],C50=aberrations["C50"])
        fig,axes=plt.subplots(3,1,gridspec_kw={'height_ratios': [3, 1,1]})
        probe_min.show(power=1,ax=axes[0])
        axes[0].set_title(r"defocus: {:.0f} nm".format(defocus*1e-1))
        plot_reciprocal_sampling(probe_min,axes[1],axes[2])
                
        sweep_params["semiangle_detector"].append(semiangle_detector)
        sweep_params["semiangle_cutoff"].append(semiangle_cutoff)
        sweep_params["extent"].append(extent)
        sweep_params["defocus"].append(defocus)
        sweep_params["sampling"].append(sampling)


# In[45]:


sweep_params


# In[46]:


plt.figure()
defocus_len=10
#fig,axes=plt.subplots(1,defocus_len)
cmap = plt.get_cmap("jet")
#plt.subplots(0,5)
for i,defocus in enumerate(np.linspace(0,8000,defocus_len)):
    semiangle=30
    gpts_detector=256
    sampling,extent=semiangle_2_sampling_extent(semiangle=semiangle,gpts=gpts_detector,energy=energy)
    probe_pok=Probe(energy=energy,semiangle_cutoff=30,sampling=sampling,extent=extent,defocus=defocus,C30=aberrations["C30"],C50=aberrations["C50"])
    x,y,chi=get_reciprocal_sampling(probe_pok)
    plt.plot(x[1:]*1e3,chi[1:]-chi[0:-1],color=cmap(i/defocus_len),label=r"$\Delta f=${:.0f}$\,$nm".format(defocus*1e-1))
    plt.ylim(-np.pi,np.pi)
    plt.legend()

    plt.xlabel(r"$\alpha_x$ [mrad]")
    plt.ylabel(r"$\Delta\chi$ [rad]")
    #axes[i].plot(x[1:]*1e3,chi[1:]-chi[0:-1])
    #axes[i].set_ylim(-np.pi,np.pi)


# In[47]:


#%matplotlib widget
Probe(energy=energy,semiangle_cutoff=semiangle_cutoff,sampling=sampling,extent=extent,defocus=1780,C30=aberrations["C30"],C50=aberrations["C50"]).show(power=0.8)


# In[48]:


k=(1/probe_dummy.wavelength)
print((1/1)/k)


# In[49]:


#%matplotlib widget
plot_probe_profile(probe_pok,-10000,10000,40)#,norm=matplotlib.colors.LogNorm())


# In[50]:


(1/10)/(1/probe_dummy.wavelength) # CBED radius ub mrad in Ptychographic Electron Microscopy Using High-angle Dark-field Scattering for Sub-nanometre Resolution Imaging by Martin James Humphry,B kraus A.C. Hurst Andrew Maiden


# In[51]:


(1/2.36)/(1/probe_dummy.wavelength) # 


# # Ptychography - getting data

# In[52]:


def get_potential(atoms_in,extent,sampling):
    atoms=atoms_in.copy()
    atoms_extent=atoms.cell.diagonal() #x,y,z
    
    vacuum=(extent-atoms_extent)/2 #x,y,z
    
    atoms.center(vacuum=vacuum[0],axis=(0))
    atoms.center(vacuum=vacuum[1],axis=(1))
    
    potential_p = Potential(FrozenPhonons(atoms, sigmas=.06, num_configs=1), #atoms_tot_p,
                      sampling=sampling,
                      #gpts=round(extent/sampling),
                      slice_thickness=2,
                      projection='infinite',
                      parametrization='kirkland')
    return potential_p


# In[53]:


atoms_tot_p=atoms_tot.copy()
atoms_tot_p*=(1,1,1)
atoms_tot_p.center(vacuum=0,axis=(0,1)) # setting vacuum so that extent is 200 A and that angular sampling will be good enough (phase shift due to aberrations does not alias) with defocus set to -1500 (and aperture cutoff_semiangle 12.5 mrad) also so thaxt sampling will be cca 0.2 A with 2048 gpts 
                                         # (constraining parameters are: I want as large probe as possible; I want fine details which wont be lost in poisson noise - second brag disc can be seen if dose is not too small; I work with 2048 gpts because it does not present problem current gpu.) 
print()

show_atoms(atoms_tot_p)
show_atoms(atoms_tot_p,plane="xz")


# In[54]:


semiangle_cutoff_p=15.5
defocus_p=2000*12.5/semiangle_cutoff_p


# In[55]:


probe_p = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff_p,defocus=defocus_p,C30=aberrations["C30"],C50=aberrations["C50"], device='gpu')
#potential_p = Potential(FrozenPhonons(atoms_tot_p, sigmas=.06, num_configs=1), #atoms_tot_p,
#                      sampling=.1,
#                      gpts=1024,
#                      slice_thickness=0.25,
#                      projection='infinite',
#                      parametrization='kirkland')

potential_p=get_potential(atoms_tot_p,100,100/1024)

probe_p.grid.match(potential_p)

print(potential_p.sampling)

probe_p.build().show();


# In[56]:


#del potential_p,probe_p #pred 1121 po 543 bez del 615
cp._default_memory_pool.free_all_blocks()


# In[57]:


probe_p.build().diffraction_pattern() #615 -> 954 -podruhe> 1025 


# In[58]:


cp._default_memory_pool.free_all_blocks() # 687


# In[59]:


probe_p.gpts


# In[60]:


#np.shape(potential_p.build().array)


# In[61]:


res=sample_probe(probe_p,np.array([0,0]),0,0) # predtim 7887 po 8465


# In[62]:


del res
cp._default_memory_pool.free_all_blocks() #pred 8465 po 7959


# In[63]:


f=1.5
scan_p = GridScan((potential_p.extent[0]*(5-f)/10,potential_p.extent[0]*(5-f)/10), (potential_p.extent[1]*(5+f)/10, potential_p.extent[1]*(5+f)/10), sampling = 3)#1.25) # sampling should be at least 0.7 of probe width
positions = scan_p.get_positions()


# In[64]:


len(positions)


# In[65]:


detector_p = PixelatedDetector(None)

I=50e-12
spatial_sigma = get_gaussian_spread(probe_p.ctf.semiangle_cutoff,probe_p.energy,B_r,I)


# In[66]:


get_ipython().run_cell_magic('time', '', '#1. iter\n#pred 687 po 900, 8249 s barem, 15449 behem multislice, 22649 po\n#2. iter\n#pred 7887, 8105 , 15449 behem, 22649 po\n\n#1. iter\n#pred 687 po 905, 8249 s baram, 15449 behem multislice, 22649 po\nmeasurement_mc=incoherent_scan_mc(probe_p,positions,detector_p,potential_p,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma,max_batch_scan=100)\n')


# In[67]:


measurement_mc[1].show(power=0.2)


# In[68]:


probe_p.show()


# In[69]:


#%matplotlib widget
incoh_probe=incoherent_probe(probe_p,temporal_sigma,spatial_sigma)
incoh_probe.show()


# In[70]:


measurement_p_raw = measurement_mc
#measurement_p_crop = incoherent_scan(probe_p,scan_p,detector_p,potential_p,temporal_sigma=temporal_sigma*0,spatial_sigma=spatial_sigma*0,max_batch=400)


# In[71]:


measurement_p_raw[0].show()


# In[72]:


np.sum(measurement_p_raw[0].array)


# In[73]:


np.shape(measurement_p_raw)


# In[74]:


#measurement_p_crop=crop(measurement_p_raw,extent=(171.5,166)) # setting angular extent to 84 so that cca 256 pixels are in detector. Potential extent should be sufficiently set so that sampling will be not to fine and not to rough so that maximal scattering angle will be as wanted (second brag disk). 
measurement_p_crop=crop(measurement_p_raw,extent=(200,200))
#measurement_p_crop=crop(measurement_p_raw,extent=(170,160.8))


# In[75]:


measurement_p_crop.calibrations[1].sampling


# In[76]:


#%matplotlib inline
measurement_p_crop[15].show(power=0.2)


# # Adding noise

# In[77]:


el=I*10e-3/c.e/100*1e1# charge: 50pA times 10ms (when time is 2ms the dose is insuficcient to resolve the sparse moire hexagon 10ms it starts to be resolvable)
pixel_area=1 #effectively disabled 
print(el)


# In[78]:


measurement_noisy = poisson_noise(measurement_p_crop/np.mean(np.sum(measurement_p_raw.array,(-2,-1))), dose=el,pixel_area=pixel_area)/el


# In[79]:


#measurement_noisy=bandlimit(measurement_noisy,12.5)


# In[80]:


#max_dose 3715 je uz moc malo :( s 5x temporal - zadny novy mod se nezrekonstruuje a vysledek je tedy asi jenom jednomodovy a dosti rozmazly. 10x vetsi dose je uz ok (max_dose=37150; neboli el=I*10e-3/c.e/10, kde I je 50 mA) zrekonstruuje se vice modu a obraz je ostrejsi, musi se ale teda zacit s pocatecnim odhadem, ktery je velmi blizko originalni stope (pr defocus*1.5 uz nefungoval)


# In[81]:


max_dose,dose_map=get_radiation_dose(incoh_probe,positions,el)
print(max_dose) # total number of electrons per pixel area times fraction of total iradiance in pixel 
dose_map.show()


# # Downsampling diffraction pattern

# In[82]:


if 0:
    #Downsample diffraction patter by factor of `p_num`
    p_num=2 # downsampling is turned off
    shape_orig=np.shape(measurement_noisy)
    array=np.zeros([shape_orig[0],shape_orig[1],shape_orig[2]//p_num,shape_orig[3]//p_num])

    for i in range(np.shape(measurement_noisy)[0]):
        for j in range(np.shape(measurement_noisy)[1]):
            m = measurement_noisy.array[i,j]
            if p_num==1:
                array[i,j]=m
            else:
                #array[i,j]=m[0::p_num,0::p_num]+m[0::p_num,1::p_num]+m[1::p_num,0::p_num]+m[1::p_num,1::p_num]            
                for k in range(p_num):
                    for l in range(p_num):
                        array[i,j]+=m[k::p_num,l::p_num]

    #setting calibrations and creating measurement
    cals=tuple(i.copy() for i in measurement_noisy.calibrations) # cannot use equals because it wouldnt copy but it would instead pass refference
    print("sampling in reciprocal space before:")
    print(cals[2].sampling,cals[3].sampling)
    cals[2].sampling*=p_num
    cals[3].sampling*=p_num
    print("sampling in reciprocal space after:")
    print(cals[2].sampling,cals[3].sampling)
    measurement_resampled= Measurement(array, calibrations=cals, name=measurement_noisy.name, units=measurement_noisy.units) 


# In[83]:


#measurement_resampled.show(power=0.2)


# # Reconstruction

# In[84]:


measurement_p=measurement_noisy
#measurement_p=measurement_resampled


# In[85]:


measurement_p[56].show(power=0.2,figsize=(10,10))


# In[86]:


probe_guess = Probe(semiangle_cutoff=semiangle_cutoff_p, energy=energy,defocus=defocus_p, C30=aberrations["C30"],C50=aberrations["C50"])#, C12=0, extent=150, gpts=128)
modes = 3
reconstructions=invms(bandlimit(measurement_p,150),probe_guess,max_angle=None,positions=positions,alpha=0.8,fac=1,beta=0.8,maxiter=64*8,k_modes=1,modes=modes,device="gpu",return_iterations=True)


# In[87]:


#%matplotlib inline
plot_every = 64

#fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))

#for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
#    axes[0,i].imshow(np.angle(reconstructions[0][j][0].array).T, origin='lower', cmap='gray')
#    axes[1,i].imshow(np.abs(reconstructions[1][j][0].array).T ** 2, origin='lower', cmap='gray')
#    for ax in (axes[0,i], axes[1,i]):
#        ax.axis('off')
      
fig, axes = plt.subplots(1+modes, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][-1].array).T, origin='lower', cmap='gray')
    for mode in range(modes): 
        axes[1+mode,i].imshow(np.abs(reconstructions[1][j][mode].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i],)+tuple([axes[1+mode,i] for mode in range(modes)]):
        ax.axis('off')        
    
plt.tight_layout()


# In[88]:


#%matplotlib widget
plt.figure()
plt.imshow(np.angle(reconstructions[0][-1][-1].array).T, origin='lower', cmap='gray')


# In[89]:


plt.figure()
plt.imshow(np.abs(probe_guess.build().array.get())**2)


# # py4DSTEM

# In[90]:


mee=measurement_p.array.reshape(10,10,286,286).transpose((1,0,2,3))


# In[91]:


np.linalg.norm(positions[1]-positions[0])


# In[92]:


ca=py4DSTEM.data.calibration.Calibration()
ca["Q_pixel_size"]=measurement_p.calibrations[1].sampling
ca["R_pixel_size"]=np.linalg.norm(positions[1]-positions[0])
ca["Q_pixel_units"]="mrad"
ca["R_pixel_units"]="A"


# In[93]:


semiangle_cutoff_p*1e-3*defocus_p


# In[94]:


np.ptp(positions[:,0])


# In[95]:


from py4DSTEM.process.phase.utils import ComplexProbe


# In[96]:


dataset = py4DSTEM.DataCube(
    data=mee,
    #measurement_p.array,
    calibration=ca
)


#ptycho = py4DSTEM.process.phase.SingleslicePtychographicReconstruction(
#ptycho = py4DSTEM.process.phase.MixedstateMultislicePtychographicReconstruction(
ptycho = py4DSTEM.process.phase.MixedstatePtychographicReconstruction(
    datacube=dataset,
    verbose=True,
    initial_scan_positions=positions,
    energy=energy,
    #num_slices=2,
    #slice_thicknesses=10,
    num_probes=1,
    semiangle_cutoff=semiangle_cutoff_p,
    defocus=defocus_p*1,
    C30=aberrations["C30"],
    device='gpu',
    object_padding_px=(0,0),
).preprocess(
    plot_center_of_mass = False,
    plot_rotation=False,
    force_com_rotation=0,
    #force_com_transpose=True,
)


# In[97]:


ptycho = ptycho.reconstruct(
    reset=True,
    store_iterations=True,
    max_iter = 64*8,
    #normalization_min=1,
    #gaussian_filter_sigma=None,
    step_size=0.8,
    
    #max_batch_size=1
).visualize(
    iterations_grid = 'auto',
    #plot_fourier_probe=True,
    plot_probe=True
)


# In[98]:


plt.figure()
plt.imshow(np.angle(ptycho.object),cmap="gray")


# In[99]:


plt.figure()
plt.imshow(np.abs(ptycho.probe_iterations[-1][0])**2,cmap="gray")


# # ASDF

# In[124]:


import matplotlib
#matplotlib.use('qt5Agg')

import PtyLab
from PtyLab import ExperimentalData
from PtyLab import Reconstruction
from PtyLab import Monitor
from PtyLab import Params
from PtyLab import Engines


# In[125]:


## initialize the ExperimentalData class and set values
exampleData = ExperimentalData(operationMode='CPM')

# check the requiredFields and the optionalFields of the ExperimentalData class 
print(exampleData.requiredFields) 
print(exampleData.optionalFields) 


# In[126]:


positions_x=positions[:,0]
positions_y=positions[:,1]


# In[127]:


# fill up the requiredFields
exampleData.ptychogram = measurement_noisy.array.transpose((0,2,1))
exampleData.wavelength = probe_p.wavelength*1e-10
exampleData.encoder = np.array((positions_y,positions_x)).T*1e-10
exampleData.dxd = measurement_noisy[0].calibrations[0].sampling*1e-3 #seting zo to 1 meter so dxd is now in radians
exampleData.zo = 1
# then fill up optionalFields. If not used, set the values to None
exampleData.entrancePupilDiameter = None  # initial estimate of beam diameter
exampleData.spectralDensity = None  # used in polychromatic ptychography
exampleData.theta = None  # used in tiltPlane reflection ptychography

# call _setData to auto-calculate all the necessary variables in ExperimentalData
exampleData._setData()


# In[128]:


## initialize the Params class and set values
params = Params()
# main parameters
params.positionOrder = 'random'  # 'sequential' or 'random'
params.propagator = 'Franhofer'  # Fraunhofer Fresnel ASP scaledASP polychromeASP scaledPolychromeASP
params.intensityConstraint = 'standard'  # standard fluctuation exponential poission
# how do we want to reconstruct (all Switches are set to False by default)
params.gpuSwitch = True
params.probePowerCorrectionSwitch = True
params.modulusEnforcedProbeSwitch = False
params.comStabilizationSwitch = True
params.orthogonalizationSwitch = True
params.orthogonalizationFrequency = 16
params.fftshiftSwitch = False


# In[129]:


## initialize the Reconstruction class and set values
reconstruction = Reconstruction(exampleData,params)
reconstruction.npsm = 1 # Number of probe modes to reconstruct
reconstruction.nosm = 1 # Number of object modes to reconstruct
reconstruction.nlambda = 1 # len(exampleData.spectralDensity) # Number of wavelength
reconstruction.nslice = 1 # Number of object slice

reconstruction.initialProbe = 'circ'
reconstruction.initialObject = 'ones'

# initialize probe and object
reconstruction.initializeObjectProbe()
# optional: customize initial probe quadratic phase
#reconstruction.probe = reconstruction.probe*np.exp(1.j*2*np.pi/reconstruction.wavelength * (reconstruction.Xp**2+reconstruction.Yp**2)/(2*0.01*6e-3))
for i in range(np.shape(reconstruction.probe)[2]): # Different probe modes should differ.. if they are similar.. the reconstruction will fail. phase factor is sufficient. 
    reconstruction.probe[0,0,i,0,:,:] = probe_guess.build().array.get()/(i+1)*np.exp(1.j*2*np.pi/reconstruction.wavelength * i*(reconstruction.Xp**2+reconstruction.Yp**2)/(2*0.01e-3))


# In[130]:


get_ipython().run_line_magic('matplotlib', 'notebook')


# In[131]:


np.sum(np.abs(probe_guess.build().array)**2)


# In[132]:


print(np.sum(np.abs(reconstruction.probe[0, 0, 0, 0,:,:] )**2))


# In[133]:


3.0e-6/1.3583932e-06


# In[134]:


np.shape(probe_guess.build().array)


# In[135]:


get_ipython().run_line_magic('matplotlib', 'notebook')


# In[136]:


plt.close()


# ## TADY DALE TO UZ NEJAK NEFUNGUJE:
# ### PtyLab mPIE na co si dat pozor:
# * kdyz mas vice modu, tak at pocatecni mody jsou dostatecne odlisne alespon ve fazi!
# * mPIE ma v sobe rPIE parametry alphaObject a alphaProbe. 

# In[158]:


get_ipython().run_cell_magic('time', '', '## initialize the Monitor class and set values\nmonitor = Monitor()\nmonitor.figureUpdateFrequency = 8\nmonitor.objectPlot = \'complex\'  # complex abs angle\nmonitor.verboseLevel = \'high\'  # high: plot two figures, low: plot only one figure\nmonitor.objectZoom = 1.5   # control object plot FoV\n#monitor.probeZoom = 0.5   # control probe plot FoV\n\n\n## choose the reconstruction engine and set values\nengine_mPIE = Engines.mPIE(reconstruction, exampleData, params, monitor)\nengine_mPIE.numIterations = 64\nengine_mPIE.betaProbe = 0.8\nengine_mPIE.betaObject = 0.8\nengine_mPIE.alphaObject = 1\nengine_mPIE.alphaProbe = 1\n#engine_mPIE.feedbackM = 0.05\n#engine_mPIE.frictionM = 0.99\n# start reconstruction\nengine_mPIE.params.OPRP=False # je potreba pro ePIE jinak nevim co dela\ng=engine_mPIE.reconstruct()\n#toto je pro ePIE...\nwhile True:\n    try: \n        print("hm")\n        next(g)\n    except: \n        print("br")\n        break\n#plt.close("all")\n')


# In[146]:


plt.figure()
plt.imshow(np.angle(np.squeeze(engine_mPIE.reconstruction.object)),origin="lower",cmap="gray")
#plt.xlim(400,500)
#plt.ylim(400,500)
#plt.clim(0,0.2)


# In[147]:


plt.figure()
plt.imshow(np.angle((engine_mPIE.reconstruction.probe[0,0,0,0,:,:])),origin="lower")


# In[148]:


engine_mPIE.reconstruction.probe.shape


# In[149]:


plt.figure()
plt.plot(engine_mPIE.reconstruction.error);plt.yscale('log')


# # Ptypy test 

# In[150]:


get_ipython().run_line_magic('mkdir', '/tmp/test_rm')


# In[151]:


import h5py
#dumping data to hdf5 as it is the most convinent.
filename_tmp = "/tmp/test_rm/tmp.hdf5"

measurement_noisy.write(filename_tmp)

gpts_x = np.shape(measurement_noisy[0])[0]
gpts_y = np.shape(measurement_noisy[0])[1]
if gpts_x != gpts_y:
    raise TypeError("different x and y dimensions in detector")
        
with h5py.File(filename_tmp, "a") as f:
    f.create_dataset('posx_m', data=positions[:,0])
    f.create_dataset('posy_m', data=positions[:,1])
    f.create_dataset('energy', data=probe_p.energy)
    
    det_pixelsize_m=55e-6
    gpts_x = np.shape(measurement_noisy[0])[0]
    gpts_y = np.shape(measurement_noisy[0])[1]
    if gpts_x != gpts_y:
        raise TypeError("different x and y dimensions in detector")
    det_pixelsize_mrad = measurement_noisy[0].calibrations[0].sampling #angular pitch of detector pixel.
    det_distance_m = det_pixelsize_m/(det_pixelsize_mrad*1e-3)
    
    f.create_dataset('det_pixelsize_m', data=det_pixelsize_m)
    f.create_dataset('det_distance_m', data=det_distance_m)


# In[152]:


get_ipython().run_line_magic('matplotlib', 'inline')


# In[153]:


import ptypy, os
import ptypy.utils as u

# This will import the HDF5Loader class
ptypy.load_ptyscan_module("hdf5_loader")

# This will import the GPU engines
ptypy.load_gpu_engines("cuda")

# Root directory of tutorial data
tutorial_data_home = "/tmp/test_rm/"

# Dataset for this tutorial
dataset = "tmp.hdf5"

# Absolute path to HDF5 file with raw data
path_to_data = os.path.join(tutorial_data_home, dataset)

# Create parameter tree
p = u.Param()

# Set verbose level to interactive
p.verbose_level = "interactive"

# Set home path and io settings (no files saved)
p.io = u.Param()
p.io.rfile = None
p.io.autosave = u.Param(active=False)
p.io.interaction = u.Param(active=False)

# Live-plotting during the reconstruction
#p.io.autoplot = u.Param()
#p.io.autoplot.active=True
#p.io.autoplot.threaded = False
#p.io.autoplot.layout = "jupyter"
#p.io.autoplot.interval = 10

# Define the scan model
p.scans = u.Param()
p.scans.scan_00 = u.Param()
p.scans.scan_00.name = 'GradFull'

# Initial illumination (based on simulated optics)
p.scans.scan_00.illumination = u.Param()
p.scans.scan_00.illumination.model = None
p.scans.scan_00.illumination.photons = None
p.scans.scan_00.illumination.aperture = u.Param()
p.scans.scan_00.illumination.aperture.form = "circ"
p.scans.scan_00.illumination.aperture.size = 0.011
p.scans.scan_00.illumination.propagation = u.Param()
p.scans.scan_00.illumination.propagation.focussed = 0.1732
p.scans.scan_00.illumination.propagation.parallel = -1.0e-8

# Data loader
p.scans.scan_00.data = u.Param()
p.scans.scan_00.data.name = 'Hdf5Loader'
p.scans.scan_00.data.orientation = 4

# Read diffraction data
p.scans.scan_00.data.intensities = u.Param()
p.scans.scan_00.data.intensities.file = path_to_data
p.scans.scan_00.data.intensities.key = "array"

# Read positions data
p.scans.scan_00.data.positions = u.Param()
p.scans.scan_00.data.positions.file = path_to_data
p.scans.scan_00.data.positions.slow_key = "posy_m"
p.scans.scan_00.data.positions.slow_multiplier = 1e-10
p.scans.scan_00.data.positions.fast_key = "posx_m"
p.scans.scan_00.data.positions.fast_multiplier = 1e-10

# Read meta data: electron energy
p.scans.scan_00.data.electron_data = True
p.scans.scan_00.data.recorded_energy = u.Param()
p.scans.scan_00.data.recorded_energy.file = path_to_data
p.scans.scan_00.data.recorded_energy.key = "energy"
p.scans.scan_00.data.recorded_energy.multiplier = 1e-3

# Read meta data: detector distance
p.scans.scan_00.data.recorded_distance = u.Param()
p.scans.scan_00.data.recorded_distance.file = path_to_data
p.scans.scan_00.data.recorded_distance.key = "det_distance_m"
p.scans.scan_00.data.recorded_distance.multiplier = 1

# Read meta data: detector pixelsize
p.scans.scan_00.data.recorded_psize = u.Param()
p.scans.scan_00.data.recorded_psize.file = path_to_data
p.scans.scan_00.data.recorded_psize.key = "det_pixelsize_m"
p.scans.scan_00.data.recorded_psize.multiplier = 1

# Read detector mask
#p.scans.scan_00.data.mask = u.Param()
#p.scans.scan_00.data.mask.file = path_to_data
#p.scans.scan_00.data.mask.key = "mask"

# Determine diffraction center from the data
p.scans.scan_00.data.auto_center = True

# Define reconstruction engine (using ePIE)
p.engines = u.Param()
if 1:
    p.engines.engine1 = u.Param()
    p.engines.engine1.name = "EPIE_pycuda"
    p.engines.engine1.numiter = 128
    p.engines.engine1.numiter_contiguous = 10
    p.engines.engine1.alpha = 0.9
    p.engines.engine1.beta = 0.9
    p.engines.engine1.probe_update_start = 0
    p.engines.engine1.object_norm_is_global = True

#else:
    p.engines.engine2 = u.Param()
    p.engines.engine2.name = "ML_pycuda"
    p.engines.engine2.ML_type = "Gaussian"
    p.engines.engine2.numiter = 2048
    p.engines.engine2.numiter_contiguous = 256
    #p.engines.engine2.smooth_gradient = 0
    #p.engines.engine2.smooth_gradient_decay = 0
    p.engines.engine2.reg_del2 = True
    p.engines.engine2.reg_del2_amplitude = 0.001
    p.engines.engine2.scale_precond = True
    p.engines.engine2.scale_probe_object = 1.

    
# Run reconstruction
P = ptypy.core.Ptycho(p,level=5)


# In[154]:


storage=P.obj.storages["Sscan_00G00"]


# In[155]:


plt.imshow(np.angle(storage.data)[0])


# In[156]:


plt.imshow(np.angle(storage.data)[0],cmap="gray")


# # Reconstructing without modes

# In[ ]:


probe_guess = Probe(semiangle_cutoff=semiangle_cutoff_p, energy=energy,defocus=defocus_p,C30=aberrations["C30"],C50=aberrations["C50"])#, C12=800)#, extent=150, gpts=128)
positions = scan_p.get_positions()
reconstructions=invms(bandlimit(measurement_p,500),probe_guess,max_angle=None,positions=positions,alpha=0.9,fac=1,beta=0.9,maxiter=180,modes=1,device="gpu",return_iterations=True)


# In[ ]:


#%matplotlib inline
plot_every = 16

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))
for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][0].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions[1][j][0].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()


# In[ ]:


#%matplotlib widget
plt.imshow(np.angle(reconstructions[0][-1][0].array).T, origin='lower', cmap='gray')


# # Coherent Case

# In[ ]:


semiangle_cutoff_coh=12.5
defocus_coh=2000*12.5/semiangle_cutoff_coh
probe_p_tmp = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff_coh,defocus=defocus_coh,C30=aberrations["C30"],C50=aberrations["C50"], device='gpu');probe_p_tmp.grid.match(potential_p)
probe_p_tmp.show()


# In[ ]:


measurement_p_raw_coh = probe_p_tmp.scan(scan_p, detector_p,potential = potential_p, pbar=True,max_batch=128)
measurement_crop_coh=crop(measurement_p_raw_coh,extent=(200,200))
measurement_noisy_coh = poisson_noise(measurement_crop_coh/np.mean(np.sum(measurement_p_raw_coh.array,(-2,-1))), dose=el,pixel_area=pixel_area)/el


# In[ ]:


measurement_crop_coh[0].show(power=0.2)


# In[ ]:


probe_guess = Probe(semiangle_cutoff=semiangle_cutoff_coh, energy=energy,defocus=defocus_coh, C30=aberrations['C30'])#, C12=0, extent=150, gpts=128)
positions = scan_p.get_positions()
reconstructions_coh=invms(bandlimit(measurement_noisy_coh,100),probe_guess,max_angle=None,positions=positions,alpha=0.2,fac=0.2,beta=1,maxiter=32,modes=1,device="gpu",return_iterations=True)


# In[ ]:


#%matplotlib inline
plot_every = 4

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions_coh[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions_coh[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions_coh[0][j][0].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions_coh[1][j][0].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()


# In[ ]:


#%matplotlib widget
plt.figure()
plt.imshow(np.abs(reconstructions[1][-1][0].array).T ** 2, origin='lower', cmap='gray')


# In[ ]:


#%matplotlib inline
plt.figure(figsize=(10,10))
plt.imshow(np.angle(reconstructions_coh[0][-1][0].array).T, origin='lower', cmap='gray')
plt.axis("off")
plt.tight_layout()


# In[ ]:


plt.figure()
plt.imshow(np.angle(reconstructions[0][-1][0].array).T, origin='lower', cmap='gray')


# In[ ]:


measurement_noisy_coh.show(power=0.1)
plt.xlim(-30,20)
plt.ylim(-70,20)


# In[ ]:


measurement_p[0].show(power=0.1)
plt.xlim(-30,20)
plt.ylim(-70,20)


# In[ ]:


measurement_p_raw_coh.show(power=0.1)
plt.xlim(-30,20)
plt.ylim(-70,20)


# In[ ]:


measurement_p_raw.show(power=0.1)
plt.xlim(-30,20)
plt.ylim(-70,20)


# In[ ]:


plt.figure()
plt.imshow(np.abs(np.fft.fftshift(np.fft.fft2(np.angle(reconstructions[0][-1][0].array).T)))**0.25, origin='lower', cmap='gray')


# # Parametric Sweep

# In[ ]:


import pandas as pd


# In[ ]:


sweep_params_df=pd.DataFrame.from_dict(sweep_params)
sweep_params_df.loc[sweep_params_df["semiangle_detector"]==60,"sampling"]/=1.5 # podpera
sweep_params_df.loc[sweep_params_df["semiangle_detector"]==30,"sampling"]/=3 # podpera
sweep_params_df


# In[ ]:


sweep_params_df.to_csv("sweep_params.csv")


# In[ ]:


for a in sweep_params_df.index:
    print(sweep_params_df.loc[a]["defocus"])


# In[ ]:


plt.figure()
for semiangle_detector in sweep_params_df["semiangle_detector"].unique():
    df=sweep_params_df[sweep_params_df["semiangle_detector"] == semiangle_detector]
    plt.plot(df["semiangle_cutoff"],df["defocus"],label="detector extent: {:0.0f} mrad".format(semiangle_detector))
plt.legend()


# In[ ]:


a_sa=30e-3*2/256
a_min=a_sa*2
1/(a_min*(1/probe_dummy.wavelength))


# In[ ]:


atoms_sweep=atoms_tot.copy()
atoms_sweep*=(1,1,1)
atoms_sweep.center(vacuum=0,axis=(0,1)) 

show_atoms(atoms_sweep,scale_atoms = 0.3)
plt.savefig("report/atoms.pdf")
show_atoms(atoms_sweep,plane="xz")


# In[ ]:


get_potential(atoms_sweep,sweep_params_df.loc[0,"extent"],sweep_params_df.loc[0,"sampling"]).build().project().show()


# In[ ]:


asdf=get_potential(atoms_sweep,sweep_params_df.loc[0,"extent"],sweep_params_df.loc[0,"sampling"])


# In[ ]:


#%matplotlib inline
from pathlib import Path
from abtem.measure import Measurement
from abtem.custom import incoherent_probe, get_probe_radius


def plot_coh(path,pd_params,Q=50e-12*10e-3,C30_fac=1,defocus_fac=1,temporal_sigma=0,spatial_sigma_flag=0,I=50e-12,modes=1,max_batch_scan=100):
    defocus_coh=pd_params["defocus"]
    semiangle_cutoff_coh=pd_params["semiangle_cutoff"]
    
    
    t=Q/I
    guess_str = ""
    if C30_fac != 1 or defocus_fac != 1: # if initial guess is different from original parameters
        guess_str="C30fac{}_defocusfac{}".format(C30_fac,defocus_fac)
        print("guess_str: ", guess_str)
    name="semi{0}_defocus{1:.0f}_I{2:.0f}pA_t{3:.0f}us".format(semiangle_cutoff_coh,defocus_coh,I/1e-12,t/1e-6)
    name = name + guess_str
    name_cache="semi{0}_defocus{1:.0f}_I{2:.0f}pA".format(semiangle_cutoff_coh,defocus_coh,I/1e-12)
    print(name)
    
    potential_p = get_potential(atoms_sweep,pd_params["extent"],pd_params["sampling"])
    
    probe_p_tmp = Probe(energy=energy, semiangle_cutoff=semiangle_cutoff_coh,defocus=defocus_coh,C30=aberrations["C30"],C50=aberrations["C50"], device='gpu');probe_p_tmp.grid.match(potential_p)
    fig, ax = plt.subplots()
    probe_p_tmp.show(ax=ax)
    #ax.plot([1,2])
    plt.axis("off")
    plt.tight_layout()

    file_name=path+name+"-probe"+".pdf"
    Path(file_name).parent.mkdir(parents=True, exist_ok=True)
    plt.savefig(file_name)
#    plt.show()
    plt.close()
    
    spatial_sigma = 0
    
    if  spatial_sigma_flag != 0 or temporal_sigma != 0:
        if spatial_sigma_flag != 0:
            spatial_sigma = get_gaussian_spread(probe_p_tmp.ctf.semiangle_cutoff,probe_p_tmp.energy,B_r,I)
        fig, ax = plt.subplots()
        print("temporal_sigma:",temporal_sigma)
        print("spatial_sigma:",spatial_sigma)
        incoherent_probe(probe_p_tmp,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma).show(ax=ax)
        print("semiangle:",probe_p_tmp.ctf.semiangle_cutoff)
        print("defocus:",probe_p_tmp.ctf.defocus)
        #ax.plot([1,2])
        plt.axis("off")
        plt.tight_layout()

        file_name=path+name+"-probe_incoh"+".pdf"
        plt.savefig(file_name)
#        plt.show()
        plt.close()

    
    incoh_probe=incoherent_probe(probe_p_tmp,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma)
    r95, _, _, _= get_probe_radius(probe_measurement=incoh_probe,current_ratio=0.95)
    d95=2*r95
    
    #scan
    detector_p=PixelatedDetector(None)
    scan_sampling= d95*0.3/2
    scan_points = 10 # number of scan points in one direction
    offset=np.array([0,0])
    start=np.array(potential_p.extent)/2-scan_sampling*scan_points//2+offset
    end=np.array(potential_p.extent)/2+scan_sampling*scan_points//2+offset
    scan_=GridScan(start,end,sampling=scan_sampling)
    positions=scan_.get_positions()#+np.random.uniform(-5,5)
    scan_p = PositionScan(positions)
    
    
    hdf5_file_name = Path.home().as_posix()+"/cache/01_iDPC/"+Path(path).parent.as_posix()+"/" + name_cache + ".hdf5"
    print(hdf5_file_name)
    if not Path(hdf5_file_name).exists():
        Path(hdf5_file_name).parent.mkdir(parents=True, exist_ok=True)
#        if pd_params["semiangle_cutoff"]<50: # podpera.. memory leek
#            max_batch_scan=50
        measurement_p_raw_coh = incoherent_scan_mc(probe_p_tmp, scan_p.get_positions(), detector=detector_p, potential = potential_p,temporal_sigma=temporal_sigma,spatial_sigma=spatial_sigma,max_batch_scan=max_batch_scan)
        measurement_p_raw_coh.write(hdf5_file_name)
    else:
        measurement_p_raw_coh = Measurement.read(hdf5_file_name)
    
    
    
    measurement_crop_coh=crop(measurement_p_raw_coh,extent=(2*pd_params["semiangle_detector"],2*pd_params["semiangle_detector"]))
    el=I*t/c.e# charge: 50pA times 10ms (when time is 2ms the dose is insuficcient to resolve the sparse moire hexagon 10ms it starts to be resolvable)
    pixel_area=1 #effectively disabled 
    measurement_noisy_coh = poisson_noise(measurement_crop_coh/np.mean(np.sum(measurement_p_raw_coh.array,(-2,-1))), dose=el,pixel_area=pixel_area)/el
    
    fig, ax = plt.subplots()
    if len(np.shape(measurement_noisy_coh)) == 4:
        measurement_noisy_coh[0,0].show(power=0.2,ax=ax)
    elif len(np.shape(measurement_noisy_coh)) == 3:
        measurement_noisy_coh[0].show(power=0.2,ax=ax)    
    else:
        raise ValueError("wrong shape")
    file_name=path+name+"-cbed.pdf"
    plt.savefig(file_name)
#    plt.show()
    plt.close()
    
    max_angle=None
    if (pd_params["semiangle_detector"]<50): # I guess to prevent aliassing and in case of large semiangle_cutoff and small semiangle_detector and to increase the sampling in direct space; 50 mrad should be enough to see atoms
        max_angle = pd_params["semiangle_detector"]*2 # if diffraction data take most of the detector, then in order to e
    probe_guess = Probe(semiangle_cutoff=semiangle_cutoff_coh, energy=energy,defocus=defocus_fac*defocus_coh, C30=C30_fac*aberrations['C30'])#, C12=0, extent=150, gpts=128)
    positions = scan_p.get_positions()
    reconstructions_coh=invms(measurement_noisy_coh,probe_guess,max_angle=max_angle,positions=positions,alpha=0.8,fac=1,beta=1,maxiter=128,modes=modes,device="gpu",return_iterations=True)
    
    plt.figure()
    plt.imshow(np.angle(reconstructions_coh[0][-1][0].array).T, origin='lower', cmap='gray')
    plt.axis("off")
    plt.tight_layout()
    
    file_name=path+name+"-object"+".pdf"
    print(name)
    plt.savefig(file_name)
#    plt.show()
    plt.close()
    
    plt.figure()
    plt.imshow(np.abs(reconstructions_coh[1][-1][0].array).T ** 2, origin='lower', cmap='gray')
    plt.axis("off")
    plt.tight_layout()
    file_name=path+name+"-probe_rec"+".pdf"
    plt.savefig(file_name)
#    plt.show()
    plt.close()
    
    del reconstructions_coh
    if temporal_sigma!=0 or spatial_sigma_flag!=0:
        modes=4
        reconstructions_partial=invms(measurement_noisy_coh,probe_guess,max_angle=max_angle,positions=positions,alpha=0.8,fac=1,beta=1,maxiter=256,modes=modes,device="gpu",return_iterations=True)
        
        plot_every = 64
        fig, axes = plt.subplots(1+modes, int(np.ceil(len(reconstructions_partial[0]) / plot_every)), figsize=(20,20))
    
        for i, j in enumerate(range(0, len(reconstructions_partial[0]), plot_every)):
            axes[0,i].imshow(np.angle(reconstructions_partial[0][j][-1].array).T, origin='lower', cmap='gray')
            for mode in range(modes): 
                axes[1+mode,i].imshow(np.abs(reconstructions_partial[1][j][mode].array).T ** 2, origin='lower', cmap='gray')
            for ax in (axes[0,i],)+tuple([axes[1+mode,i] for mode in range(modes)]):
                ax.axis('off')
    
        file_name=path+name+"-object-modes"+str(modes)+".pdf"
        print(name)
        plt.tight_layout()
        plt.savefig(file_name)
#        plt.show()
        plt.close()
        del reconstructions_partial
    del potential_p,measurement_crop_coh,measurement_p_raw_coh,measurement_noisy_coh,probe_p_tmp,incoh_probe,probe_guess
    cp._default_memory_pool.free_all_blocks()


# In[ ]:


Q0=50e-12*10e-3
Q0
doses_val=np.array([Q0/100,Q0/10,Q0,Q0*10])


# In[ ]:


cp._default_memory_pool.free_all_blocks()


# In[ ]:


#TEST
#doses_val=np.array([Q0])#np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for semiangle_detector in sweep_params_df["semiangle_detector"].unique()[2:]:
    pd_paramss=sweep_params_df[sweep_params_df["semiangle_detector"]==semiangle_detector]
    for defocus_fac in [1]:#,0.9,0.8,0.7,0.6,0.5]:
        for dose_val in doses_val:
            for idx in pd_paramss.index[-1:]:
                pd_params=pd_paramss.loc[idx]
                dose_str = "{:.0e}/".format(dose_val)
                plot_coh(path="report/figs/"+"det_{}mrad".format(semiangle_detector)+"/01_coh_50pA/"+dose_str,pd_params=pd_params,Q=dose_val,temporal_sigma=0,spatial_sigma_flag=0,I=50e-12,defocus_fac=defocus_fac)


# In[ ]:


#doses_val=np.array([Q0/100,Q0/10,Q0])#np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for semiangle_detector in sweep_params_df["semiangle_detector"].unique():
    pd_paramss=sweep_params_df[sweep_params_df["semiangle_detector"]==semiangle_detector]
    for defocus_fac in [1]:#,0.9,0.8,0.7,0.6,0.5]:
        for dose_val in doses_val:
            for idx in pd_paramss.index:
                pd_params=pd_paramss.loc[idx]
                dose_str = "{:.0e}/".format(dose_val)
                plot_coh(path="report/figs/"+"det_{}mrad".format(semiangle_detector)+"/01_coh_50pA/"+dose_str,pd_params=pd_params,Q=dose_val,temporal_sigma=0,spatial_sigma_flag=0,I=50e-12,defocus_fac=defocus_fac)


# In[ ]:


#doses_val=np.array([Q0/100,Q0/10,Q0,Q0*10])#np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for semiangle_detector in sweep_params_df["semiangle_detector"].unique():
    pd_paramss=sweep_params_df[sweep_params_df["semiangle_detector"]==semiangle_detector]
    for defocus_fac in [1]:#,0.9,0.8,0.7,0.6,0.5]:
        for dose_val in doses_val:
            for idx in pd_paramss.index:
                pd_params=pd_paramss.loc[idx]
                dose_str = "{:.0e}/".format(dose_val)
                plot_coh(path="report/figs/"+"det_{}mrad".format(semiangle_detector)+"/02_partial_temporal_50pA/"+dose_str,pd_params=pd_params,Q=dose_val,temporal_sigma=temporal_sigma,spatial_sigma_flag=0,I=50e-12,defocus_fac=defocus_fac)


# In[ ]:


#doses_val=np.array([Q0/100,Q0/10,Q0,Q0*10])#np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for semiangle_detector in sweep_params_df["semiangle_detector"].unique()[::-1]:
    pd_paramss=sweep_params_df[sweep_params_df["semiangle_detector"]==semiangle_detector]
    for defocus_fac in [1]:#,0.9,0.8,0.7,0.6,0.5]:
        for dose_val in doses_val:
            for idx in pd_paramss.index:
                pd_params=pd_paramss.loc[idx]
                dose_str = "{:.0e}/".format(dose_val)
                plot_coh(path="report/figs/"+"det_{}mrad".format(semiangle_detector)+"/03_partial_spatial_50pA/"+dose_str,pd_params=pd_params,Q=dose_val,temporal_sigma=0,spatial_sigma_flag=1,I=50e-12,defocus_fac=defocus_fac)


# In[ ]:


#doses_val=np.array([Q0*10,Q0,Q0/10,Q0/100])#np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for semiangle_detector in sweep_params_df["semiangle_detector"].unique()[::-1]:
    pd_paramss=sweep_params_df[sweep_params_df["semiangle_detector"]==semiangle_detector]
    for defocus_fac in [1]:#,0.9,0.8,0.7,0.6,0.5]:
        for dose_val in doses_val:
            for idx in pd_paramss.index:
                pd_params=pd_paramss.loc[idx]
                dose_str = "{:.0e}/".format(dose_val)
                plot_coh(path="report/figs/"+"det_{}mrad".format(semiangle_detector)+"/04_partial_both_50pA/"+dose_str,pd_params=pd_params,Q=dose_val,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,I=50e-12,defocus_fac=defocus_fac)


# In[ ]:


#doses_val=np.array([Q0])#np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for semiangle_detector in sweep_params_df["semiangle_detector"].unique():
    pd_paramss=sweep_params_df[sweep_params_df["semiangle_detector"]==semiangle_detector]
    for defocus_fac in [1]:#,0.9,0.8,0.7,0.6,0.5]:
        for dose_val in doses_val:
            for idx in pd_paramss.index:
                pd_params=pd_paramss.loc[idx]
                dose_str = "{:.0e}/".format(dose_val)
                plot_coh(path="report/figs/"+"det_{}mrad".format(semiangle_detector)+"/05_partial_both_100pA/"+dose_str,pd_params=pd_params,Q=dose_val,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,I=100e-12,defocus_fac=defocus_fac)


# In[ ]:


#doses_val=np.array([Q0])#np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for semiangle_detector in sweep_params_df["semiangle_detector"].unique()[::-1]:
    pd_paramss=sweep_params_df[sweep_params_df["semiangle_detector"]==semiangle_detector]
    for defocus_fac in [1]:#,0.9,0.8,0.7,0.6,0.5]:
        for dose_val in doses_val:
            for idx in pd_paramss.index:
                pd_params=pd_paramss.loc[idx]
                dose_str = "{:.0e}/".format(dose_val)
                plot_coh(path="report/figs/"+"det_{}mrad".format(semiangle_detector)+"/06_partial_both_200pA/"+dose_str,pd_params=pd_params,Q=dose_val,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,I=200e-12,defocus_fac=defocus_fac)


# In[ ]:


#doses_val=np.array([Q0*10,Q0,Q0/10,Q0/100]) #5e-12,5e-13,5e-14
for semiangle_detector in sweep_params_df["semiangle_detector"].unique()[::-1]:
    pd_paramss=sweep_params_df[sweep_params_df["semiangle_detector"]==semiangle_detector]
    for defocus_fac in [1]:#,0.9,0.8,0.7,0.6,0.5]:
        for dose_val in doses_val:
            for idx in pd_paramss.index:
                pd_params=pd_paramss.loc[idx]
                dose_str = "{:.0e}/".format(dose_val)
                plot_coh(path="report/figs/"+"det_{}mrad".format(semiangle_detector)+"/07_partial_both_400pA/"+dose_str,pd_params=pd_params,Q=dose_val,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,I=400e-12,defocus_fac=defocus_fac)


# In[ ]:


#STARE:


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/01_coh/5e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*10,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/01_coh/5e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/01_coh/5e-14C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/10,scan_p = scan_dummy_0)


# In[ ]:


#partial coherence


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/02_partial_temporal/5e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*10,temporal_sigma=temporal_sigma,spatial_sigma_flag=0,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/02_partial_temporal/5e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0,temporal_sigma=temporal_sigma,spatial_sigma_flag=0,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/02_partial_temporal/5e-14C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/10,temporal_sigma=temporal_sigma,spatial_sigma_flag=0,scan_p = scan_dummy_0)


# In[ ]:


# Partial spatial coherence


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
   plot_coh(path="report/figs/03_partial_spatial/5e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*10,temporal_sigma=0,spatial_sigma_flag=1,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/03_partial_spatial/5e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0,temporal_sigma=0,spatial_sigma_flag=1,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/03_partial_spatial/5e-14C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/10,temporal_sigma=0,spatial_sigma_flag=1,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/04_partial_both/5e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*10,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/04_partial_both/5e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0)


# In[ ]:


#plot_rm(path="report/figs/04_partial_both/5e-13C_rm/",semiangle_cutoff_coh=12.5,Q=Q0,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs[::-1]:
    plot_coh(path="report/figs/04_partial_both/5e-14C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/10,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0)


# In[ ]:





# In[ ]:


scan_dummy = GridScan((potential_p.extent[0]*(5-f)/10,potential_p.extent[0]*(5-f)/10), (potential_p.extent[1]*(5+f)/10, potential_p.extent[1]*(5+f)/10), sampling = 3/2)


# In[ ]:


#PROBLEM! or not problem? -> reconstruction divreges in amplitude.. alpha parameter needs to be lower. 


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/08_coh_stepsize1.5/1.25e-13/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/4,temporal_sigma=0,spatial_sigma_flag=0,scan_p=scan_dummy)


# In[ ]:


#HOPEFULLY END OF PROBLEM


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/05_partial_temporal_stepsize1.5/1.25e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/4,temporal_sigma=temporal_sigma,spatial_sigma_flag=0,scan_p=scan_dummy)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/06_partial_spatial_stepsize1.5/1.25e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/4,temporal_sigma=0,spatial_sigma_flag=1,scan_p=scan_dummy)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs[::-1]:
    plot_coh(path="report/figs/07_partial_both_stepsize1.5/1.25e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/4,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p=scan_dummy)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs[::-1]:
    plot_coh(path="report/figs/07_partial_both_stepsize1.5/1.25e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/10/4,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p=scan_dummy)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs[::-1]:
    plot_coh(path="report/figs/07_partial_both_stepsize1.5/1.25e-11C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*10/4,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p=scan_dummy)


# In[ ]:


####


# In[ ]:


scan_dummy_2 = GridScan((potential_p.extent[0]*(5-f)/10,potential_p.extent[0]*(5-f)/10), (potential_p.extent[1]*(5+f)/10, potential_p.extent[1]*(5+f)/10), sampling = 3*np.sqrt(2))


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/09_partial_temporal_stepsize4.2/10e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*2,temporal_sigma=temporal_sigma,spatial_sigma_flag=0,scan_p=scan_dummy_2)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/10_partial_spatial_stepsize4.2/10e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*2,temporal_sigma=0,spatial_sigma_flag=1,scan_p=scan_dummy_2)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs[::-1]:
    plot_coh(path="report/figs/11_partial_both_stepsize4.2/10e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*2,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p=scan_dummy_2)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs[::-1]:
    plot_coh(path="report/figs/11_partial_both_stepsize4.2/10e-14C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/10*2,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p=scan_dummy_2)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs[::-1]:
    plot_coh(path="report/figs/11_partial_both_stepsize4.2/10e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*10*2,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p=scan_dummy_2)


# In[ ]:





# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/12_partial_both_1nA/5e-13C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0,I=1000e-12)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/12_partial_both_1nA/5e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*10,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0,I=1000e-12)


# In[ ]:


for semiangle_cutoff in semiangle_cutoffs:
    plot_coh(path="report/figs/12_partial_both_1nA/5e-14C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0/10,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0,I=1000e-12)


# In[ ]:


for defocus_fac in [0.5,0.7,0.9]:
    for semiangle_cutoff in semiangle_cutoffs:
        plot_coh(path="report/figs/04_partial_both/5e-12C/",semiangle_cutoff_coh=semiangle_cutoff,Q=Q0*10,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0,C30_fac=1,defocus_fac=defocus_fac)


# In[ ]:


doses_val=np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for defocus_fac in [1,0.9,0.8,0.7,0.6,0.5]:
    for dose_val in doses_val:
        for semiangle_cutoff in semiangle_cutoffs:
            dose_str = "{:.0e}/".format(dose_val)
            plot_coh(path="report/figs/13_partial_both_500pA/"+dose_str,semiangle_cutoff_coh=semiangle_cutoff,Q=dose_val,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0,I=500e-12,defocus_fac=defocus_fac)


# In[ ]:


doses_val=np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for defocus_fac in [1,0.9,0.8,0.7,0.6,0.5]:
    for dose_val in doses_val:
        for semiangle_cutoff in semiangle_cutoffs:
            dose_str = "{:.0e}/".format(dose_val)
            plot_coh(path="report/figs/14_partial_both_250pA/"+dose_str,semiangle_cutoff_coh=semiangle_cutoff,Q=dose_val,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0,I=250e-12,defocus_fac=defocus_fac)


# In[ ]:


doses_val=np.array([Q0*10,Q0,Q0/10]) #5e-12,5e-13,5e-14
for defocus_fac in [1,0.9,0.8,0.7,0.6,0.5]:
    for dose_val in doses_val:
        for semiangle_cutoff in semiangle_cutoffs:
            dose_str = "{:.0e}/".format(dose_val)
            plot_coh(path="report/figs/15_partial_both_125pA/"+dose_str,semiangle_cutoff_coh=semiangle_cutoff,Q=dose_val,temporal_sigma=temporal_sigma,spatial_sigma_flag=1,scan_p = scan_dummy_0,I=125e-12,defocus_fac=defocus_fac)


# In[ ]:


#del potential_p

cp._default_memory_pool.free_all_blocks()


# In[ ]:


plt.close()

