import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from abtem.reconstruct import invms
from abtem.measure import Measurement, Calibration
from abtem.utils import energy2wavelength
from abtem.waves import Probe
from abtem.utils import energy2wavelength
import scipy.io as io


matplotlib.rcParams["figure.figsize"] = (20, 10)
matplotlib.rcParams['lines.linewidth'] = 2
font = {'weight' : 'bold',
                'size'   : 22}
matplotlib.rc('font', **font)

def plot(fil,modes=1,energy = 300e3, defocus = -200):
    measurement=Measurement.read(fil)
    f = h5py.File(fil,"r")
    positions=np.array(f["positions"])


    semiangle_cutoff = 21.4 
    probe_guess = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=defocus,extent=40,gpts=256)
    reconstructions = invms(measurement, probe_guess, positions = positions, modes=modes,k_modes=1, alpha=1, fac=1, beta=1, slices=1, slice_thickness=21,  maxiter=8, return_iterations=True, fix_com=True,device = 'gpu')
    
    
    plot_every = 2

    fig, axes = plt.subplots(1+modes, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))

    for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
        axes[0,i].imshow(np.angle(reconstructions[0][j][-1].array).T, origin='lower', cmap='gray')
        for mode in range(modes): 
            axes[1+mode,i].imshow(np.abs(reconstructions[1][j][mode].array).T ** 2, origin='lower', cmap='gray')
        for ax in (axes[0,i], axes[1,i]):
            ax.axis('off')
    
    res = np.zeros_like(reconstructions[1][-1][0].array,dtype=float)
    for i in range(modes):
            res += np.abs(reconstructions[1][-1][i].array)**2
    plt.tight_layout()
    plt.figure()
    fig, axes = plt.subplots(1, 2, figsize=(20,20))
    axes[0].imshow(np.angle(reconstructions[0][-1][-1].array).T, origin='lower', cmap='gray') 
    axes[1].imshow(res.T, origin='lower', cmap='gray')

    plt.tight_layout()
#|
filename="data/1_frozen_100keV/1_merged.hdf5"
#incoherent with 1 mode
plot(filename, modes = 1, energy = 100e3, defocus = -150)
#|
#incoherent with 3 modes
plot(filename, modes = 3, energy = 100e3, defocus = -150)
#|
filename="data/1_frozen_100keV/1_focal4.hdf5"
#coherent with 1 mode
plot(filename, modes = 1, energy = 100e3, defocus = -150)
#|
#coherent with 3 modes
plot(filename, modes = 3, energy = 100e3, defocus = -150)

#|
filename="data/1_frozen_4xfocalspread/1_merged.hdf5"
#incoherent with 1 mode
plot(filename, modes=1)
#|
#incoherent with 3 modes
plot(filename, modes=3)
#|
filename="data/1_frozen_4xfocalspread/1_focal4.hdf5"
#coherent with 1 mode
plot(filename, modes=1)
#|
#coherent with 3 modes
plot(filename, modes=3)
#|
#########################################
#filename="data/1_frozen/1_merged.hdf5"
#plot(filename)
##|
#filename="data/1_frozen/1_focal4.hdf5"
#plot(filename)
#########################################



##|
#filename="data/20_frozen/1_merged.hdf5"
#plot(filename)
##|
#filename="data/20_frozen/1_focal4.hdf5"
#plot(filename)
