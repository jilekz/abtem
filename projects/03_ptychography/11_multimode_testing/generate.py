#%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

from abtem import GridScan, PixelatedDetector, Potential, Probe, show_atoms, SMatrix, AnnularDetector
from abtem.scan import PositionScan
from ase.build import mx2
from abtem.structures import orthogonalize_cell
from abtem.measure import bandlimit, Measurement
from abtem.utils import fft_shift

#|

from ase.visualize import view
from abtem import PotentialArray
from abtem.utils import energy2sigma
from pathlib import Path
from abtem.waves import Waves
import cupy

from abtem.reconstruct import invms, gram_schmidt
import h5py

#| 

import matplotlib
matplotlib.rcParams["figure.figsize"] = (20, 20)
matplotlib.rcParams['lines.linewidth'] = 2
font = {'weight' : 'bold',
        'size'   : 22}
matplotlib.rc('font', **font)

#|

from ase.io import read
prsco3 = read('../PrScO3_mp-559756_primitive.cif')
atoms=prsco3*(8,8,2)
view(atoms)

#|

#rs_sampling=0.08984*2/3*0.98#S.wavelength/(256*0.856*1e-3*2/3)
gpts=512
if 1:
    potential = Potential(atoms, 
                          gpts=gpts,
                          #sampling=rs_sampling,
                          projection='infinite', 
                          slice_thickness=1, 
                          parametrization='kirkland').build()
else:
    from abtem.temperature import FrozenPhonons

    def B2sigma(B):
        return(np.sqrt(B*3/8/np.pi**2))

    B_Pr = 0.88
    B_Sc = 0.70
    B_O = 1.00

    sigma_Pr = B2sigma(B_Pr)
    sigma_Sc = B2sigma(B_Sc)
    sigma_O = B2sigma(B_O)

    sigmas = {'Pr': sigma_Pr, 'Sc': sigma_Sc, 'O': sigma_O} # standard deviations of thermal vibrations
    num_configs = 20 # number of frozen phonon configurations

    frozen_phonons = FrozenPhonons(atoms, num_configs=num_configs, sigmas=sigmas)
    potential = Potential(frozen_phonons, 
                          gpts=gpts, 
                          #sampling=rs_sampling,
                          slice_thickness=1, parametrization='kirkland', projection='infinite')

    atoms_conf = next(iter(frozen_phonons))

    show_atoms(atoms_conf)

#|
try:
    print(np.shape(potential.array)) # For potential obtained via PotentialArray
except:
    print(np.shape(potential.build().array)) #For normal potential

#|
energy = 100e3
Cc = 2e-3/1e-10 # In Angstrom
focal_spread = Cc*(0.4/energy)
defocus = -50

print(focal_spread)
#|

N=10
support_width=focal_spread*2
focal_spreads=np.linspace(-support_width,support_width,N)
p=1/np.sqrt(np.pi)/focal_spread*np.exp(-(focal_spreads/focal_spread)**2)
p=p/np.sum(p)

for i,_ in enumerate(focal_spreads):
    probe = Probe(semiangle_cutoff=21.4, energy=energy, defocus=defocus+focal_spreads[i], Cs=0,device='gpu')
    probe.grid.match(potential)

    center=np.array(potential.extent)/2
    offset=np.array([16.1,16.6])/2 #== np.array([35,36])*0.46/2 number of steps*sampling/2

    #gridscan = GridScan((0,0), np.array(potential.extent), sampling=0.46)
    gridscan = GridScan(center-offset,center+offset, sampling=0.46)
    scan=gridscan
    #scan = PositionScan(spiral_scan(15/2,20,1200)+center)

    nam="1"+"_focal"+str(i)
    filename="data/1_frozen_100keV/"+nam+".hdf5"

    detector = PixelatedDetector(None,mode="intensity",save_file=filename)#max_angle=256*0.43609418678539313*2/2*0.98) # from parameters in paper times 0.99

    if not Path(filename).exists():
        measurement = probe.scan(scan, [detector], potential, pbar=True)

        positions = scan.get_positions()
        with h5py.File(filename, "a") as f:
            f.create_dataset('positions', data=positions)
    else:
        print("Doing nothing. Results are in file "+filename)

#|
measurement = Measurement.read("data/1_frozen_100keV/1_focal0.hdf5")
measurement.array *=0

for i,_ in enumerate(focal_spreads):
    nam="data/1_frozen_100keV/1_focal"+str(i)+".hdf5"
    measurement.array += Measurement.read(nam).array*p[i]

filename_merged = "data/1_frozen_100keV/1_merged.hdf5"
measurement.write(filename_merged)
positions = scan.get_positions()
with h5py.File(filename_merged, "a") as f:
    f.create_dataset('positions', data=positions)

#|
