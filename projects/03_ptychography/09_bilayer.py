# on abtem version: 7a663e35aea0aa787257840f232dcaee4a734ae7
# on py4DSTEM version: 40ae3cbe6c9ed5ccc826edd1c378a419736ebd7a
import mat73
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from abtem.reconstruct import invms
from abtem.measure import Measurement, Calibration
from abtem.utils import energy2wavelength
from abtem.waves import Probe
from abtem.utils import energy2wavelength
import scipy.io as io


matplotlib.rcParams["figure.figsize"] = (20, 10)
matplotlib.rcParams['lines.linewidth'] = 2
font = {'weight' : 'bold',
                'size'   : 22}
matplotlib.rc('font', **font)

#|

data=io.loadmat("/home/jilek/Downloads/rawdata_1x_crop.mat") #data from https://data.paradim.org/blob/8a5b844f325841eba38156896d94d74de984cd6de4e9d52a553f0e8eac24c00c3da281f243e8e05352b62cea37536fbdf8efee60774aa925f85ce8df5d94b704
energy=data['voltage'][0,0]*1e3 # voltage in kV
#dp=data['cbed'].reshape((*data['cbed'].shape[:2],-1)) # from shape 124,124,60,60 to shape 124,124,3600 
dp=data['cbed'][:,:,30:30+30,0:0+30].reshape((*data['cbed'].shape[:2],-1)) # from shape 124,124,60,60 to shape 124,124,3600 (selecting only region of 30x30 scan points out of 60x60)
dp=dp.transpose((2,0,1)) # from shape 124,124,3600 to shape 3600,124,124
dp[dp<data['ADU']]=0
dp=dp/np.mean(np.sum(np.abs(dp),(1,2)))
N_dp=30 # see np.sqrt(np.shape(data['cbed'])[-1])
#N_dp=60 # see np.sqrt(np.shape(data['cbed'])[-1])
x = np.linspace(0,(N_dp-1)*data['scanstep'],N_dp) 
y = np.linspace(0,(N_dp-1)*data['scanstep'],N_dp) 
grid = np.meshgrid(x,y)
positions_x = grid[0].T.reshape(-1)
positions_y = grid[1].T.reshape(-1)

rot_angle = 30/180*np.pi
positions_x_rot = positions_x*np.cos(rot_angle) + positions_y*np.sin(rot_angle)  + 5
positions_y_rot = positions_x*-np.sin(rot_angle) + positions_y*np.cos(rot_angle) + 20
positions = np.array([positions_x_rot,positions_y_rot]).T

#| 

plt.figure()
plt.plot(positions_x_rot,positions_y_rot,"-x")
plt.show()

#|
#scale_factor=1000*energy2wavelength(energy*1e3)#to convert spatial frequency to mrad
units = 'mrad'
names = ['alpha_x', 'alpha_y']

dk=data['alpha0']/1e3/data['rbf']/energy2wavelength(energy) #https://github.com/muller-group-cornell/ptychography/blob/73bf0b77560b5dcfaa03c11fbdd2c82f6f501395/get_inputs_real_new.m
dalpha=dk/(1/energy2wavelength(energy))*1e3

cals=(None,)
for name in names:
    cal=Calibration(offset=0,sampling=dalpha, units = units, name=names)
    #cal=calibrations_from_grid(gpts=np.shape(dp)[1:], sampling = (data['dk'],)*2 , fourier_space=True, names=name, units=units, scale_factor=scale_factor)
    cals+=(cal,)
measurement=Measurement(dp,calibrations=cals)

#|
semiangle_cutoff = data['alpha0'][0,0]
defocus = data['df'][0,0]
probe_guess = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=defocus,extent=40,gpts=256)
reconstructions = invms(measurement, probe_guess, positions = positions, modes=3,k_modes=1, alpha=0.05, fac=1, beta=0.5, slices=1, slice_thickness=21,  maxiter=32*8, return_iterations=True, fix_com=True,device='gpu')

#| 

plot_every = 32

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][-1].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions[1][j][0].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()

#|

plot_every = 32

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][-1].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions[1][j][1].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()

#|

plot_every = 32

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][-1].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions[1][j][2].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()

#|

plt.figure(figsize=(20,20))
plt.imshow(np.angle(reconstructions[0][-1][-1].array).T, origin='lower', cmap='gray')

#|

import py4DSTEM # nejak se mi to nedari s py4DSTEM
mee=measurement.array.reshape(N_dp,N_dp,124,124).transpose((0,1,2,3))
ca=py4DSTEM.data.calibration.Calibration()
ca["Q_pixel_size"]=np.squeeze(measurement.calibrations[1].sampling)
ca["R_pixel_size"]=np.linalg.norm(positions[1]-positions[0])
ca["Q_pixel_units"]="mrad"
ca["R_pixel_units"]="A"

dataset = py4DSTEM.DataCube(
    data=mee,
    calibration=ca
)


#ptycho = py4DSTEM.process.phase.SingleslicePtychographicReconstruction(
#ptycho = py4DSTEM.process.phase.MixedstateMultislicePtychographicReconstruction(
ptycho = py4DSTEM.process.phase.MixedstatePtychographicReconstruction(
    datacube=dataset,
    verbose=True,
    #initial_scan_positions=positions,
    energy=energy,
    #num_slices=2,
    #slice_thicknesses=10,
    num_probes=1,
    semiangle_cutoff=semiangle_cutoff,
    defocus=defocus,
    #C30=aberrations["C30"],
    device='gpu',
    object_padding_px=(30,30),
).preprocess(
    plot_center_of_mass = False,
    plot_rotation=False,
    force_com_rotation=30,
    force_com_transpose=False,
)

ptycho = ptycho.reconstruct(
    reset=True,
    store_iterations=True,
    max_iter = 32*8,
    #normalization_min=1,
    #gaussian_filter_sigma=None,
    step_size=0.2, # kdyz se do kodu prida konstanta pro ovlivneni stopy step_size_probe = 0.5, tak aby byla odlisna od step_size=0.2, tak je vysledek trochu lepsi, ale stoji to porad za prd, nicmene naznak tam videt je.. 
    
    max_batch_size=256 # shodou okolnosti kdyz max_batch_size zvysim treba az na 2024, tak ma vysledek lepsi kontrast, ale furt chybi vnitrni struktura - neodpovida to tomu co maji ve clanku
).visualize(
    iterations_grid = 'auto',
    #plot_fourier_probe=True,
    plot_probe=True
)

#|

plt.figure(figsize=(20,20)) 
plt.imshow(np.angle(ptycho.object_iterations[-1]).T,cmap="gray",origin='lower')

#|

plt.figure(figsize=(20,20))
plt.imshow(np.abs(ptycho.probe_iterations[-1][2])**2)

