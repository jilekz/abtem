import mat73
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from abtem.reconstruct import invms
from abtem.measure import Measurement, Calibration
from abtem.utils import energy2wavelength
from abtem.waves import Probe
from abtem.utils import energy2wavelength
import scipy.io as io


matplotlib.rcParams["figure.figsize"] = (20, 10)
matplotlib.rcParams['lines.linewidth'] = 2
font = {'weight' : 'bold',
                'size'   : 22}
matplotlib.rc('font', **font)

#|

data=io.loadmat("/home/jilek/Downloads/MoS2_4D_data_step2x_crop_3x3uc.mat") #data from https://drive.google.com/drive/folders/1QF9ntp8tRhJetT-5_fkcsxml7X--9PFw 
energy=data['voltage'][0,0]*1e3 # voltage in kV
wavelength=energy2wavelength(energy)
dp=data['dp'].reshape((*data['dp'].shape[:2],-1)) # from shape 124,124,31,26 to shape 124,124,806 
N_dp_x=26
N_dp_y=31 
dp=dp.transpose((2,0,1)) # from shape 124,124,806 to shape 806,124,124
dp[dp<20]=0
dp=dp/np.mean(np.sum(np.abs(dp),(1,2)))
x = np.linspace(0,(N_dp_x-1)*data['scanStepSize_x'],N_dp_x) 
y = np.linspace(0,(N_dp_y-1)*data['scanStepSize_y'],N_dp_y) 
grid = np.meshgrid(x,y)
positions_x = grid[0].reshape(-1)
positions_y = grid[1].reshape(-1)

rot_angle = 30/180*np.pi
positions_x_rot = positions_x*np.cos(rot_angle) + positions_y*np.sin(rot_angle)
positions_y_rot = positions_x*-np.sin(rot_angle) + positions_y*np.cos(rot_angle)
positions = np.array([positions_x_rot,positions_y_rot]).T

#| 

plt.figure()
plt.plot(positions_x_rot,positions_y_rot,"-x")
plt.show()

#|
units = 'mrad'
names = ['alpha_x', 'alpha_y']

dk = data['dk'][0][0] 
dalpha = dk/(1/wavelength)*1e3

cals=(None,)
for name in names:
    cal=Calibration(offset=0,sampling=dalpha, units = units, name=names)
    #cal=calibrations_from_grid(gpts=np.shape(dp)[1:], sampling = (data['dk'],)*2 , fourier_space=True, names=name, units=units, scale_factor=scale_factor)
    cals+=(cal,)
measurement=Measurement(dp,calibrations=cals)

#|
semiangle_cutoff = data['alpha_max'][0,0]
defocus = data['df'][0,0]
probe_guess = Probe(semiangle_cutoff=semiangle_cutoff, energy=energy,defocus=defocus,extent=40,gpts=256)
reconstructions = invms(measurement, probe_guess, positions = positions, modes=2,k_modes=4, alpha=0.2, fac=0.2, beta=1, slices=1, slice_thickness=21,  maxiter=16, return_iterations=True, fix_com=False,device='gpu')

#| 

plot_every = 4

fig, axes = plt.subplots(2, int(np.ceil(len(reconstructions[0]) / plot_every)), figsize=(20,20))

for i, j in enumerate(range(0, len(reconstructions[0]), plot_every)):
    axes[0,i].imshow(np.angle(reconstructions[0][j][-1].array).T, origin='lower', cmap='gray')
    axes[1,i].imshow(np.abs(reconstructions[1][j][0].array).T ** 2, origin='lower', cmap='gray')
    for ax in (axes[0,i], axes[1,i]):
        ax.axis('off')
    
plt.tight_layout()

#| 
res=[]
for i in range(16):
        res.append(np.sum(np.abs(reconstructions[1][i][0].array)**2))
        plt.plot(res)
